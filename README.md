# Hodebba back

## Set up

- Create `.env` (You can edit variables if you wish)

````
cp .env.sample .env
````

- Create `flyway.conf` (You can edit variables if you wish)

````
cp flyway/conf/flyway.conf.sample flyway/conf/flyway.conf
````

### When you need to work on the backend:

- Turn on the database

```` 
docker-compose up --build --force-recreate hodebba-db flyway elasticsearch
````

- Then import the pom.xml in intelij and launch `BackApplication.java`. The backend app will be running on http://localhost:8080. (Be sure to use JDK13 in intelij)

### When you need to work on the frontend with the backend running locally:

````
docker-compose up --build
````

The backend app will be running on http://localhost:3001.

## API

The API documentation is updated with Swagger. Check it on http://localhost:8080/swagger-ui.html (or http://localhost:3001/swagger-ui.html if you are running the backend with Docker)