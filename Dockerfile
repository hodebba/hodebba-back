## MA-Store Build image
FROM maven:3.6.1-jdk-11 AS hodebba-build
ENV HODEBBA_HOME /opt/hodebba/backend
WORKDIR $HODEBBA_HOME
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src ./src
RUN mvn package -D env=prod -DskipTests

## MA-Store run image
FROM openjdk:11.0.1-jre-slim
ENV HODEBBA_HOME /opt/hodebba/backend
ENV DB_HOST=hodebba-db
ENV DB_PORT=5433
WORKDIR $HODEBBA_HOME

# Download wait-for-it.sh
ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh wait-for-it.sh
RUN chmod +x $HODEBBA_HOME/wait-for-it.sh

# Copy artifact from build-image
COPY --from=hodebba-build $HODEBBA_HOME/target/*.jar $HODEBBA_HOME/hodebba.jar

ENTRYPOINT bash ./wait-for-it.sh ${DB_HOST}:$DB_PORT -- java -jar hodebba.jar
