package com.hodebba.back.celebrity.persistence;

import com.hodebba.back.celebrity.models.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TopicDao extends JpaRepository<Topic, Long> {
	Optional<Topic> findByLabel(String label);
}
