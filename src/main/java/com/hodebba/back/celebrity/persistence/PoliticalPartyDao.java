package com.hodebba.back.celebrity.persistence;

import java.util.Optional;

import com.hodebba.back.celebrity.models.PoliticalParty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PoliticalPartyDao extends JpaRepository<PoliticalParty, Long> {
	Optional<PoliticalParty> findByLabel(String label);
}
