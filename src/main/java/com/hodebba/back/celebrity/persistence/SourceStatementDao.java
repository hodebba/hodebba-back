package com.hodebba.back.celebrity.persistence;

import com.hodebba.back.celebrity.models.SourceStatement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SourceStatementDao extends JpaRepository<SourceStatement, Long> {
}
