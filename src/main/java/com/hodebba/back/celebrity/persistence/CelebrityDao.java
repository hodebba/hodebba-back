package com.hodebba.back.celebrity.persistence;

import com.hodebba.back.celebrity.models.Celebrity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CelebrityDao extends JpaRepository<Celebrity, Long> {
	Optional<Celebrity> findByFullName(String fullName);
}
