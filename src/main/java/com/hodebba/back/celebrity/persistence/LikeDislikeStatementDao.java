package com.hodebba.back.celebrity.persistence;

import com.hodebba.back.celebrity.models.LikeDislikeStatement;
import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface LikeDislikeStatementDao extends JpaRepository<LikeDislikeStatement, Long> {

	Optional<LikeDislikeStatement> findByUserAndStatement(User user, Statement statement);

	@Query(value = "SELECT * FROM (public.like_dislike_statement l INNER JOIN public.statement s ON s.id = l.statement_id) INNER JOIN like_dislike d ON d.id=l.id WHERE d.user_id=?1", nativeQuery = true)
	Collection<LikeDislikeStatement> findByUserId(Long userId);
}