package com.hodebba.back.celebrity.persistence;

import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatementDao extends JpaRepository<Statement, Long> {
	// Get all statements of a celebrity sort by popularity
	@Query(value = "SELECT *, case when s.likes+s.dislikes > 0 then ((s.likes + 1.9208) / (s.likes + s.dislikes) - 01.96 * SQRT((s.likes * s.dislikes) / (s.likes + s.dislikes) + 0.9604) / (s.likes + s.dislikes)) / (1 + 3.8416 / (s.likes + s.dislikes)) else 0.01 end score FROM public.statement s INNER JOIN public.celebrity c ON s.celebrity_id = c.id WHERE c.id=?1 AND s.is_validated=true ORDER BY score DESC", nativeQuery = true)
	List<Statement> findAllValidatedStatementsByCelebrity(Long celebrityId);

	@Query(value = "SELECT * FROM public.statement s INNER JOIN public.celebrity c ON s.celebrity_id = c.id WHERE s.is_validated=false", nativeQuery = true)
	List<Statement> findAllNotValidatedStatements();

	// Get all statements of a debate sort by popularity
	@Query(value = "SELECT *, case when s.likes+s.dislikes > 0 then ((s.likes + 1.9208) / (s.likes + s.dislikes) - 01.96 * SQRT((s.likes * s.dislikes) / (s.likes + s.dislikes) + 0.9604) / (s.likes + s.dislikes)) / (1 + 3.8416 / (s.likes + s.dislikes)) else 0.01 end score FROM public.statement s INNER JOIN public.debate_statement d ON s.id = d.statement_id WHERE d.debate_id=?1 ORDER BY score DESC", nativeQuery = true)
	List<Statement> findAllByDebatesContains(Long debateId);

	Long countAllByUser(User user);

}
