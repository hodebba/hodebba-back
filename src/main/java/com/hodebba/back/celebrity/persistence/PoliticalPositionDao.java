package com.hodebba.back.celebrity.persistence;

import com.hodebba.back.celebrity.models.PoliticalPosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PoliticalPositionDao extends JpaRepository<PoliticalPosition, Long> {
        Optional<PoliticalPosition> findByLabel(String label);
}
