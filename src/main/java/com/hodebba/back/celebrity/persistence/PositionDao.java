package com.hodebba.back.celebrity.persistence;

import com.hodebba.back.celebrity.models.Celebrity;
import com.hodebba.back.celebrity.models.Position;
import com.hodebba.back.debate.models.Debate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PositionDao extends JpaRepository<Position, Long> {

	List<Position> findAllByCelebrity(Celebrity celebrity);
	List<Position> findAllByDebate(Debate debate);
}
