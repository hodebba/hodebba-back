package com.hodebba.back.celebrity.persistence;

import com.hodebba.back.celebrity.models.Profession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProfessionDao extends JpaRepository<Profession, Long> {
	Optional<Profession> findByLabel(String label);
}
