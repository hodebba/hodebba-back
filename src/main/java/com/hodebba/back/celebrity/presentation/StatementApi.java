package com.hodebba.back.celebrity.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.celebrity.dto.LikeDislikeStatementDto;
import com.hodebba.back.celebrity.dto.StatementDisplayDto;
import com.hodebba.back.celebrity.dto.StatementDto;
import com.hodebba.back.celebrity.dto.StatementUpdateDto;
import com.hodebba.back.celebrity.models.LikeDislikeStatement;
import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.celebrity.models.StatementType;
import com.hodebba.back.celebrity.services.LikeDislikeStatementService;
import com.hodebba.back.celebrity.services.StatementService;
import com.hodebba.back.post.models.LikeDislike;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/celebrities")
public class StatementApi {

	private static final Logger logger = LoggerFactory.getLogger(StatementApi.class);

	@Autowired
	StatementService statementService;

	@Autowired
	LikeDislikeStatementService likeDislikeStatementService;

	// ============== GET STATEMENT ============
	@GetMapping(value = "/{id}/statements", produces = "application/json")
	@ApiOperation(value = "Find statements by celebrity", notes = "Find statements by celebrity")
	@JsonView(Statement.Views.Full.class)
	public Map<Long, List<Statement>> getValidatedStatementsByCelebrityGroupByTopic(
			@ApiParam(value = "id of the celebrity to find", required = true) @PathVariable long id) {
		try {
			return statementService.getValidatedStatementsByCelebrityGroupByTopic(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	@GetMapping(value = "/statements", produces = "application/json")
	@ApiOperation(value = "Find all statements not validated", notes = "Find statements not validated")
	@JsonView(Statement.Views.Full.class)
	public List<Statement> getNotValidatedStatements() {
		try {
			return statementService.getNotValidatedStatements();
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	@GetMapping(value = "/statements/{id}", produces = "application/json")
	@ApiOperation(value = "Find statement by id", notes = "Find a statement by its id.")
	@JsonView(Statement.Views.Full.class)
	public Statement getStatement(
			@ApiParam(value = "id of the statement to find", required = true) @PathVariable long id) {
		try {
			return statementService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	@GetMapping(value = "/debates/{id}/statements", produces = "application/json")
	@ApiOperation(value = "Find statements by debates", notes = "Find statements by debates")
	@JsonView(StatementDisplayDto.Views.FULL.class)
	public Map<StatementType, List<StatementDisplayDto>> getStatementsByDebate(
			@ApiParam(value = "id of the debate to find", required = true) @PathVariable long id) {
		try {
			return statementService.getStatementsByDebate(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== GET LIKE / DISLIKE ==============
	// - by user
	@GetMapping(value = "/statements/likes", produces = "application/json")
	@ApiOperation(value = "Find User likes / dislikes by user", notes = "Find User likes / dislikes with the user id.")
	@JsonView(LikeDislikeStatement.Views.Light.class)
	public Collection<LikeDislikeStatement> getLikeDislikeByUserAndDebate(
			@ApiParam(value = "id of the User", required = true) @RequestParam Long userId) {
		try {
			return likeDislikeStatementService.findLikesByUser(userId);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== CREATE STATEMENT ============
	@PostMapping(value = "/statements", produces = "application/json")
	@ApiOperation(value = "Create statement", notes = "Create a statement.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Statement.Views.Full.class)
	public Statement create(
			@ApiParam(value = "StatementDto model", required = true) @RequestBody @Valid StatementDto statementDto) {
		try {
			Statement statement = statementService.create(statementDto);
			logger.info("A new Statement has been created : {}.", statement);
			return statement;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== UPDATE STATEMENT ============
	@PutMapping(value = "/statements", produces = "application/json")
	@ApiOperation(value = "Update Statement", notes = "Update data of a Statement.")
	@ApiImplicitParam(name = "Authorization", value = "Access token of an admin", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Statement.Views.Full.class)
	public Statement updateStatement(
			@ApiParam(value = "PositionUpdateDto model", required = true) @RequestBody @Valid StatementUpdateDto statementUpdateDto) {
		try {
			Statement statement = statementService.update(statementUpdateDto);
			logger.info("A statement has been updated : {}.", statement);
			return statement;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== CREATE OR UPDATE LIKE / DISLIKE STATEMENT =============
	@PostMapping(value = "/statements/likes", produces = "application/json")
	@ApiOperation(value = "Add like", notes = "Add a new like")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of the user", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(LikeDislike.Views.FULL.class)
	public LikeDislike createLikeDislike(
			@ApiParam(value = "LikeDislikeStatementDto model", required = true) @RequestBody @Valid LikeDislikeStatementDto likeDislikeStatementDto,
			final Locale locale, Principal principal) {
		return likeDislikeStatementService.create(likeDislikeStatementDto, locale, principal);
	}

	// ============== DELETE ============
	@DeleteMapping(value = "/statements/{id}", produces = "application/json")
	@ApiOperation(value = "Delete a statement by id", notes = "Delete a statement by its id.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public Long delete(@ApiParam(value = "id of the statement to delete", required = true) @PathVariable Long id) {
		try {
			statementService.delete(id);
			logger.info("The Statement with the id : {} has been deleted.", id);
			return id;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

}
