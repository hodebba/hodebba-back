package com.hodebba.back.celebrity.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.celebrity.dto.TopicDto;
import com.hodebba.back.celebrity.models.Topic;
import com.hodebba.back.celebrity.services.TopicService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/celebrities")
public class TopicApi {

	private static final Logger logger = LoggerFactory.getLogger(TopicApi.class);

	@Autowired
	TopicService topicService;

	// ============== CREATE TOPIC ============
	@PostMapping(value = "/topics", produces = "application/json")
	@ApiOperation(value = "Create topic", notes = "Create a new topic.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Topic.Views.Full.class)
	public Topic create(
			@ApiParam(value = "Label of the topic", required = true) @RequestBody @Valid TopicDto topicDto) {
		try {
			Topic topic = topicService.create(topicDto);
			logger.info("A new Topic has been created : {}.", topic);
			return topic;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== GET ============
	// - all
	@GetMapping(value = "/topics", produces = "application/json")
	@ApiOperation(value = "Find all Topics", notes = "Find the list of all Topic.")
	@JsonView(Topic.Views.Full.class)
	public List<Topic> findAll() {
		return topicService.findAll();
	}
}
