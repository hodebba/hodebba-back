package com.hodebba.back.celebrity.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.celebrity.dto.PositionUpdateDto;
import com.hodebba.back.celebrity.dto.ProfessionDto;
import com.hodebba.back.celebrity.dto.ProfessionUpdateDto;
import com.hodebba.back.celebrity.models.Position;
import com.hodebba.back.celebrity.models.Profession;
import com.hodebba.back.celebrity.services.ProfessionService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collection;

import java.util.Locale;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/celebrities")
public class ProfessionApi {

	private static final Logger logger = LoggerFactory.getLogger(ProfessionApi.class);

	@Autowired
	private final ProfessionService professionService;

	// ============== GET PROFESSION =============
	// - by id
	@GetMapping(value = "/professions/{id}", produces = "application/json")
	@ApiOperation(value = "Find Profession by id", notes = "Find a Profession by its id.")
	@JsonView(Profession.Views.Full.class)
	public Profession getProfessionById(
			@ApiParam(value = "id of the Profession to find", required = true) @PathVariable long id) {
		try {
			return professionService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - all
	@GetMapping(value = "/professions", produces = "application/json")
	@ApiOperation(value = "Find all Profession", notes = "Find the list of all Profession.")
	@JsonView(Profession.Views.Full.class)
	public Collection<Profession> findAllProfession() {
		return professionService.findAll();
	}

	// - all light
	@GetMapping(value = "/professions/light", produces = "application/json")
	@ApiOperation(value = "Find all Profession", notes = "Find the list of all Profession.")
	@JsonView(Profession.Views.Light.class)
	public Collection<Profession> findAllProfessionLight() {
		return professionService.findAll();
	}

	// ============== CREATE PROFESSION ============
	@PostMapping(value = "/professions", produces = "application/json")
	@ApiOperation(value = "Create Profession", notes = "Create a new Profession")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Profession.Views.Full.class)
	public Profession createProfession(
			@ApiParam(value = "ProfessionDto model", required = true) @RequestBody @Valid ProfessionDto professionDto) {
		Profession profession = professionService.create(professionDto.getLabel());
		logger.info("A new Profession has been created : {}.", profession);
		return profession;
	}

	// ============== UPDATE POSITION ============
	@PutMapping(value = "/professions", produces = "application/json")
	@ApiOperation(value = "Update Profession", notes = "Update Profession.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Profession.Views.Full.class)
	public Profession updateProfession(
			@ApiParam(value = "ProfessionUpdateDto model", required = true) @RequestBody @Valid ProfessionUpdateDto professionUpdateDto) {
		try {
			Profession profession = professionService.update(professionUpdateDto);
			logger.info("A Profession has been updated : {}.", profession);
			return profession;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== DELETE ============
	@DeleteMapping(value = "/professions/{id}", produces = "application/json")
	@ApiOperation(value = "Delete a profession by id", notes = "Delete a profession by its id.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void delete(@ApiParam(value = "id of the profession to delete", required = true) @PathVariable Long id) {
		try {
			professionService.delete(id);
			logger.info("The profession with the id : {} has been deleted.", id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

}
