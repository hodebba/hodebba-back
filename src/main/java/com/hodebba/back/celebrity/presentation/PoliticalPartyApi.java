package com.hodebba.back.celebrity.presentation;

import java.util.Collection;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import com.hodebba.back.celebrity.dto.PoliticalPartyDto;
import com.hodebba.back.celebrity.models.PoliticalParty;
import com.hodebba.back.celebrity.services.PoliticalPartyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import com.fasterxml.jackson.annotation.JsonView;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/celebrities")
public class PoliticalPartyApi {

	private static final Logger logger = LoggerFactory.getLogger(PoliticalPartyApi.class);

	@Autowired
	private final PoliticalPartyService politicalPartyService;

	// ============== GET POLITICAL PARTY =============
	// - by id
	@GetMapping(value = "/political-parties/{id}", produces = "application/json")
	@ApiOperation(value = "Find Political Party by id", notes = "Find a Political Party by its id.")
	@JsonView(PoliticalParty.Views.Full.class)
	public PoliticalParty getPoliticalPositionById(
			@ApiParam(value = "id of the Political Party to find", required = true) @PathVariable long id) {
		try {
			return politicalPartyService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - all
	@GetMapping(value = "/political-parties", produces = "application/json")
	@ApiOperation(value = "Find all Political Party", notes = "Find the list of all Political Party.")
	@JsonView(PoliticalParty.Views.Light.class)
	public Collection<PoliticalParty> findAll() {
		return politicalPartyService.findAll();
	}

	// ============== CREATE POLITICAL PARTY ============
	@PostMapping(value = "/political-parties", produces = "application/json")
	@ApiOperation(value = "Create Political Party", notes = "Create a new Political Party")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(PoliticalParty.Views.Full.class)
	public PoliticalParty create(
			@ApiParam(value = "PoliticalPartyDto model", required = true) @RequestBody @Valid PoliticalPartyDto politicalPartyDto) {
		PoliticalParty politicalParty = politicalPartyService.create(politicalPartyDto.getLabel());
		logger.info("A new Political Party has been created : {}.", politicalParty);
		return politicalParty;
	}
}
