package com.hodebba.back.celebrity.presentation;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.celebrity.dto.CelebrityDto;
import com.hodebba.back.celebrity.models.Celebrity;
import com.hodebba.back.celebrity.services.CelebrityService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
public class CelebrityApi {

	private static final Logger logger = LoggerFactory.getLogger(CelebrityApi.class);

	@Autowired
	CelebrityService celebrityService;

	// ============== GET CELEBRITY ============
	@GetMapping(value = "/celebrities/{id}", produces = "application/json")
	@ApiOperation(value = "Find celebrity by id", notes = "Find a celebrity by its id.")
	@JsonView(Celebrity.Views.Light.class)
	public Celebrity getCelebrity(
			@ApiParam(value = "id of the celebrity to find", required = true) @PathVariable long id) {
		try {
			return celebrityService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	@GetMapping(value = "/celebrities", produces = "application/json")
	@ApiOperation(value = "Find all celebrities", notes = "Find all celebrities")
	@JsonView(Celebrity.Views.Light.class)
	public List<Celebrity> getAllCelebrities() {
		try {
			return celebrityService.findAll();
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== CREATE CELEBRITY ============
	@PostMapping(value = "/celebrities", produces = "application/json")
	@ApiOperation(value = "Create celebrity", notes = "Create a new celebrity.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Celebrity.Views.Light.class)
	public Celebrity create(
			@ApiParam(value = "CelebrityDto model", required = true) @RequestBody @Valid final CelebrityDto celebrityDtoDto) {
		try {
			Celebrity celebrity = celebrityService.create(celebrityDtoDto);
			logger.info("A new Celebrity has been created : {}.", celebrity);
			return celebrity;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== UPDATE ES INDEX ============
	@PostMapping(value = "/celebrities/update-es", produces = "application/json")
	@ApiOperation(value = "Copy all celebrities to ES", notes = "Copy all celebrities to ES.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void updateEs() {
		try {
			celebrityService.sendAllCelebritiesToEs();
			logger.info("ES celebrities index has been updated.");
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== DELETE ============
	@DeleteMapping(value = "/celebrities/{id}", produces = "application/json")
	@ApiOperation(value = "Delete a Celebrity by id", notes = "Delete a Celebrity by its id.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void delete(@ApiParam(value = "id of the Celebrity to delete", required = true) @PathVariable Long id) {
		try {
			celebrityService.delete(id);
			logger.info("The Celebrity with the id : {} has been deleted.", id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== CLOUDINARY ============
	// Delete one picture of celebrities folder
	@DeleteMapping(value = "/cloudinary/celebrities/{id}", produces = "application/json")
	public void deleteImageCelebrities(@PathVariable String id) throws IOException {
		// TODO: Make it with Singleton and environment variable
		// private Cloudinary cloudinary = Singleton.getCloudinary();
		Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap("cloud_name", "dbqmtrilv", "api_key",
				"798681417477252", "api_secret", "mn06kGKRVW6XlZr3ctM5Z8ssnxY"));
		cloudinary.uploader().destroy("celebrities/" + id, ObjectUtils.emptyMap());
	}

}