package com.hodebba.back.celebrity.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.celebrity.dto.PositionDto;
import com.hodebba.back.celebrity.dto.PositionUpdateDto;
import com.hodebba.back.celebrity.models.Position;
import com.hodebba.back.celebrity.models.SummaryType;
import com.hodebba.back.celebrity.services.PositionService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/celebrities")
public class PositionApi {

	private static final Logger logger = LoggerFactory.getLogger(PositionApi.class);

	@Autowired
	PositionService positionService;

	// ============== GET POSITION ============
	@GetMapping(value = "/{id}/positions", produces = "application/json")
	@ApiOperation(value = "Find positions by celebrity", notes = "Find positions by celebrity")
	@JsonView(Position.Views.Full.class)
	public Map<Long, List<Position>> getPositionsByCelebrityGroupByTopic(
			@ApiParam(value = "id of the celebrity to find", required = true) @PathVariable long id) {
		try {
			return positionService.getPositionsByCelebrityGroupByTopic(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== GET POSITION ============
	@GetMapping(value = "/debates/{id}/positions", produces = "application/json")
	@ApiOperation(value = "Find positions by debate", notes = "Find positions by debate")
	@JsonView(Position.Views.Light.class)
	public Map<SummaryType, List<Position>> getPositionsByDebateGroupeByType(
			@ApiParam(value = "id of the debate to find", required = true) @PathVariable long id) {
		try {
			return positionService.getPositionsByDebateGroupeByType(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	@GetMapping(value = "/positions/{id}", produces = "application/json")
	@ApiOperation(value = "Find position by id", notes = "Find a position by its id.")
	@JsonView(Position.Views.Full.class)
	public Position getPosition(
			@ApiParam(value = "id of the position to find", required = true) @PathVariable long id) {
		try {
			return positionService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== CREATE POSITION ============
	@PostMapping(value = "/positions", produces = "application/json")
	@ApiOperation(value = "Create position", notes = "Create a position.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Position.Views.Full.class)
	public Position create(
			@ApiParam(value = "PositionDto model", required = true) @RequestBody @Valid PositionDto positionDto) {
		try {
			Position position = positionService.create(positionDto);
			logger.info("A new Position has been created : {}.", position);
			return position;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== UPDATE POSITION ============
	@PutMapping(value = "/positions", produces = "application/json")
	@ApiOperation(value = "Update Position", notes = "Update data of a Position.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Position.Views.Full.class)
	public Position updatePosition(
			@ApiParam(value = "PositionUpdateDto model", required = true) @RequestBody @Valid PositionUpdateDto positionUpdateDto,
			final Locale locale, Principal principal) {
		try {
			Position position = positionService.update(positionUpdateDto, locale, principal);
			logger.info("A position has been updated : {}.", position);
			return position;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== DELETE ============
	@DeleteMapping(value = "/positions/{id}", produces = "application/json")
	@ApiOperation(value = "Delete a Position by id", notes = "Delete a Position by its id.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void delete(@ApiParam(value = "id of the Position to delete", required = true) @PathVariable Long id) {
		try {
			positionService.delete(id);
			logger.info("The Position with the id : {} has been deleted.", id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

}
