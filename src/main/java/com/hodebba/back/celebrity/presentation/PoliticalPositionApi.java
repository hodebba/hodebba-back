package com.hodebba.back.celebrity.presentation;

import java.util.Collection;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import com.hodebba.back.celebrity.dto.PoliticalPositionDto;
import com.hodebba.back.celebrity.models.PoliticalPosition;
import com.hodebba.back.celebrity.services.PoliticalPositionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.annotation.JsonView;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/celebrities")
public class PoliticalPositionApi {

	private static final Logger logger = LoggerFactory.getLogger(PoliticalPositionApi.class);

	@Autowired
	private final PoliticalPositionService politicalPositionService;

	// ============== GET POLITICAL POSITION =============
	// - by id
	@GetMapping(value = "/political-positions/{id}", produces = "application/json")
	@ApiOperation(value = "Find Political Position by id", notes = "Find a Political Position by its id.")
	@JsonView(PoliticalPosition.Views.Full.class)
	public PoliticalPosition getPoliticalPositionById(
			@ApiParam(value = "id of the Political Position to find", required = true) @PathVariable long id) {
		try {
			return politicalPositionService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - all
	@GetMapping(value = "/political-positions", produces = "application/json")
	@ApiOperation(value = "Find all Political Position", notes = "Find the list of all Political Position.")
	@JsonView(PoliticalPosition.Views.Light.class)
	public Collection<PoliticalPosition> findAll() {
		return politicalPositionService.findAll();
	}

	// ============== CREATE POLITICAL POSITION ============
	@PostMapping(value = "/political-positions", produces = "application/json")
	@ApiOperation(value = "Create Political Position", notes = "Create a new Political Position")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(PoliticalPosition.Views.Full.class)
	public PoliticalPosition create(
			@ApiParam(value = "PoliticalPositionDto model", required = true) @RequestBody @Valid PoliticalPositionDto politicalPositionDto) {
		PoliticalPosition politicalPosition = politicalPositionService.create(politicalPositionDto.getLabel());
		logger.info("A new Political Position has been created : {}.", politicalPosition);
		return politicalPosition;
	}
}
