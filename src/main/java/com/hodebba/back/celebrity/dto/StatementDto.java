package com.hodebba.back.celebrity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

@Getter
@ApiModel(value = "StatementDto", description = "Model for creation of a new Statement, contain only necessary info.")
public class StatementDto {

	@Lob
	@NotNull
	@ApiModelProperty(notes = "Content of the statement", required = true, position = 0)
	private String content;

	@NotNull
	@ApiModelProperty(notes = "Sources of the statement (link)", required = true, position = 1)
	private Collection<String> sourceLinks;

	@NotNull
	@ApiModelProperty(notes = "Id of the topic related to this statement", required = true, position = 2)
	private Long topicId;

	@NotNull
	@ApiModelProperty(notes = "Id of the celebrity", required = true, position = 3)
	private Long celebrityId;

	@NotNull
	@ApiModelProperty(notes = "Id of the user who added this statement", required = true, position = 4)
	private Long userId;

	@ApiModelProperty(notes = "Optional debate id related to this statement", required = true, position = 5)
	private Long debateId;

	@ApiModelProperty(notes = "Date of statement", position = 6)
	private Date createdDate;

}
