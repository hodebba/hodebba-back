package com.hodebba.back.celebrity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@ApiModel(value = "TopicDto", description = "Model for creation of a new Topic, contain only necessary info.")
public class TopicDto {

	@NotNull
	@Size(min = 3)
	@ApiModelProperty(notes = "Label of the topic", required = true, position = 0)
	private String label;
}
