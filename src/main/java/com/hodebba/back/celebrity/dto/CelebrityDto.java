package com.hodebba.back.celebrity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@ApiModel(value = "CelebrityDto", description = "Model for creation of a new Celebrity, contain only necessary info.")
public class CelebrityDto {

	@NotNull
	@Size(min = 5)
	@ApiModelProperty(notes = "Full name of the celebrity", required = true, position = 0)
	private String fullName;

	@NotNull
	@ApiModelProperty(notes = "Celebrity photo cloudinary link", required = true, position = 1)
	private String imageLink;

	@NotNull
	@ApiModelProperty(notes = "Profession practiced by the celebrity", required = true, position = 2)
	private String profession;

	@ApiModelProperty(notes = "Political party of the celebrity", position = 3)
	private String politicalParty;

	@ApiModelProperty(notes = "Political position of the celebrity", required = true, position = 4)
	private String politicalPosition;

	@ApiModelProperty(notes = "Short description (in case this celebrity is not known by wikipedia) ", position = 5)
	private String shortDescription;

}
