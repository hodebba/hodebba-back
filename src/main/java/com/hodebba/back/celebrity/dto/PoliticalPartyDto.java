package com.hodebba.back.celebrity.dto;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@Getter
@ApiModel(value = "PoliticalPartyDto", description = "Model for creation of a new PoliticalParty, contain only necessary info.")
public class PoliticalPartyDto {

	@NotNull
	@ApiModelProperty(notes = "Name of the political party", required = true, position = 0)
	private String label;
}
