package com.hodebba.back.celebrity.dto;
import com.hodebba.back.celebrity.models.SummaryType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "PositionDto", description = "Model for creation of a new Position, contain only necessary info.")
public class PositionDto {

	@NotNull
	@ApiModelProperty(notes = "Position's question", required = true, position = 0)
	private String question;

	@NotNull
	@ApiModelProperty(notes = "Brief summary of the celebrity position (type)", required = true, position = 1)
	private SummaryType summaryType;

	@NotNull
	@ApiModelProperty(notes = "Brief summary of the celebrity position (content)", required = true, position = 2)
	private String summaryContent;

	@Lob
	@ApiModelProperty(notes = "Content of the position", required = false, position = 3)
	private String explanation;

	@NotNull
	@ApiModelProperty(notes = "Position's topic", required = true, position = 4)
	private Long topicId;

	@NotNull
	@ApiModelProperty(notes = "Label of the topic", required = true, position = 5)
	private Long celebrityId;

	@ApiModelProperty(notes = "Label of the topic", required = false, position = 6)
	private Long debateId;
}
