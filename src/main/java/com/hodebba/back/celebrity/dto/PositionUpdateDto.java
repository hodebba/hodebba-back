package com.hodebba.back.celebrity.dto;

import com.hodebba.back.celebrity.models.SummaryType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "PositionUpdateDto", description = "Model for updating of a new Position, contain only updatable info.")
public class PositionUpdateDto {

	@NotNull
	@ApiModelProperty(notes = "id of the position to update.", required = true, position = 0)
	private Long id;

	@ApiModelProperty(notes = "Brief summary of the celebrity position (type)", position = 1)
	private SummaryType summaryType;

	@ApiModelProperty(notes = "Brief summary of the celebrity position (content)", position = 2)
	private String summaryContent;

	@Lob
	@ApiModelProperty(notes = "Content of the position", position = 3)
	private String explanation;

	@ApiModelProperty(notes = "Label of the topic", position = 4)
	private Long debateId;
}
