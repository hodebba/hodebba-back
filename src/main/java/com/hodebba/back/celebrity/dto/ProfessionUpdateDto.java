package com.hodebba.back.celebrity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "ProfessionUpdateDto", description = "Model for updating of a Profession, contain only updatable info.")
public class ProfessionUpdateDto {

	@NotNull
	@ApiModelProperty(notes = "id of the profession to update.", required = true, position = 0)
	private Long id;

	@NotNull
	@ApiModelProperty(notes = "label of the profession to update.", required = true, position = 1)
	private String label;
}
