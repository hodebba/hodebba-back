package com.hodebba.back.celebrity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "LikeDislikeStatementDto", description = "Model for creation of a new LikeDislikeStatement")
public class LikeDislikeStatementDto {

	@NotNull
	@ApiModelProperty(notes = "Like or dislike", required = true, position = 0)
	private Boolean isLike;

	@NotNull
	@ApiModelProperty(notes = "Statement id of the like", required = true, position = 1)
	private Long statementId;

	@NotNull
	@ApiModelProperty(notes = "Id of the User who liked / disliked", required = true, position = 2)
	private Long userId;

}
