package com.hodebba.back.celebrity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "ProfessionDto", description = "Model for creation of a new Profession, contain only necessary info.")
public class ProfessionDto {

	@NotNull
	@ApiModelProperty(notes = "Name of the profession", required = true, position = 0)
	private String label;
}
