package com.hodebba.back.celebrity.dto;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@Getter
@ApiModel(value = "PoliticalPositionDto", description = "Model for creation of a new PoliticalPosition, contain only necessary info.")
public class PoliticalPositionDto {

	@NotNull
	@ApiModelProperty(notes = "Name of the political position", required = true, position = 0)
	private String label;
}
