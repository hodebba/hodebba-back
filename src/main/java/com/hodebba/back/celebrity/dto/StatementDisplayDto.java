package com.hodebba.back.celebrity.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.celebrity.models.*;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.post.models.*;
import com.hodebba.back.user.models.User;
import lombok.Getter;

import java.util.Collection;
import java.util.Date;

@Getter
@JsonView(StatementDisplayDto.Views.FULL.class)
public class StatementDisplayDto {
    private Long id;
    private Date createdDate;
    private Boolean isValidated;
    private StatementType statementType;
    private String content;
    private Collection<LikeDislike> likeDislikes;
    private int likes = 0;
    private int dislikes = 0;
    private Topic topic;
    private Celebrity celebrity;
    private User user;
    private Collection<Debate> debates;
    private Collection<SourceStatement> sources;
    private long rank;

    public StatementDisplayDto(Statement statement, long rank) {
        this.id = statement.getId();
        this.createdDate = statement.getCreatedDate();
        this.isValidated = statement.getIsValidated();
        this.statementType = statement.getStatementType();
        this.content = statement.getContent();
        this.likeDislikes = statement.getLikeDislikes();
        this.likes = statement.getLikes();
        this.dislikes = statement.getDislikes();
        this.topic = statement.getTopic();
        this.celebrity = statement.getCelebrity();
        this.user = statement.getUser();
        this.debates = statement.getDebates();
        this.sources = statement.getSources();
        this.rank = rank;
    }


    public static class Views {
        public interface FULL extends Statement.Views.Full {
        }
    }
}
