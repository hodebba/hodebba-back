package com.hodebba.back.celebrity.dto;

import com.hodebba.back.celebrity.models.StatementType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "StatementUpdateDto", description = "Model for updating of a Statement, contain only updatable info.")
public class StatementUpdateDto {

	@NotNull
	@ApiModelProperty(notes = "id of the statement to update.", required = true, position = 0)
	private Long id;

	@ApiModelProperty(notes = "This statement has been validated by an admin or not", position = 1)
	private Boolean isValidated;

	@ApiModelProperty(notes = "Type of statement (pro or con)", position = 2)
	private StatementType statementType;

	@Lob
	@ApiModelProperty(notes = "Content of the statement", position = 3)
	private String content;
}
