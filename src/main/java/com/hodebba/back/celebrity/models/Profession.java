package com.hodebba.back.celebrity.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity(name = "profession")
@Getter
public class Profession {

	@Id
	@SequenceGenerator(name = "profession_id_generator", sequenceName = "profession_id_seq", initialValue = 50)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profession_id_generator")
	@JsonView(Profession.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(Profession.Views.LABEL.class)
	private String label;

	@OneToMany(targetEntity = Celebrity.class, mappedBy = "profession")
	@JsonView(Profession.Views.CELEBRITIES.class)
	private Collection<Celebrity> celebrities;

	public Profession() {
	}

	public Profession(String label) {
		this.label = label;
	}

	public static class Views {
		private interface ID {
		}

		private interface LABEL {
		}

		private interface CELEBRITIES {
		}

		public interface Id extends ID {
		}

		public interface Light extends ID, LABEL {
		}

		public interface Full extends ID, LABEL, CELEBRITIES, Celebrity.Views.Light {
		}
	}

}