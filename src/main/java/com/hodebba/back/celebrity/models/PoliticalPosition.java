package com.hodebba.back.celebrity.models;

import java.util.Collection;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "political_position")
@Getter
public class PoliticalPosition {

	@Id
	@SequenceGenerator(name = "political_position_id_generator", sequenceName = "political_position_id_seq", initialValue = 50)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "political_position_id_generator")
	@JsonView(PoliticalPosition.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(PoliticalPosition.Views.LABEL.class)
	private String label;

	@OneToMany(targetEntity = Celebrity.class, mappedBy = "politicalPosition")
	@JsonView(PoliticalPosition.Views.CELEBRITIES.class)
	private Collection<Celebrity> celebrities;

	public PoliticalPosition() {
	}

	public PoliticalPosition(String label) {
		this.label = label;
	}

	public static class Views {
		private interface ID {
		}

		private interface LABEL {
		}

		private interface CELEBRITIES {
		}

		public interface Id extends ID {
		}

		public interface Light extends ID, LABEL {
		}

		public interface Full extends ID, LABEL, CELEBRITIES, Celebrity.Views.Light {
		}
	}

}