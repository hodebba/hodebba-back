package com.hodebba.back.celebrity.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.post.models.LikeDislike;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity(name = "statement")
@Getter
public class Statement {

	@Id
	@SequenceGenerator(name = "position_id_generator", sequenceName = "position_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "position_id_generator")
	@JsonView(Statement.Views.ID.class)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonView(Statement.Views.CREATED_DATE.class)
	private Date createdDate;

	@Setter
	private Boolean isValidated;

	@Setter
	@Enumerated(EnumType.STRING)
	@JsonView(Statement.Views.STATEMENT_TYPE.class)
	private StatementType statementType;

	@Setter
	@Lob
	@Type(type = "text")
	@JsonView(Statement.Views.CONTENT.class)
	private String content;

	@Setter
	@OneToMany(targetEntity = LikeDislikeStatement.class, mappedBy = "statement", cascade = CascadeType.REMOVE)
	@JsonView(Statement.Views.LIKES_DISLIKES.class)
	private Collection<LikeDislike> likeDislikes;

	@JsonView(Statement.Views.LIKES.class)
	private int likes = 0;

	@JsonView(Statement.Views.DISLIKES.class)
	private int dislikes = 0;

	@Setter
	@ManyToOne(targetEntity = Topic.class)
	@JoinColumn(name = "topic_id", referencedColumnName = "id")
	@JsonView(Statement.Views.TOPIC.class)
	private Topic topic;

	@ManyToOne(targetEntity = Celebrity.class)
	@JoinColumn(name = "celebrity_id", referencedColumnName = "id")
	@JsonView(Statement.Views.CELEBRITY.class)
	private Celebrity celebrity;

	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	@JsonView(Statement.Views.USER.class)
	private User user;

	@Setter
	@ManyToMany(targetEntity = Debate.class, mappedBy = "statements")
	@JsonView(Statement.Views.DEBATES.class)
	private Collection<Debate> debates;

	@Setter
	@OneToMany(targetEntity = SourceStatement.class, mappedBy = "statement", cascade = CascadeType.REMOVE)
	@JsonView(Statement.Views.SOURCES.class)
	private Collection<SourceStatement> sources;

	public Statement() {
	}

	public Statement(String content, Topic topic, Celebrity celebrity, User user, Date createdDate) {
		this.createdDate = createdDate;
		this.content = content;
		this.topic = topic;
		this.celebrity = celebrity;
		this.user = user;
		this.likes = 0;
		this.dislikes = 0;
		this.isValidated = false;
	}

	public static class Views {
		public interface ID {
		}
		private interface CREATED_DATE {
		}
		private interface LIKES_DISLIKES {
		}
		private interface LIKES {
		}
		private interface DISLIKES {
		}
		private interface TOPIC {
		}
		private interface CELEBRITY {
		}
		private interface USER {
		}
		private interface DEBATES {
		}
		private interface CONTENT {
		}
		private interface SOURCES {
		}
		private interface STATEMENT_TYPE {

		}

		public interface Full
				extends
					ID,
					CREATED_DATE,
					CONTENT,
					LIKES,
					DISLIKES,
					TOPIC,
					Topic.Views.Full,
					SOURCES,
					SourceStatement.Views.LIGHT,
					CELEBRITY,
					DEBATES,
					Debate.Views.Title,
					Celebrity.Views.Light,
					STATEMENT_TYPE,
					USER,
					User.Views.LIGHT {
		}
	}

	public void incLikes() {
		this.likes = likes + 1;
	}

	public void incDislikes() {
		this.dislikes = dislikes + 1;
	}

	public void decLikes() {
		this.likes = likes - 1;
	}

	public void decDislikes() {
		this.dislikes = dislikes - 1;
	}

}
