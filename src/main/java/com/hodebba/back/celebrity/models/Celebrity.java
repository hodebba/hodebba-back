package com.hodebba.back.celebrity.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Theme;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity(name = "celebrity")
@Getter
public class Celebrity {

	@Id
	@SequenceGenerator(name = "celebrity_id_generator", sequenceName = "celebrity_id_seq", initialValue = 200)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "celebrity_id_generator")
	@JsonView(Celebrity.Views.ID.class)
	private Long id;

	@Setter
	@ManyToOne(targetEntity = Profession.class)
	@JoinColumn(name = "profession_id", referencedColumnName = "id")
	@JsonView(Celebrity.Views.PROFESSION.class)
	Profession profession;

	@Setter
	@ManyToOne(targetEntity = PoliticalPosition.class)
	@JoinColumn(name = "political_position_id", referencedColumnName = "id")
	@JsonView(Celebrity.Views.POLITICAL_POSITION.class)
	PoliticalPosition politicalPosition;

	@Setter
	@ManyToOne(targetEntity = PoliticalParty.class)
	@JoinColumn(name = "political_party_id", referencedColumnName = "id")
	@JsonView(Celebrity.Views.POLITICAL_PARTY.class)
	PoliticalParty politicalParty;

	@Setter
	@JsonView(Celebrity.Views.FULL_NAME.class)
	private String fullName;

	@Setter
	@JsonView(Celebrity.Views.SHORT_DESCRIPTION.class)
	private String shortDescription;

	@Setter
	@JsonView(Celebrity.Views.IMAGE_LINK.class)
	private String imageLink;

	@OneToMany(targetEntity = Position.class, mappedBy = "celebrity", cascade = CascadeType.REMOVE)
	@JsonView(Celebrity.Views.POSITIONS.class)
	private Collection<Position> positions;

	@OneToMany(targetEntity = Statement.class, mappedBy = "celebrity", cascade = CascadeType.REMOVE)
	@JsonView(Celebrity.Views.STATEMENTS.class)
	private Collection<Statement> statements;

	public Celebrity() {
	}
	public Celebrity(String fullName, String shortDescription, String imageLink, Profession profession,
			PoliticalPosition politicalPosition, PoliticalParty politicalParty) {
		this.fullName = fullName;
		this.shortDescription = shortDescription;
		this.imageLink = imageLink;
		this.profession = profession;
		this.politicalParty = politicalParty;
		this.politicalPosition = politicalPosition;
	}

	@Override
	public String toString() {
		return "Celebrity{" + "id=" + id + ", fullName='" + fullName + '\'' + ", imageLink='" + imageLink + '\'' + '}';
	}

	public static class Views {
		public interface ID {
		}
		private interface FULL_NAME {
		}
		private interface PROFESSION {
		}
		private interface POLITICAL_PARTY {
		}
		private interface POLITICAL_POSITION {
		}
		private interface IMAGE_LINK {
		}
		private interface POSITIONS {
		}
		private interface STATEMENTS {
		}
		private interface SHORT_DESCRIPTION {

		}
		public interface Full
				extends
					ID,
					FULL_NAME,
					SHORT_DESCRIPTION,
					IMAGE_LINK,
					POSITIONS,
					STATEMENTS,
					POLITICAL_PARTY,
					PoliticalParty.Views.Light,
					POLITICAL_POSITION,
					PoliticalPosition.Views.Light {
		}

		public interface Light
				extends
					ID,
					FULL_NAME,
					SHORT_DESCRIPTION,
					IMAGE_LINK,
					POLITICAL_PARTY,
					PoliticalParty.Views.Light,
					POLITICAL_POSITION,
					PoliticalPosition.Views.Light {
		}

		public interface ForHome
				extends
					ID,
					SHORT_DESCRIPTION,
					FULL_NAME,
					IMAGE_LINK,
					PROFESSION,
					Profession.Views.Light {
		}

	}

}
