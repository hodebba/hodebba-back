package com.hodebba.back.celebrity.models;

public enum SummaryType {
	pro, neutral, con
}
