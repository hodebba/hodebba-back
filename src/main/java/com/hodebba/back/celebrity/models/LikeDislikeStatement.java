package com.hodebba.back.celebrity.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.post.models.LikeDislike;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "like_dislike_statement")
@Getter
public class LikeDislikeStatement extends LikeDislike {
	@Setter
	@ManyToOne(targetEntity = Statement.class)
	@JoinColumn(name = "statement_id", referencedColumnName = "id", unique = true)
	@JsonView(LikeDislikeStatement.Views.STATEMENT.class)
	private Statement statement;

	public LikeDislikeStatement() {
	}

	public static class Views {
		private interface STATEMENT {
		}

		public interface Full extends LikeDislike.Views.FULL, STATEMENT, Statement.Views.ID {
		}
		public interface Light extends LikeDislike.Views.LIGHT, STATEMENT, Statement.Views.ID {
		}
	}

	@Override
	public String toString() {
		return "LikeDislikeStatement{id=" + super.getId() + ", user=" + super.getUser().getId() + ", isLike="
				+ super.getIsLike() + "statement=" + statement.getId() + '}';
	}
}