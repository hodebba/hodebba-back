package com.hodebba.back.celebrity.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "topic")
@Getter
public class Topic {

	@Id
	@SequenceGenerator(name = "topic_id_generator", sequenceName = "topic_id_seq", initialValue = 50)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "topic_id_generator")
	@JsonView(Topic.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(Topic.Views.LABEL.class)
	private String label;

	public Topic() {
	}

	public Topic(String label) {
		this.label = label;
	}

	public static class Views {
		private interface ID {
		}
		private interface LABEL {
		}

		public interface Full extends ID, LABEL {
		}
	}
}
