package com.hodebba.back.celebrity.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Debate;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "position")
@Getter
public class Position {

	@Id
	@SequenceGenerator(name = "position_id_generator", sequenceName = "position_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "position_id_generator")
	@JsonView(Position.Views.ID.class)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonView(Position.Views.CREATED_DATE.class)
	private Date createdDate;

	@Setter
	@JsonView(Position.Views.QUESTION.class)
	private String question;

	@Setter
	@Enumerated(EnumType.STRING)
	@JsonView(Position.Views.SUMMARY_TYPE.class)
	private SummaryType summaryType;

	@Setter
	@JsonView(Position.Views.SUMMARY_CONTENT.class)
	private String summaryContent;

	@Setter
	@Lob
	@Type(type = "text")
	@JsonView(Position.Views.EXPLANATION.class)
	private String explanation;

	@Setter
	@ManyToOne(targetEntity = Topic.class)
	@JoinColumn(name = "topic_id", referencedColumnName = "id")
	@JsonView(Position.Views.TOPIC.class)
	private Topic topic;

	@ManyToOne(targetEntity = Celebrity.class)
	@JoinColumn(name = "celebrity_id", referencedColumnName = "id")
	@JsonView(Position.Views.CELEBRITY.class)
	private Celebrity celebrity;

	@Setter
	@ManyToOne(targetEntity = Debate.class)
	@JoinColumn(name = "debate_id", referencedColumnName = "id")
	@JsonView(Position.Views.DEBATE.class)
	private Debate debate;

	public Position() {
	}

	public Position(String question, SummaryType summarytype, String summaryContent, String explanation, Topic topic,
			Celebrity celebrity) {
		this.createdDate = new Date();
		this.question = question;
		this.summaryType = summarytype;
		this.summaryContent = summaryContent;
		this.explanation = explanation;
		this.topic = topic;
		this.celebrity = celebrity;
	}

	public static class Views {
		private interface ID {
		}
		private interface CREATED_DATE {
		}
		private interface QUESTION {
		}
		private interface SUMMARY_TYPE {
		}
		private interface SUMMARY_CONTENT {
		}
		private interface EXPLANATION {
		}
		private interface TOPIC {
		}
		private interface CELEBRITY {
		}
		private interface DEBATE {
		}

		public interface Full
				extends
					ID,
					CREATED_DATE,
					QUESTION,
					SUMMARY_CONTENT,
					SUMMARY_TYPE,
					EXPLANATION,
					TOPIC,
					Topic.Views.Full,
					CELEBRITY,
					Celebrity.Views.Light,
					DEBATE,
					Debate.Views.Id {
		}

		public interface Light extends ID, CELEBRITY, Celebrity.Views.Light {
		}

	}

}
