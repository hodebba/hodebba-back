package com.hodebba.back.celebrity.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.post.models.Post;
import com.hodebba.back.post.models.Source;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "source_statement")
@Getter
public class SourceStatement extends Source {

	@Setter
	@ManyToOne(targetEntity = Statement.class)
	@JoinColumn(name = "statement_id", referencedColumnName = "id")
	@JsonView(SourceStatement.Views.STATEMENT.class)
	private Statement statement;

	public SourceStatement() {
	}
	public SourceStatement(String link, Statement statement) {
		super(link);
		this.statement = statement;
	}

	public static class Views {
		private interface STATEMENT {
		}
		public interface LIGHT extends Source.Views.LIGHT {
		}
	}
}