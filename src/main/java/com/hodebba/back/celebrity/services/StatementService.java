package com.hodebba.back.celebrity.services;

import com.hodebba.back.celebrity.dto.StatementDisplayDto;
import com.hodebba.back.celebrity.dto.StatementDto;
import com.hodebba.back.celebrity.dto.StatementUpdateDto;
import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.celebrity.models.StatementType;
import com.hodebba.back.user.models.User;

import java.util.List;
import java.util.Map;

public interface StatementService {
	Statement create(StatementDto statementDto);
	void delete(Long id);
	Statement findById(Long id);
	Map<Long, List<Statement>> getValidatedStatementsByCelebrityGroupByTopic(Long celebrityId);
	List<Statement> getNotValidatedStatements();
	Map<StatementType, List<StatementDisplayDto>> getStatementsByDebate(Long debateId);
	void incLikes(Statement statement);
	void decLikes(Statement statement);
	void incDislikes(Statement statement);
	void decDislikes(Statement statement);
	Statement update(StatementUpdateDto statementUpdateDto);
	Long getStatementsByUser(User user);
}
