package com.hodebba.back.celebrity.services.impl;

import com.hodebba.back.celebrity.dto.TopicDto;
import com.hodebba.back.celebrity.models.Topic;
import com.hodebba.back.celebrity.persistence.TopicDao;
import com.hodebba.back.celebrity.services.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class TopicServiceImpl implements TopicService {

	@Autowired
	TopicDao topicDao;

	@Override
	@Transactional
	public Topic create(TopicDto topicDto) {
		Optional<Topic> optTopic = topicDao.findByLabel(topicDto.getLabel());
		if (optTopic.isPresent()) {
			return optTopic.get();
		} else {
			Topic topic = new Topic(topicDto.getLabel());
			return topicDao.save(topic);
		}
	}

	@Override
	public List<Topic> findAll() {
		return topicDao.findAll();
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO: delete depedencies
		topicDao.deleteById(id);
	}

	@Override
	public Topic findById(Long id) {
		Optional<Topic> optTopic = topicDao.findById(id);
		if (optTopic.isPresent()) {
			return optTopic.get();
		} else {
			throw new NoSuchElementException("No topic with this id:" + id);
		}
	}
}
