package com.hodebba.back.celebrity.services;

import com.hodebba.back.celebrity.dto.LikeDislikeStatementDto;
import com.hodebba.back.celebrity.models.LikeDislikeStatement;
import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.user.models.User;

import java.security.Principal;
import java.util.Collection;
import java.util.Locale;

public interface LikeDislikeStatementService {
	LikeDislikeStatement create(LikeDislikeStatementDto likeDislikeStatementDto, Locale locale, Principal principal);
	LikeDislikeStatement findByUserAndStatement(User user, Statement statement);
	Collection<LikeDislikeStatement> findLikesByUser(Long userId);
}
