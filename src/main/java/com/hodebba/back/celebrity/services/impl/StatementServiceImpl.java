package com.hodebba.back.celebrity.services.impl;

import com.hodebba.back.celebrity.dto.StatementDisplayDto;
import com.hodebba.back.celebrity.dto.StatementDto;
import com.hodebba.back.celebrity.dto.StatementUpdateDto;
import com.hodebba.back.celebrity.models.*;
import com.hodebba.back.celebrity.persistence.StatementDao;
import com.hodebba.back.celebrity.services.CelebrityService;
import com.hodebba.back.celebrity.services.SourceStatementService;
import com.hodebba.back.celebrity.services.StatementService;
import com.hodebba.back.celebrity.services.TopicService;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
public class StatementServiceImpl implements StatementService {

	@Lazy
	@Autowired
	UserService userService;

	@Autowired
	DebateService debateService;

	@Autowired
	CelebrityService celebrityService;

	@Autowired
	TopicService topicService;

	@Autowired
	SourceStatementService sourceStatementService;

	@Autowired
	StatementDao statementDao;

	@Override
	@Transactional
	public Statement create(StatementDto statementDto) {
		User user = userService.findById(statementDto.getUserId());
		Celebrity celebrity = celebrityService.findById(statementDto.getCelebrityId());
		Topic topic = topicService.findById(statementDto.getTopicId());

		Statement statement = new Statement(statementDto.getContent(), topic, celebrity, user,
				statementDto.getCreatedDate());
		statement = statementDao.save(statement);

		if (statementDto.getDebateId() != null) {
			Debate debate = debateService.findById(statementDto.getDebateId());
			debateService.saveStatement(statement, debate);
			List<Debate> debates = new ArrayList<>();
			debates.add(debate);
			statement.setDebates(debates);
		}
		Collection<SourceStatement> sources = saveSources(statementDto.getSourceLinks(), statement);
		statement.setSources(sources);
		return statement;
	}

	@Override
	public Statement findById(Long id) {
		Optional<Statement> optStatement = statementDao.findById(id);
		if (optStatement.isPresent()) {
			return optStatement.get();
		} else {
			throw new NoSuchElementException("No statement with this id:" + id);
		}
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Statement statement = findById(id);
		statement.getDebates().forEach(debate -> debateService.deleteStatement(debate, statement));
		statementDao.delete(statement);
	}

	@Override
	public Map<Long, List<Statement>> getValidatedStatementsByCelebrityGroupByTopic(Long celebrityId) {
		List<Statement> statements = statementDao.findAllValidatedStatementsByCelebrity(celebrityId);
		return statements.stream().collect(groupingBy(statement -> statement.getTopic().getId()));
	}

	@Override
	public List<Statement> getNotValidatedStatements() {
		return statementDao.findAllNotValidatedStatements();
	}

	@Override
	@Transactional
	public void incLikes(Statement statement) {
		statement.incLikes();
		statementDao.save(statement);
	}

	@Override
	@Transactional
	public void decLikes(Statement statement) {
		statement.decLikes();
		statementDao.save(statement);
	}

	@Override
	@Transactional
	public void incDislikes(Statement statement) {
		statement.incDislikes();
		statementDao.save(statement);
	}

	@Override
	@Transactional
	public void decDislikes(Statement statement) {
		statement.decDislikes();
		statementDao.save(statement);
	}

	private Collection<SourceStatement> saveSources(Collection<String> sourceLinks, Statement statement) {
		Collection<SourceStatement> sources = new ArrayList<>();
		if (sourceLinks != null) {
			for (String sourceLink : sourceLinks) {
				sources.add(sourceStatementService.create(sourceLink, statement));
			}
		}
		return sources;
	}

	@Override
	@Transactional
	public Statement update(StatementUpdateDto statementUpdateDto) {
		Optional<Statement> optStatement = statementDao.findById(statementUpdateDto.getId());
		if (optStatement.isPresent()) {
			Statement statement = optStatement.get();

			// update isValidated
			if (statementUpdateDto.getIsValidated() != null) {
				statement.setIsValidated(true);
			}

			// update statement type
			if (statementUpdateDto.getStatementType() != null) {
				statement.setStatementType(statementUpdateDto.getStatementType());
			}

			// update statement content
			if (statementUpdateDto.getContent() != null) {
				statement.setContent(statementUpdateDto.getContent());
			}

			return statementDao.save(statement);
		} else {
			throw new NoSuchElementException("No statement found with this id: " + statementUpdateDto.getId());
		}
	}

	@Override
	public Map<StatementType, List<StatementDisplayDto>> getStatementsByDebate(Long debateId) {
		List<Statement> statements = statementDao.findAllByDebatesContains(debateId).stream().filter(element -> element.getStatementType() != null).collect(Collectors.toList());

		Map<StatementType, List<StatementDisplayDto>> ret = new HashMap<>();

		List<StatementDisplayDto> proList = new ArrayList<>();
		List<StatementDisplayDto> conList = new ArrayList<>();

		for (int i = 0; i < statements.size(); i++) {
			if(statements.get(i).getStatementType().equals(StatementType.pro)){
				proList.add(new StatementDisplayDto(statements.get(i), i));
			}else {
				conList.add(new StatementDisplayDto(statements.get(i), i));
			}
		}
		ret.put(StatementType.pro, proList);
		ret.put(StatementType.con, conList);

		return ret;
	}

	@Override
	public Long getStatementsByUser(User user) {
		return statementDao.countAllByUser(user);
	}

}
