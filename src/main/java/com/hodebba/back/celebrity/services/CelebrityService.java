package com.hodebba.back.celebrity.services;

import com.hodebba.back.celebrity.dto.CelebrityDto;
import com.hodebba.back.celebrity.models.Celebrity;
import com.hodebba.back.celebrity.models.Profession;

import java.util.List;
import java.util.Map;

public interface CelebrityService {
	Celebrity create(CelebrityDto celebrityDto);
	Celebrity findById(Long id);
	Celebrity findByFullName(String fullName);
	void delete(Long id);
	List<Celebrity> findAll();
	void sendAllCelebritiesToEs();
}
