package com.hodebba.back.celebrity.services;

import com.hodebba.back.celebrity.dto.TopicDto;
import com.hodebba.back.celebrity.models.Topic;

import java.util.List;

public interface TopicService {
	Topic create(TopicDto topicDto);
	Topic findById(Long id);
	void delete(Long id);
	List<Topic> findAll();
}
