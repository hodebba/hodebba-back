package com.hodebba.back.celebrity.services.impl;

import com.hodebba.back.celebrity.dto.PositionDto;
import com.hodebba.back.celebrity.dto.PositionUpdateDto;
import com.hodebba.back.celebrity.models.*;
import com.hodebba.back.celebrity.persistence.PositionDao;
import com.hodebba.back.celebrity.services.CelebrityService;
import com.hodebba.back.celebrity.services.PositionService;
import com.hodebba.back.celebrity.services.TopicService;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.services.DebateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;

@Service
public class PositionServiceImpl implements PositionService {

	@Autowired
	TopicService topicService;

	@Autowired
	CelebrityService celebrityService;

	@Autowired
	DebateService debateService;

	@Autowired
	PositionDao positionDao;

	@Override
	@Transactional
	public Position create(PositionDto positionDto) {
		Topic topic = topicService.findById(positionDto.getTopicId());
		Celebrity celebrity = celebrityService.findById(positionDto.getCelebrityId());

		Position position = new Position(positionDto.getQuestion(), positionDto.getSummaryType(),
				positionDto.getSummaryContent(), positionDto.getExplanation(), topic, celebrity);

		if (positionDto.getDebateId() != null) {
			Debate debate = debateService.findById(positionDto.getDebateId());
			position.setDebate(debate);
		}

		return positionDao.save(position);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		positionDao.deleteById(id);
	}

	@Override
	public Map<Long, List<Position>> getPositionsByCelebrityGroupByTopic(Long celebrityId) {
		Celebrity celebrity = celebrityService.findById(celebrityId);
		List<Position> positions = positionDao.findAllByCelebrity(celebrity);
		return positions.stream().collect(groupingBy(position -> position.getTopic().getId()));
	}

	@Override
	public Position findById(Long id) {
		Optional<Position> optPosition = positionDao.findById(id);
		if (optPosition.isPresent()) {
			return optPosition.get();
		} else {
			throw new NoSuchElementException("No position with this id:" + id);
		}
	}

	@Override
	public Map<SummaryType, List<Position>> getPositionsByDebateGroupeByType(Long debateId) {
		Debate debate = debateService.findById(debateId);
		List<Position> positions = positionDao.findAllByDebate(debate);
		return positions.stream().collect(groupingBy(position -> position.getSummaryType()));
	}

	@Override
	@Transactional
	public Position update(PositionUpdateDto positionUpdateDto, Locale locale, Principal principal) {
		Optional<Position> optPosition = positionDao.findById(positionUpdateDto.getId());
		if (optPosition.isPresent()) {
			Position position = optPosition.get();

			// update summary type
			if (positionUpdateDto.getSummaryType() != null) {
				if (!positionUpdateDto.getSummaryType().equals(position.getSummaryType())) {
					position.setSummaryType(positionUpdateDto.getSummaryType());
				}
			}

			// update summary content
			if (positionUpdateDto.getSummaryContent() != null) {
				if (!positionUpdateDto.getSummaryContent().equals(position.getSummaryContent())) {
					position.setSummaryContent(positionUpdateDto.getSummaryContent());
				}
			}

			// update explanation
			if (positionUpdateDto.getExplanation() != null) {
				if (!positionUpdateDto.getExplanation().equals(position.getExplanation())) {
					position.setExplanation(positionUpdateDto.getExplanation());
				}
			}

			// update debate
			if (positionUpdateDto.getDebateId() != null) {
				Debate debate = debateService.findById(positionUpdateDto.getId());
				position.setDebate(debate);

			}
			return positionDao.save(position);
		} else {

			throw new NoSuchElementException("No position found with this id: " + positionUpdateDto.getId());

		}
	}
}
