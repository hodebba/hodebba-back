package com.hodebba.back.celebrity.services;

import com.hodebba.back.celebrity.dto.ProfessionUpdateDto;
import com.hodebba.back.celebrity.models.Profession;

import java.util.Collection;

public interface ProfessionService {
	Profession findById(Long id);
	Profession findByLabel(String label);
	Collection<Profession> findAll();
	Profession create(String label);
	Profession update(ProfessionUpdateDto professionUpdateDto);
	void delete(Long id);
}
