package com.hodebba.back.celebrity.services.impl;

import com.hodebba.back.celebrity.dto.ProfessionUpdateDto;
import com.hodebba.back.celebrity.models.Position;
import com.hodebba.back.celebrity.models.Profession;
import com.hodebba.back.celebrity.persistence.ProfessionDao;
import com.hodebba.back.celebrity.services.ProfessionService;
import com.hodebba.back.debate.models.Debate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ProfessionServiceImpl implements ProfessionService {

	@Autowired
	ProfessionDao professionDao;

	@Override
	public List<Profession> findAll() {
		return professionDao.findAll();
	}

	@Override
	public Profession findById(Long id) {
		Optional<Profession> optProfession = professionDao.findById(id);
		if (optProfession.isPresent()) {
			return optProfession.get();
		} else {
			throw new NoSuchElementException("No profession with this id:" + id);
		}
	}

	@Override
	public Profession findByLabel(String label) {
		Optional<Profession> optProfession = professionDao.findByLabel(label);
		if (optProfession.isPresent()) {
			return optProfession.get();
		} else {
			throw new NoSuchElementException("No profession with this label:" + label);
		}
	}

	@Override
	@Transactional
	public Profession create(String label) {
		Optional<Profession> optProfession = professionDao.findByLabel(label);
		if (optProfession.isPresent()) {
			return optProfession.get();
		} else {
			Profession profession = new Profession(label);
			return professionDao.save(profession);
		}
	}

	@Override
	@Transactional
	public Profession update(ProfessionUpdateDto professionUpdateDto) {
		Optional<Profession> optProfession = professionDao.findById(professionUpdateDto.getId());
		if (optProfession.isPresent()) {
			Profession profession = optProfession.get();
			profession.setLabel(professionUpdateDto.getLabel());
			return professionDao.save(profession);
		} else {
			throw new NoSuchElementException("No position found with this id: " + professionUpdateDto.getId());
		}
	}

	@Override
	@Transactional
	public void delete(Long id) {
		professionDao.deleteById(id);
	}
}
