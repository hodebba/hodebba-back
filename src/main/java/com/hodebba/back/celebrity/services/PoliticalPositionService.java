package com.hodebba.back.celebrity.services;

import java.util.Collection;

import com.hodebba.back.celebrity.models.PoliticalPosition;

public interface PoliticalPositionService {
	PoliticalPosition findById(Long id);
	PoliticalPosition findByLabel(String label);
	Collection<PoliticalPosition> findAll();
	PoliticalPosition create(String label);
}
