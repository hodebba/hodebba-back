package com.hodebba.back.celebrity.services.impl;

import com.hodebba.back.celebrity.models.SourceStatement;
import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.celebrity.persistence.SourceStatementDao;
import com.hodebba.back.celebrity.services.SourceStatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class SourceStatementServiceImpl implements SourceStatementService {

	@Autowired
	SourceStatementDao sourceStatementDao;

	@Override
	@Transactional
	public SourceStatement create(String link, Statement statement) {
		// Statement verification is made before calling this function
		return sourceStatementDao.save(new SourceStatement(link, statement));
	}
}
