package com.hodebba.back.celebrity.services.impl;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.transaction.Transactional;

import com.hodebba.back.celebrity.models.PoliticalParty;
import com.hodebba.back.celebrity.persistence.PoliticalPartyDao;
import com.hodebba.back.celebrity.services.PoliticalPartyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PoliticalPartyServiceImpl implements PoliticalPartyService {

	@Autowired
	PoliticalPartyDao politicalPartyDao;

	@Override
	public List<PoliticalParty> findAll() {
		return politicalPartyDao.findAll();
	}

	@Override
	public PoliticalParty findById(Long id) {
		Optional<PoliticalParty> optPoliticalParty = politicalPartyDao.findById(id);
		if (optPoliticalParty.isPresent()) {
			return optPoliticalParty.get();
		} else {
			throw new NoSuchElementException("No Political Party  with this id:" + id);
		}
	}

	@Override
	public PoliticalParty findByLabel(String label) {
		Optional<PoliticalParty> optPoliticalParty = politicalPartyDao.findByLabel(label);
		if (optPoliticalParty.isPresent()) {
			return optPoliticalParty.get();
		} else {
			throw new NoSuchElementException("No Political Party  with this label:" + label);
		}
	}

	@Override
	@Transactional
	public PoliticalParty create(String label) {
		Optional<PoliticalParty> optPoliticalParty = politicalPartyDao.findByLabel(label);
		if (optPoliticalParty.isPresent()) {
			return optPoliticalParty.get();
		} else {
			PoliticalParty politicalParty = new PoliticalParty(label);
			return politicalPartyDao.save(politicalParty);
		}
	}
}
