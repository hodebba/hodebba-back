package com.hodebba.back.celebrity.services.impl;

import com.hodebba.back.celebrity.dto.LikeDislikeStatementDto;
import com.hodebba.back.celebrity.models.LikeDislikeStatement;
import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.celebrity.persistence.LikeDislikeStatementDao;
import com.hodebba.back.celebrity.services.LikeDislikeStatementService;
import com.hodebba.back.celebrity.services.StatementService;
import com.hodebba.back.post.dto.LikeDislikePostDto;
import com.hodebba.back.post.models.LikeDislike;
import com.hodebba.back.post.models.LikeDislikePost;
import com.hodebba.back.post.models.Post;
import com.hodebba.back.post.persistence.LikeDislikeDao;
import com.hodebba.back.post.services.PostService;
import com.hodebba.back.security.utils.TokenChecker;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class LikeDislikeStatementImpl implements LikeDislikeStatementService {

	@Lazy
	@Autowired
	UserService userService;

	@Autowired
	PostService postService;

	@Autowired
	StatementService statementService;

	@Autowired
	LikeDislikeStatementDao likeDislikeStatementDao;

	@Autowired
	LikeDislikeDao likeDislikeDao;

	@Override
	@Transactional
	public LikeDislikeStatement create(LikeDislikeStatementDto likeDislikeStatementDto, Locale locale,
			Principal principal) {
		User user = userService.findById(likeDislikeStatementDto.getUserId());
		TokenChecker.checkUserToken(locale, principal, user);
		Statement statement = statementService.findById(likeDislikeStatementDto.getStatementId());
		LikeDislikeStatement likeDislike = findByUserAndStatement(user, statement);

		if (likeDislike == null) {
			likeDislike = new LikeDislikeStatement();
			likeDislike.setStatement(statement);
			likeDislike.setUser(user);

			if (likeDislikeStatementDto.getIsLike()) {
				statementService.incLikes(statement);
			} else {
				statementService.incDislikes(statement);
			}
		} else {
			if (likeDislikeStatementDto.getIsLike() && !likeDislike.getIsLike()) {
				statementService.incLikes(statement);
				statementService.decDislikes(statement);
			} else if (!likeDislikeStatementDto.getIsLike() && likeDislike.getIsLike()) {
				statementService.incDislikes(statement);
				statementService.decLikes(statement);
			}
		}

		likeDislike.setIsLike(likeDislikeStatementDto.getIsLike());

		return likeDislikeDao.save(likeDislike);
	}

	@Override
	public LikeDislikeStatement findByUserAndStatement(User user, Statement statement) {
		Optional<LikeDislikeStatement> optLikeDislikeStatement = likeDislikeStatementDao.findByUserAndStatement(user,
				statement);
		if (optLikeDislikeStatement.isPresent()) {
			return optLikeDislikeStatement.get();
		} else {
			return null;
		}
	}

	@Override
	public Collection<LikeDislikeStatement> findLikesByUser(Long userId) {
		return likeDislikeStatementDao.findByUserId(userId);
	}

}
