package com.hodebba.back.celebrity.services;

import com.hodebba.back.celebrity.dto.PositionDto;
import com.hodebba.back.celebrity.dto.PositionUpdateDto;
import com.hodebba.back.celebrity.models.Position;
import com.hodebba.back.celebrity.models.SummaryType;

import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface PositionService {
	Position create(PositionDto positionDto);
	Position findById(Long id);
	void delete(Long id);
	Map<Long, List<Position>> getPositionsByCelebrityGroupByTopic(Long celebrityId);
	Map<SummaryType, List<Position>> getPositionsByDebateGroupeByType(Long debateId);
	Position update(PositionUpdateDto positionUpdateDto, Locale locale, Principal principal);
}
