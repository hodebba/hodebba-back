package com.hodebba.back.celebrity.services.impl;

import com.hodebba.back.celebrity.dto.CelebrityDto;
import com.hodebba.back.celebrity.models.Celebrity;
import com.hodebba.back.celebrity.models.PoliticalParty;
import com.hodebba.back.celebrity.models.PoliticalPosition;
import com.hodebba.back.celebrity.models.Profession;
import com.hodebba.back.celebrity.persistence.CelebrityDao;
import com.hodebba.back.celebrity.presentation.CelebrityApi;
import com.hodebba.back.celebrity.services.CelebrityService;
import com.hodebba.back.celebrity.services.PoliticalPartyService;
import com.hodebba.back.celebrity.services.PoliticalPositionService;
import com.hodebba.back.celebrity.services.ProfessionService;
import lombok.SneakyThrows;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class CelebrityServiceImpl implements CelebrityService {

	@Autowired
	CelebrityDao celebrityDao;

	@Autowired
	ProfessionService professionService;

	@Autowired
	PoliticalPositionService politicalPositionService;

	@Autowired
	PoliticalPartyService politicalPartyService;

	@Autowired
	private RestHighLevelClient client;

	private static final Logger logger = LoggerFactory.getLogger(CelebrityApi.class);

	@Override
	@Transactional
	public Celebrity create(CelebrityDto celebrityDto) {
		Optional<Celebrity> optCelebrity = celebrityDao.findByFullName(celebrityDto.getFullName());
		if (optCelebrity.isPresent()) {
			return optCelebrity.get();
		}
		Profession profession = professionService.findByLabel(celebrityDto.getProfession());
		PoliticalParty politicalParty = null;
		PoliticalPosition politicalPosition = null;

		if (celebrityDto.getPoliticalParty() != null) {
			politicalParty = politicalPartyService.findByLabel(celebrityDto.getPoliticalParty());
		}

		if (celebrityDto.getPoliticalPosition() != null) {
			politicalPosition = politicalPositionService.findByLabel(celebrityDto.getPoliticalPosition());
		}

		Celebrity celebrity = celebrityDao
				.save(new Celebrity(celebrityDto.getFullName(), celebrityDto.getShortDescription(),
						celebrityDto.getImageLink(), profession, politicalPosition, politicalParty));

		if (!checkIndexExists("celebrities")) {
			createCelebritiesIndex();
		}

		sendCelebrityToES(celebrity);
		return celebrity;
	}

	@Override
	public void sendAllCelebritiesToEs() {
		List<Celebrity> celebrities = celebrityDao.findAll();

		if (!checkIndexExists("celebrities")) {
			createCelebritiesIndex();
		}

		celebrities.forEach(celebrity -> {
			sendCelebrityToES(celebrity);
		});
	}

	@SneakyThrows
	private void sendCelebrityToES(Celebrity celebrity) {
		Map<String, Object> jsonMap = new HashMap<>();
		jsonMap.put("fullName", celebrity.getFullName());

		if (celebrity.getPoliticalParty() != null) {
			jsonMap.put("politicalParty", celebrity.getPoliticalParty().getLabel());
		}

		if (celebrity.getPoliticalPosition() != null) {
			jsonMap.put("politicalPosition", celebrity.getPoliticalPosition().getLabel());
		}

		jsonMap.put("profession", celebrity.getProfession().getLabel());
		jsonMap.put("imageLink", celebrity.getImageLink());

		IndexRequest indexRequest = new IndexRequest("celebrities").id(celebrity.getId().toString()).source(jsonMap);
		IndexResponse indexResponse = null;

		indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);

		logger.info("A new Celebrity has been sent to ES : {}.", indexResponse);
	}

	@SneakyThrows
	private boolean checkIndexExists(String index) {
		GetIndexRequest request = new GetIndexRequest(index);
		return client.indices().exists(request, RequestOptions.DEFAULT);
	}

	@SneakyThrows
	private void createCelebritiesIndex() {
		CreateIndexRequest request = new CreateIndexRequest("celebrities");
		Map<String, Object> text = new HashMap<>();
		text.put("type", "text");

		Map<String, Object> properties = new HashMap<>();
		properties.put("fullName", text);
		properties.put("politicalParty", text);
		properties.put("politicalPosition", text);
		properties.put("imageLink", text);
		properties.put("profession", text);

		Map<String, Object> mapping = new HashMap<>();
		mapping.put("properties", properties);
		request.mapping(mapping);
		CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
		logger.info("A new ES index {} has been created.", createIndexResponse);
	}

	@Override
	public Celebrity findById(Long id) {
		Optional<Celebrity> optCelebrity = celebrityDao.findById(id);
		if (optCelebrity.isPresent()) {
			return optCelebrity.get();
		} else {
			throw new NoSuchElementException("No celebrity with this id:" + id);
		}
	}

	@Override
	public Celebrity findByFullName(String fullName) {
		Optional<Celebrity> optCelebrity = celebrityDao.findByFullName(fullName);
		if (optCelebrity.isPresent()) {
			return optCelebrity.get();
		} else {
			throw new NoSuchElementException("No celebrity with this fullName:" + fullName);
		}
	}

	@Override
	public List<Celebrity> findAll() {
		return celebrityDao.findAll();
	}

	@Override
	@Transactional
	public void delete(Long id) {
		celebrityDao.deleteById(id);
	}

}
