package com.hodebba.back.celebrity.services;

import com.hodebba.back.celebrity.models.SourceStatement;
import com.hodebba.back.celebrity.models.Statement;

public interface SourceStatementService {
	SourceStatement create(String link, Statement statement);
}
