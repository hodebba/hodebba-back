package com.hodebba.back.celebrity.services.impl;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import javax.transaction.Transactional;
import com.hodebba.back.celebrity.models.PoliticalPosition;
import com.hodebba.back.celebrity.persistence.PoliticalPositionDao;
import com.hodebba.back.celebrity.services.PoliticalPositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PoliticalPositionServiceImpl implements PoliticalPositionService {

	@Autowired
	PoliticalPositionDao politicalPositionDao;

	@Override
	public List<PoliticalPosition> findAll() {
		return politicalPositionDao.findAll();
	}

	@Override
	public PoliticalPosition findById(Long id) {
		Optional<PoliticalPosition> optPoliticalPosition = politicalPositionDao.findById(id);
		if (optPoliticalPosition.isPresent()) {
			return optPoliticalPosition.get();
		} else {
			throw new NoSuchElementException("No Political Position  with this id:" + id);
		}
	}

	@Override
	public PoliticalPosition findByLabel(String label) {
		Optional<PoliticalPosition> optPoliticalPosition = politicalPositionDao.findByLabel(label);
		if (optPoliticalPosition.isPresent()) {
			return optPoliticalPosition.get();
		} else {
			throw new NoSuchElementException("No Political Position with this label:" + label);
		}
	}

	@Override
	@Transactional
	public PoliticalPosition create(String label) {
		Optional<PoliticalPosition> optPoliticalPosition = politicalPositionDao.findByLabel(label);
		if (optPoliticalPosition.isPresent()) {
			return optPoliticalPosition.get();
		} else {
			PoliticalPosition politicalPosition = new PoliticalPosition(label);
			return politicalPositionDao.save(politicalPosition);
		}
	}
}
