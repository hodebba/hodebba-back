package com.hodebba.back.celebrity.services;

import java.util.Collection;

import com.hodebba.back.celebrity.models.PoliticalParty;

public interface PoliticalPartyService {
	PoliticalParty findById(Long id);
	PoliticalParty findByLabel(String label);
	Collection<PoliticalParty> findAll();
	PoliticalParty create(String label);
}
