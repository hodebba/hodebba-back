package com.hodebba.back.post.models;

import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.user.models.User;

import javax.persistence.*;

@Entity(name = "testimonial")
public class Testimonial extends Post {

	public Testimonial() {
	}

	public Testimonial(String title, String content, User user, Debate debate) {
		super(title, content, user, debate);
	}

	@Override
	public String toString() {
		return "Testimonial{" + "id=" + super.getId() + ", title='" + super.getTitle() + '\'' + ", createdDate="
				+ super.getCreatedDate() + ", content='" + super.getContent() + '\'' + ", user="
				+ super.getUser().getId();
	}
}
