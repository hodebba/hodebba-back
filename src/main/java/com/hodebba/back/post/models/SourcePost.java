package com.hodebba.back.post.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "source_post")
@Getter
public class SourcePost extends Source {

	@Setter
	@ManyToOne(targetEntity = Post.class)
	@JoinColumn(name = "post_id", referencedColumnName = "id")
	@JsonView(SourcePost.Views.POST.class)
	private Post post;

	public SourcePost() {
	}
	public SourcePost(String link, Post post) {
		super(link);
		this.post = post;
	}

	public static class Views {
		private interface POST {
		}
		public interface LIGHT extends Source.Views.LIGHT {
		}
	}
}
