package com.hodebba.back.post.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "comment")
@Getter
public class Comment extends Post {

	@Setter
	@ManyToOne(targetEntity = Post.class)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	@JsonIgnoreProperties({"comments", "title", "user", "createdDate", "content", "parent", "isLastLevel", "postType",
			"likes", "dislikes", "sources"})
	@JsonView(Comment.Views.PARENT.class)
	private Post parent;

	@Setter
	@JsonView(Comment.Views.IS_LAST_LEVEL.class)
	private Boolean isLastLevel = false;

	public Comment() {
	}

	public Comment(String title, String content, User user, Debate debate, Post parent, Boolean isLastLevel) {
		super(title, content, user, debate);
		this.parent = parent;
		this.isLastLevel = isLastLevel;
	}

	public Comment(String title, String content, User user, Debate debate, Post parent) {
		super(title, content, user, debate);
		this.parent = parent;
	}

	public static class Views {
		private interface PARENT {
		}

		public interface IS_LAST_LEVEL {
		}

		public interface FULL extends PARENT, IS_LAST_LEVEL {
		}

	}

	@Override
	public String toString() {
		return "Comment{" + "id=" + super.getId() + ", title='" + super.getTitle() + '\'' + ", createdDate="
				+ super.getCreatedDate() + ", content='" + super.getContent() + '\'' + ", user="
				+ super.getUser().getId() + ",debate=" + super.getDebate().getId() + "parent=" + parent.getId() + '}';
	}
}
