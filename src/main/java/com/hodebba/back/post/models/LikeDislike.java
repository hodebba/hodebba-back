package com.hodebba.back.post.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "like_dislike")
@Getter
@Inheritance(strategy = InheritanceType.JOINED)
public class LikeDislike {

	@Id
	@SequenceGenerator(name = "like_dislike_id_generator", sequenceName = "like_dislike_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "like_dislike_id_generator")
	@JsonView(LikeDislike.Views.ID.class)
	private Long id;

	@Setter
	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	@JsonView(LikeDislike.Views.USER.class)
	private User user;

	@Setter
	@JsonView(LikeDislike.Views.IS_LIKE.class)
	private Boolean isLike;

	public LikeDislike() {
	}

	@Override
	public String toString() {
		return "LikeDislike{" + "id=" + id + ", user=" + user.getId() + ", isLike=" + isLike + '}';
	}

	public static class Views {
		private interface ID {
		}
		private interface USER {
		}
		private interface IS_LIKE {
		}

		public interface FULL extends ID, USER, User.Views.ID, IS_LIKE {
		}
		public interface LIGHT extends ID, IS_LIKE {
		}
	}

}
