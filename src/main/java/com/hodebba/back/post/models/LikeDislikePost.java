package com.hodebba.back.post.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "like_dislike_post")
@Getter
public class LikeDislikePost extends LikeDislike {

	@Setter
	@ManyToOne(targetEntity = Post.class)
	@JoinColumn(name = "post_id", referencedColumnName = "id", unique = true)
	@JsonView(LikeDislikePost.Views.POST.class)
	private Post post;

	public LikeDislikePost() {
	}

	public static class Views {
		private interface POST {
		}

		public interface Full extends LikeDislike.Views.FULL, POST, Post.Views.Id {
		}
		public interface Light extends LikeDislike.Views.LIGHT, POST, Post.Views.Id {
		}
	}

	@Override
	public String toString() {
		return "LikeDislikePost{id=" + super.getId() + ", user=" + super.getUser().getId() + ", isLike="
				+ super.getIsLike() + "post=" + post.getId() + '}';
	}
}
