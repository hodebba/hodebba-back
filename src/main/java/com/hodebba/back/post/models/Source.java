package com.hodebba.back.post.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "source")
@Getter
@Inheritance(strategy = InheritanceType.JOINED)
public class Source {

	@Id
	@SequenceGenerator(name = "source_id_generator", sequenceName = "source_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "source_id_generator")
	@JsonView(Source.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(Source.Views.LINK.class)
	private String link;

	public Source() {
	}

	public Source(String link) {
		this.link = link;
	}

	public static class Views {
		private interface ID {
		}
		private interface LINK {
		}

		public interface LIGHT extends ID, LINK {
		}
	}

}
