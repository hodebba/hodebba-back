package com.hodebba.back.post.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.*;

@Entity(name = "post")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
public class Post {

	@Id
	@SequenceGenerator(name = "post_id_generator", sequenceName = "post_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_id_generator")
	@JsonView(Post.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(Post.Views.TITLE.class)
	private String title;

	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	@JsonView(Post.Views.CREATED_DATE.class)
	private Date createdDate;

	@Setter
	@OneToMany(targetEntity = LikeDislikePost.class, mappedBy = "post", cascade = CascadeType.REMOVE)
	@JsonView(Post.Views.LIKES_DISLIKES.class)
	private Collection<LikeDislikePost> likeDislike;

	private long likes = 0;

	private long dislikes = 0;

	@Setter
	@Lob
	@Type(type = "text")
	@JsonView(Post.Views.CONTENT.class)
	private String content;

	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	@JsonView(Post.Views.USER.class)
	private User user;

	@Getter
	@Setter
	@OneToMany(targetEntity = Comment.class, mappedBy = "parent", cascade = CascadeType.REMOVE)
	@JsonView(Post.Views.COMMENTS.class)
	private Collection<Comment> comments;

	@ManyToOne(targetEntity = Debate.class)
	@JoinColumn(name = "debate_id", referencedColumnName = "id")
	@JsonView(Post.Views.DEBATE.class)
	private Debate debate;

	public Post() {
	}

	public Post(String title, String content, User user, Debate debate) {
		this.title = title;
		this.user = user;
		this.createdDate = new Date();
		this.content = content;
		this.debate = debate;
	}

	public static class Views {
		private interface ID {
		}
		private interface TITLE {
		}
		private interface USER {
		}
		private interface CREATED_DATE {
		}
		private interface CONTENT {
		}
		private interface COMMENTS {
		}
		private interface LIKES_DISLIKES {
		}
		private interface DEBATE {
		}

		public interface Id extends ID {
		}
		public interface FULL
				extends
					ID,
					TITLE,
					USER,
					User.Views.LIGHT,
					CREATED_DATE,
					CONTENT,
					DEBATE,
					Debate.Views.Id,
					COMMENTS,
					Comment.Views.FULL,
					Argument.Views.FULL,
					ArgumentComment.Views.FULL {
		}
		public interface LIGHT extends ID, DEBATE, Debate.Views.Id {
		}

		public interface ForDashboard
				extends
					ID,
					TITLE,
					USER,
					User.Views.LIGHT,
					CREATED_DATE,
					CONTENT,
					DEBATE,
					COMMENTS,
					Comment.Views.IS_LAST_LEVEL,
					Debate.Views.Title,
					Argument.Views.FULL {
		}

	}

	public void incLikes() {
		this.likes = likes + 1;
	}

	public void incDislikes() {
		this.dislikes = dislikes + 1;
	}

	public void decLikes() {
		this.likes = likes - 1;
	}

	public void decDislikes() {
		this.dislikes = dislikes - 1;
	}

	@Override
	public String toString() {
		return "Post{" + "id=" + id + ", title='" + title + '\'' + ", createdDate=" + createdDate + ", content='"
				+ content + '\'' + ", user=" + user.getId() + ", debate=" + debate.getId() + '}';
	}

}
