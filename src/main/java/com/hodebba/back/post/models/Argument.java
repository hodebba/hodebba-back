package com.hodebba.back.post.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity(name = "argument")
@JsonView(Argument.Views.FULL.class)
@Getter
public class Argument extends Post {

	@Setter
	@Enumerated(EnumType.STRING)
	@JsonView(Argument.Views.POST_TYPE.class)
	private ArgumentType postType;

	@Setter
	@OneToMany(targetEntity = SourcePost.class, mappedBy = "post", cascade = CascadeType.REMOVE)
	@JsonView(Argument.Views.SOURCES.class)
	private Collection<SourcePost> sources;

	public Argument() {
	}

	public Argument(String title, String content, User user, Debate debate, ArgumentType postType) {
		super(title, content, user, debate);
		this.postType = postType;
	}

	public static class Views {
		private interface POST_TYPE {
		}
		private interface SOURCES {
		}

		public interface FULL extends SOURCES, SourcePost.Views.LIGHT, POST_TYPE {
		}

	}

	@Override
	public String toString() {
		return "Argument{" + "id=" + super.getId() + ", title='" + super.getTitle() + '\'' + ", createdDate="
				+ super.getCreatedDate() + ", content='" + super.getContent() + '\'' + ", user="
				+ super.getUser().getId() + "postType=" + postType + '}';
	}
}
