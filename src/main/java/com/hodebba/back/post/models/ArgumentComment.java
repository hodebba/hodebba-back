package com.hodebba.back.post.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity(name = "argument_comment")
@Getter
public class ArgumentComment extends Comment {

	@Setter
	@Enumerated(EnumType.STRING)
	@JsonView(ArgumentComment.Views.POST_TYPE.class)
	private CommentType postType;

	@Setter
	@OneToMany(targetEntity = SourcePost.class, mappedBy = "post", cascade = CascadeType.REMOVE)
	@JsonView(ArgumentComment.Views.SOURCES.class)
	private Collection<SourcePost> sources;

	public ArgumentComment(String title, String content, User user, Debate debate, Post parent, CommentType postType) {
		super(title, content, user, debate, parent);
		this.postType = postType;
	}

	public ArgumentComment() {
	}

	public static class Views {

		private interface POST_TYPE {
		}

		private interface SOURCES {
		}

		public interface FULL extends POST_TYPE, SOURCES, SourcePost.Views.LIGHT {
		}

	}
}
