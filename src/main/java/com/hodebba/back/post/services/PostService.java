package com.hodebba.back.post.services;

import com.hodebba.back.debate.models.Debate;

import com.hodebba.back.post.dto.DashboardDto;
import com.hodebba.back.post.dto.UpdatePostDto;
import com.hodebba.back.post.models.*;

import java.security.Principal;
import java.util.Collection;
import java.util.Locale;

public interface PostService {
	Post findPostById(long id);
	void deletePostById(long id, Locale locale, Principal principal);
	void incLikes(Post post);
	void decLikes(Post post);
	void incDislikes(Post post);
	void decDislikes(Post post);
	void deleteAllByDebate(Debate debate);
	Collection<SourcePost> saveSources(Collection<String> sourceLinks, Post post);
	DashboardDto findPostsDataByUser(Long id, Locale locale, Principal principal);
	Post update(UpdatePostDto UudatePostDto);
}
