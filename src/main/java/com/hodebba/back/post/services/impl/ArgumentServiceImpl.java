package com.hodebba.back.post.services.impl;

import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.services.DebateService;
//import com.hodebba.back.debate.services.ElasticSearchService;
import com.hodebba.back.post.dto.ArgumentDto;
import com.hodebba.back.post.models.Argument;
import com.hodebba.back.post.models.ArgumentType;
import com.hodebba.back.post.models.SourcePost;
import com.hodebba.back.post.persistence.ArgumentDao;
import com.hodebba.back.post.services.ArgumentService;
import com.hodebba.back.post.services.PostService;
import com.hodebba.back.security.utils.TokenChecker;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;

@Service
public class ArgumentServiceImpl implements ArgumentService {

	@Autowired
	private ArgumentDao argumentDao;

	@Autowired
	private DebateService debateService;

	@Lazy
	@Autowired
	private UserService userService;

	@Autowired
	private PostService postService;

	// @Autowired
	// private ElasticSearchService elasticSearchService;

	@Override
	@Transactional
	public Argument createArgument(ArgumentDto argumentDto, Locale locale, Principal principal) {
		User user = userService.findById(argumentDto.getUserId());
		TokenChecker.checkUserToken(locale, principal, user);
		Debate debate = debateService.findById(argumentDto.getDebateId());

		Argument argument = new Argument(argumentDto.getTitle(), argumentDto.getContent(), user, debate,
				argumentDto.getPostType());
		argument = argumentDao.save(argument);

		Collection<SourcePost> sources = postService.saveSources(argumentDto.getSourceLinks(), argument);
		argument.setSources(sources);

		// elasticSearchService.updateDebateES(argument.getDebate());

		return argument;
	}

	@Override
	public Map<ArgumentType, List<Argument>> findByDebateGroupByType(Long debateId) {
		Optional<Collection<Argument>> optArguments = argumentDao.findByDebate(debateId);
		Collection<Argument> arguments;
		if (optArguments.isPresent()) {
			arguments = optArguments.get();
		} else {
			return null;
		}
		return arguments.stream().collect(groupingBy(Argument::getPostType));
	}

	@Override
	public Collection<Argument> findByDebate(Long debateId) {
		Optional<Collection<Argument>> optArguments = argumentDao.findByDebate(debateId);
		if (optArguments.isPresent()) {
			return optArguments.get();
		} else {
			return null;
		}
	}

	@Override
	public List<Argument> findAllArguments() {
		return this.argumentDao.findAll();
	}

}
