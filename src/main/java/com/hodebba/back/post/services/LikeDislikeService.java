package com.hodebba.back.post.services;

import com.hodebba.back.post.dto.LikeDislikePostDto;
import com.hodebba.back.post.models.LikeDislike;
import com.hodebba.back.post.models.LikeDislikePost;
import com.hodebba.back.post.models.Post;
import com.hodebba.back.user.models.User;

import java.security.Principal;
import java.util.Collection;
import java.util.Locale;

public interface LikeDislikeService {
	LikeDislike create(LikeDislikePostDto likeDislikePostDto, Locale locale, Principal principal);
	LikeDislike findByUserAndPost(User user, Post post);
	Collection<LikeDislikePost> findLikeDislikeByUserAndDebate(Long userId, Long debateId);
	void delete(Long id, Locale locale, Principal principal);
}
