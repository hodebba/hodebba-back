package com.hodebba.back.post.services.impl;

import com.hodebba.back.debate.dto.TestimonialDto;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.post.models.Testimonial;
import com.hodebba.back.post.persistence.CommentDao;
import com.hodebba.back.post.persistence.TestimonialDao;
import com.hodebba.back.post.services.TestimonialService;
import com.hodebba.back.security.utils.TokenChecker;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class TestimonialServiceImpl implements TestimonialService {

	@Autowired
	private TestimonialDao testimonialDao;

	@Lazy
	@Autowired
	private UserService userService;

	@Autowired
	private DebateService debateService;

	@Override
	@Transactional
	public Testimonial createTestimonial(TestimonialDto testimonialDto, Locale locale, Principal principal) {
		User user = userService.findById(testimonialDto.getUserId());
		TokenChecker.checkUserToken(locale, principal, user);
		Debate debate = debateService.findById(testimonialDto.getDebateId());

		Testimonial testimonial = new Testimonial(testimonialDto.getTitle(), testimonialDto.getContent(), user, debate);
		return testimonialDao.save(testimonial);
	}

	@Override
	public Collection<Testimonial> findTestimonialsByDebateId(long debateId) {
		Optional<Collection<Testimonial>> optTestimonials = testimonialDao.findByDebate(debateId);
		Collection<Testimonial> testimonials;
		if (optTestimonials.isPresent()) {
			testimonials = optTestimonials.get();
		} else {
			return null;
		}
		return testimonials;
	}

	@Override
	public List<Testimonial> findAllTestimonials() {
		return this.testimonialDao.findAll();
	}
}
