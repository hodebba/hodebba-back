package com.hodebba.back.post.services;

import com.hodebba.back.debate.dto.TestimonialDto;
import com.hodebba.back.post.models.Testimonial;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public interface TestimonialService {

	List<Testimonial> findAllTestimonials();
	Testimonial createTestimonial(TestimonialDto testimonialDto, Locale locale, Principal principal);
	Collection<Testimonial> findTestimonialsByDebateId(long debateId);
}
