package com.hodebba.back.post.services;

import com.hodebba.back.post.dto.ArgumentCommentDto;
import com.hodebba.back.post.dto.CommentDto;
import com.hodebba.back.post.models.ArgumentComment;
import com.hodebba.back.post.models.Comment;

import java.security.Principal;
import java.util.Locale;

public interface CommentService {
	Comment createComment(CommentDto commentDto, Locale locale, Principal principal);
	ArgumentComment createArgumentComment(ArgumentCommentDto argumentCommentDto, Locale locale, Principal principal);
	Comment findCommentById(long id);
}
