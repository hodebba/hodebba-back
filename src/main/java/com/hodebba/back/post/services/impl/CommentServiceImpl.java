package com.hodebba.back.post.services.impl;

import com.hodebba.back.post.dto.ArgumentCommentDto;
import com.hodebba.back.post.dto.CommentDto;
import com.hodebba.back.post.models.*;
import com.hodebba.back.post.persistence.CommentDao;
import com.hodebba.back.post.services.CommentService;
import com.hodebba.back.post.services.PostService;
import com.hodebba.back.security.utils.TokenChecker;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

	@Lazy
	@Autowired
	private UserService userService;

	@Autowired
	private PostService postService;

	@Autowired
	private CommentDao commentDao;

	static final String noPostFoundMessage = "No com.hodebba.back.post with this id:";

	@Override
	@Transactional
	public Comment createComment(CommentDto commentDto, Locale locale, Principal principal) {
		User user = userService.findById(commentDto.getUserId());
		TokenChecker.checkUserToken(locale, principal, user);
		Post parent = postService.findPostById(commentDto.getParentId());

		if (parent.getClass().getSimpleName().equals("Argument")
				|| parent.getClass().getSimpleName().equals("ArgumentComment")) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"Impossible to create simple comment with this kind of post.");
		}

		Comment comment = new Comment(commentDto.getTitle(), commentDto.getContent(), user, parent.getDebate(), parent);
		checkIsLastLevel(comment, parent);
		comment = commentDao.save(comment);
		return comment;
	}

	@Override
	@Transactional
	public ArgumentComment createArgumentComment(ArgumentCommentDto argumentCommentDto, Locale locale,
			Principal principal) {
		User user = userService.findById(argumentCommentDto.getUserId());
		TokenChecker.checkUserToken(locale, principal, user);
		Post parent = postService.findPostById(argumentCommentDto.getParentId());

		if (parent.getClass().getSimpleName().equals("Testimonial")
				|| parent.getClass().getSimpleName().equals("Comment")) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"Impossible to comment of argument with this kind of post.");
		}

		ArgumentComment comment = new ArgumentComment(argumentCommentDto.getTitle(), argumentCommentDto.getContent(),
				user, parent.getDebate(), parent, argumentCommentDto.getPostType());

		checkIsLastLevel(comment, parent);
		comment = commentDao.save(comment);
		Collection<SourcePost> sources = postService.saveSources(argumentCommentDto.getSourceLinks(), comment);
		comment.setSources(sources);
		return comment;
	}

	private Comment checkIsLastLevel(Comment comment, Post parent) {
		if (parent.getClass().getSimpleName().equals("Comment")
				|| parent.getClass().getSimpleName().equals("ArgumentComment")) {
			comment.setIsLastLevel(true);
			Comment parentComment = (Comment) parent;

			if (parentComment.getIsLastLevel()) {
				parentComment = findCommentById(parentComment.getParent().getId());
				comment.setParent(parentComment);
			}
		}
		return comment;
	}

	@Override
	public Comment findCommentById(long id) {
		Optional<Comment> optComment = commentDao.findById(id);
		if (optComment.isPresent()) {
			return optComment.get();
		} else {
			throw new NoSuchElementException(noPostFoundMessage + id);
		}
	}
}
