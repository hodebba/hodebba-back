package com.hodebba.back.post.services.impl;

import com.hodebba.back.celebrity.models.LikeDislikeStatement;
import com.hodebba.back.celebrity.services.StatementService;
import com.hodebba.back.post.dto.LikeDislikePostDto;
import com.hodebba.back.post.models.LikeDislike;
import com.hodebba.back.post.models.LikeDislikePost;
import com.hodebba.back.post.models.Post;
import com.hodebba.back.post.persistence.LikeDislikeDao;
import com.hodebba.back.post.persistence.LikeDislikePostDao;
import com.hodebba.back.post.services.LikeDislikeService;
import com.hodebba.back.post.services.PostService;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import com.hodebba.back.security.utils.TokenChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class LikeDislikeServiceImpl implements LikeDislikeService {

	@Autowired
	private LikeDislikeDao likeDislikeDao;

	@Autowired
	private LikeDislikePostDao likeDislikePostDao;

	@Lazy
	@Autowired
	private UserService userService;

	@Autowired
	private PostService postService;

	@Autowired
	private StatementService statementService;

	@Override
	@Transactional
	public LikeDislikePost create(LikeDislikePostDto likeDislikePostDto, Locale locale, Principal principal) {
		User user = userService.findById(likeDislikePostDto.getUserId());
		TokenChecker.checkUserToken(locale, principal, user);
		Post post = postService.findPostById(likeDislikePostDto.getPostId());
		LikeDislikePost likeDislike = findByUserAndPost(user, post);

		if (likeDislike == null) {
			likeDislike = new LikeDislikePost();
			likeDislike.setPost(post);
			likeDislike.setUser(user);

			if (likeDislikePostDto.getIsLike()) {
				postService.incLikes(post);
			} else {
				postService.incDislikes(post);
			}
		} else {
			if (likeDislikePostDto.getIsLike() && !likeDislike.getIsLike()) {
				postService.incLikes(post);
				postService.decDislikes(post);
			} else if (!likeDislikePostDto.getIsLike() && likeDislike.getIsLike()) {
				postService.incDislikes(post);
				postService.decLikes(post);
			}
		}

		likeDislike.setIsLike(likeDislikePostDto.getIsLike());

		return likeDislikeDao.save(likeDislike);
	}

	@Override
	public LikeDislikePost findByUserAndPost(User user, Post post) {
		Optional<LikeDislikePost> optLikeDislikePost = likeDislikePostDao.findByUserAndPost(user, post);
		if (optLikeDislikePost.isPresent()) {
			return optLikeDislikePost.get();
		} else {
			return null;
		}
	}

	public Collection<LikeDislikePost> findLikeDislikeByUserAndDebate(Long userId, Long debateId) {
		return likeDislikePostDao.findByUserIdAndDebateId(userId, debateId);
	}

	private LikeDislike findById(Long id) {
		Optional<LikeDislike> optLikeDislike = likeDislikeDao.findById(id);
		if (optLikeDislike.isPresent()) {
			return optLikeDislike.get();
		} else {
			throw new NoSuchElementException("No LikeDislike found with this id:" + id);
		}
	}

	@Override
	@Transactional
	public void delete(Long id, Locale locale, Principal principal) {
		LikeDislike likeDislike = findById(id);
		TokenChecker.checkUserToken(locale, principal, likeDislike.getUser());

		if (likeDislike.getIsLike()) {
			if (likeDislike.getClass().getSimpleName().equals("LikeDislikePost")) {
				LikeDislikePost likeDislikePost = (LikeDislikePost) likeDislike;
				postService.decLikes(likeDislikePost.getPost());
			} else {
				LikeDislikeStatement likeDislikeStatement = (LikeDislikeStatement) likeDislike;
				statementService.decLikes(likeDislikeStatement.getStatement());
			}
		} else {
			if (likeDislike.getClass().getSimpleName().equals("LikeDislikePost")) {
				LikeDislikePost likeDislikePost = (LikeDislikePost) likeDislike;
				postService.decDislikes(likeDislikePost.getPost());
			} else {
				LikeDislikeStatement likeDislikeStatement = (LikeDislikeStatement) likeDislike;
				statementService.decDislikes(likeDislikeStatement.getStatement());
			}
		}

		likeDislikeDao.delete(likeDislike);
	}

}
