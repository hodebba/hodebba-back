package com.hodebba.back.post.services;

import com.hodebba.back.post.dto.ArgumentDto;
import com.hodebba.back.post.models.Argument;
import com.hodebba.back.post.models.ArgumentType;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface ArgumentService {
	List<Argument> findAllArguments();
	Argument createArgument(ArgumentDto argumentDto, Locale locale, Principal principal);
	Map<ArgumentType, List<Argument>> findByDebateGroupByType(Long debateId);
	Collection<Argument> findByDebate(Long debateId);
}
