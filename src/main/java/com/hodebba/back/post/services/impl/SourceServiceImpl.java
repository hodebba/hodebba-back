package com.hodebba.back.post.services.impl;

import com.hodebba.back.post.models.Post;
import com.hodebba.back.post.models.Source;
import com.hodebba.back.post.models.SourcePost;
import com.hodebba.back.post.persistence.SourceDao;
import com.hodebba.back.post.services.SourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class SourceServiceImpl implements SourceService {

	@Autowired
	private SourceDao sourceDao;

	@Override
	@Transactional
	public SourcePost create(String link, Post post) {
		// Post verification is made before calling this function
		return sourceDao.save(new SourcePost(link, post));
	}

	@Override
	@Transactional
	public void delete(long id) {
		sourceDao.deleteById(id);
	}

	@Override
	public Source findById(long id) {
		Optional<Source> optSource = sourceDao.findById(id);
		if (optSource.isPresent()) {
			return optSource.get();
		} else {
			throw new NoSuchElementException("No source with this id:" + id);
		}
	}
}
