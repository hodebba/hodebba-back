package com.hodebba.back.post.services;

import com.hodebba.back.post.models.Post;
import com.hodebba.back.post.models.Source;
import com.hodebba.back.post.models.SourcePost;

public interface SourceService {
	SourcePost create(String link, Post post);
	void delete(long id);
	Source findById(long id);
}
