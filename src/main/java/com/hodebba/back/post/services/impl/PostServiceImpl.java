package com.hodebba.back.post.services.impl;

import com.hodebba.back.celebrity.services.StatementService;
import com.hodebba.back.debate.models.*;
import com.hodebba.back.debate.services.ParticipantService;
import com.hodebba.back.post.dto.DashboardDto;
import com.hodebba.back.post.dto.UpdatePostDto;
import com.hodebba.back.post.services.PostService;
import com.hodebba.back.post.services.SourceService;
import com.hodebba.back.security.utils.TokenChecker;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.hodebba.back.post.models.*;
import com.hodebba.back.post.persistence.PostDao;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.*;

@Service
public class PostServiceImpl implements PostService {

	@Autowired
	private PostDao postDao;

	@Autowired
	private SourceService sourceService;

	@Lazy
	@Autowired
	private UserService userService;

	@Autowired
	private StatementService statementService;

	@Autowired
	private ParticipantService participantService;

	static final String noPostFoundMessage = "No post with this id:";

	@Override
	public Post findPostById(long id) {
		Optional<Post> optPost = postDao.findById(id);
		if (optPost.isPresent()) {
			return optPost.get();
		} else {
			throw new NoSuchElementException(noPostFoundMessage + id);
		}
	}

	@Override
	@Transactional
	public void deletePostById(long id, Locale locale, Principal principal) {
		Post post = findPostById(id);
		if(post.getDebate().getIsPrivate()){
			User loggedAdmin = userService.findByEmail(principal.getName());
			Participant adminParticipant = participantService.findByUserAndDebate(loggedAdmin, post.getDebate());

			if(adminParticipant.getRole().equals(DebateRole.admin)){
				postDao.deleteById(id);
			}else{
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		}
		else{
			TokenChecker.checkAdminOrUserToken(locale, principal, post.getUser(), userService);
			postDao.deleteById(id);
		}

	}

	@Override
	@Transactional
	public void incLikes(Post post) {
		post.incLikes();
		postDao.save(post);
	}

	@Override
	@Transactional
	public void decLikes(Post post) {
		post.decLikes();
		postDao.save(post);
	}

	@Override
	@Transactional
	public void incDislikes(Post post) {
		post.incDislikes();
		postDao.save(post);
	}

	@Override
	@Transactional
	public void decDislikes(Post post) {
		post.decDislikes();
		postDao.save(post);
	}

	@Override
	@Transactional
	public void deleteAllByDebate(Debate debate) {
		postDao.deleteAllByDebate(debate);
	}

	@Override
	public Collection<SourcePost> saveSources(Collection<String> sourceLinks, Post post) {
		Collection<SourcePost> sources = new ArrayList<>();
		if (sourceLinks != null) {
			for (String sourceLink : sourceLinks) {
				sources.add(sourceService.create(sourceLink, post));
			}
		}
		return sources;
	}

	@Override
	public DashboardDto findPostsDataByUser(Long id, Locale locale, Principal principal) {
		User user = userService.findById(id);
		TokenChecker.checkUserToken(locale, principal, user);
		List<Post> posts = postDao.findAllByUserOrderByLikes(user);
		List<Object[]> totals = postDao.getSumLikesDislikesByUser(id);
		long nbStatements = statementService.getStatementsByUser(user);
		BigDecimal totalLikes = new BigDecimal(0);
		BigDecimal totalDislikes = new BigDecimal(0);
		for (Object[] total : totals) {
			if (total[0] != null) {
				totalLikes = (BigDecimal) total[0];
				totalDislikes = (BigDecimal) total[1];
			}
		}
		return new DashboardDto(posts, totalLikes.longValue(), totalDislikes.longValue(), nbStatements);
	}

	@Override
	@Transactional
	public Post update(UpdatePostDto updatePostDto) {
		Post post = findPostById(updatePostDto.getId());

		if (updatePostDto.getContent() != null) {
			post.setContent(updatePostDto.getContent());
		}

		if (updatePostDto.getTitle() != null) {
			post.setTitle(updatePostDto.getTitle());
		}
		return postDao.save(post);
	}
}
