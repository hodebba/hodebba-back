package com.hodebba.back.post.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import java.util.Date;

@Getter
@JsonView(TestimonialDisplayDto.Views.FULL.class)
public class TestimonialDisplayDto {
	private Long id;
	private String title;
	private Date createdDate;
	private long likes;
	private long dislikes;
	private String content;
	private User user;
	private Debate debate;
	private int comments;

	public TestimonialDisplayDto(Long id, String title, Date createdDate, long likes, long dislikes, String content,
			User user, Debate debate, int comments) {
		this.id = id;
		this.title = title;
		this.createdDate = createdDate;
		this.likes = likes;
		this.dislikes = dislikes;
		this.content = content;
		this.user = user;
		this.debate = debate;
		this.comments = comments;
	}

	public static class Views {

		public interface FULL extends User.Views.LIGHT, Debate.Views.Id {
		}
	}
}
