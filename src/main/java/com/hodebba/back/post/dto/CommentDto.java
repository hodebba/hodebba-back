package com.hodebba.back.post.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "CommentDto", description = "Model for creation of a new Comment, contain only necessary info.")
public class CommentDto {

	@ApiModelProperty(notes = "Title of the debate", required = true, position = 0)
	private String title;

	@NotNull
	@Lob
	@ApiModelProperty(notes = "Content of the post", required = true, position = 1)
	private String content;

	@NotNull
	@ApiModelProperty(notes = "Id of the User who created the post", required = true, position = 4)
	private Long userId;

	@NotNull
	@ApiModelProperty(notes = "Id of the comment's parent", required = true, position = 5)
	private Long parentId;

}
