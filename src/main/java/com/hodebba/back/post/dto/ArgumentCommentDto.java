package com.hodebba.back.post.dto;

import com.hodebba.back.post.models.CommentType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Getter
@ApiModel(value = "ArgumentCommentDto", description = "Model for creation of a new ArgumentCommentDto, contain only necessary info.")
public class ArgumentCommentDto {
	@ApiModelProperty(notes = "Title of the debate", required = true, position = 0)
	private String title;

	@NotNull
	@Lob
	@ApiModelProperty(notes = "Content of the post", required = true, position = 1)
	private String content;

	@NotNull
	@ApiModelProperty(notes = "Type of post (pro, con or neutral)", required = true, position = 2)
	private CommentType postType;

	@ApiModelProperty(notes = "Source of the argument (link)", required = true, position = 3)
	private Collection<String> sourceLinks;

	@NotNull
	@ApiModelProperty(notes = "Id of the User who created the post", required = true, position = 4)
	private Long userId;

	@NotNull
	@ApiModelProperty(notes = "Id of the comment's parent", required = true, position = 5)
	private Long parentId;
}
