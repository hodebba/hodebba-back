package com.hodebba.back.post.dto;

import com.hodebba.back.post.models.Post;
import io.swagger.annotations.ApiModel;
import lombok.Getter;

import java.util.List;

@Getter
@ApiModel(value = "DashboardDto", description = "Model for display user dasboard data")
public class DashboardDto {

	List<Post> posts;
	long totalLikes;
	long totalDislikes;
	long nbStatements;

	public DashboardDto(List<Post> posts, long totalLikes, long totalDislikes, long nbStatements) {
		this.posts = posts;
		this.totalLikes = totalLikes;
		this.totalDislikes = totalDislikes;
		this.nbStatements = nbStatements;
	}
}
