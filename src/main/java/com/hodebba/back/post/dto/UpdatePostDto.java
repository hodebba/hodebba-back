package com.hodebba.back.post.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "UpdatePostDto", description = "Model for updating of a post, contain only necessary info.")
public class UpdatePostDto {

	@ApiModelProperty(notes = "Id of the post", required = true, position = 0)
	private Long id;

	@ApiModelProperty(notes = "Title of the post", position = 1)
	private String title;

	@NotNull
	@Lob
	@ApiModelProperty(notes = "Content of the post", position = 2)
	private String content;
}
