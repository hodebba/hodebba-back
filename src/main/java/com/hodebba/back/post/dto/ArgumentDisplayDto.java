package com.hodebba.back.post.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.post.models.*;
import com.hodebba.back.user.models.User;
import lombok.Getter;

import java.util.Collection;
import java.util.Date;

@Getter
@JsonView(ArgumentDisplayDto.Views.FULL.class)
public class ArgumentDisplayDto {
	private Long id;
	private String title;
	private Date createdDate;
	private long likes;
	private long dislikes;
	private String content;
	private User user;
	private Debate debate;
	private int comments;
	private ArgumentType postType;
	private Collection<SourcePost> sources;
	private long rank;

	public ArgumentDisplayDto(Long id, String title, Date createdDate, long likes, long dislikes, String content,
			User user, int comments, ArgumentType postType, Collection<SourcePost> sources, Debate debate, long rank) {
		this.id = id;
		this.title = title;
		this.createdDate = createdDate;
		this.likes = likes;
		this.dislikes = dislikes;
		this.content = content;
		this.user = user;
		this.comments = comments;
		this.postType = postType;
		this.sources = sources;
		this.debate = debate;
		this.rank = rank;
	}

	public static class Views {

		public interface FULL extends User.Views.LIGHT, Debate.Views.Id, Source.Views.LIGHT {
		}
	}
}
