package com.hodebba.back.post.dto;

import com.hodebba.back.post.models.ArgumentType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Getter
@ApiModel(value = "ArgumentDto", description = "Model for creation of a new Argument, contain only necessary info.")
public class ArgumentDto {

	@ApiModelProperty(notes = "Title of the argument", required = true, position = 0)
	private String title;

	@NotNull
	@Lob
	@ApiModelProperty(notes = "Content of the argument", required = true, position = 1)
	private String content;

	@NotNull
	@ApiModelProperty(notes = "Type of the argument (pro or con)", required = true, position = 2)
	private ArgumentType postType;

	@ApiModelProperty(notes = "Source of the argument (link)", required = true, position = 3)
	private Collection<String> sourceLinks;

	@NotNull
	@ApiModelProperty(notes = "Id of the User who created the argument", required = true, position = 4)
	private Long userId;

	@NotNull
	@ApiModelProperty(notes = "Id of the Debate to which belongs the argument", required = true, position = 5)
	private Long debateId;

}
