package com.hodebba.back.post.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "LikeDislikePostDto", description = "Model for creation of a new LikeDislike")
public class LikeDislikePostDto {

	@NotNull
	@ApiModelProperty(notes = "Like or dislike", required = true, position = 0)
	private Boolean isLike;

	@NotNull
	@ApiModelProperty(notes = "Post id of the like", required = true, position = 1)
	private Long postId;

	@NotNull
	@ApiModelProperty(notes = "Id of the User who liked / disliked", required = true, position = 2)
	private Long userId;

}
