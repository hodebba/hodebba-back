package com.hodebba.back.post.persistence;

import com.hodebba.back.post.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentDao extends JpaRepository<Comment, Long> {
}
