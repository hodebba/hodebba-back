package com.hodebba.back.post.persistence;

import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.post.models.Post;
import com.hodebba.back.user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostDao extends JpaRepository<Post, Long> {

	void deleteAllByDebate(Debate debate);

	List<Post> findAllByUserOrderByLikes(User user);

	@Query(value = "SELECT SUM(p.likes) AS totalLikes, SUM(p.dislikes) AS totalDislikes FROM public.post p WHERE user_id=?1", nativeQuery = true)
	List<Object[]> getSumLikesDislikesByUser(Long userId);
}
