package com.hodebba.back.post.persistence;

import com.hodebba.back.post.models.Testimonial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Collection;
import java.util.Optional;

public interface TestimonialDao extends JpaRepository<Testimonial, Long> {

	@Query(value = "SELECT *, "
			+ "case when p.likes+p.dislikes > 0 then ((p.likes + 1.9208) / (p.likes + p.dislikes) - 01.96 * SQRT((p.likes * p.dislikes) / (p.likes + p.dislikes) + 0.9604) / (p.likes + p.dislikes)) / (1 + 3.8416 / (p.likes + p.dislikes)) "
			+ "else 0.01 end score " + "FROM public.post p" + " INNER JOIN public.testimonial t ON p.id = t.id "
			+ "WHERE p.debate_id=?1" + " ORDER BY score DESC", nativeQuery = true)
	Optional<Collection<Testimonial>> findByDebate(Long debateId);
}
