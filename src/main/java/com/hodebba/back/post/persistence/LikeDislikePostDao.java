package com.hodebba.back.post.persistence;

import com.hodebba.back.post.models.LikeDislike;
import com.hodebba.back.post.models.LikeDislikePost;
import com.hodebba.back.post.models.Post;
import com.hodebba.back.user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface LikeDislikePostDao extends JpaRepository<LikeDislikePost, Long> {
	Optional<LikeDislikePost> findByUserAndPost(User user, Post post);

	@Query(value = "SELECT * FROM (public.like_dislike_post l INNER JOIN public.post p ON p.id = l.post_id) INNER JOIN public.like_dislike d ON d.id=l.id WHERE  d.user_id=?1 AND p.debate_id=?2", nativeQuery = true)
	Collection<LikeDislikePost> findByUserIdAndDebateId(Long userId, Long debateId);
}
