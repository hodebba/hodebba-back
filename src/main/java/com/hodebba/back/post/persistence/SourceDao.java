package com.hodebba.back.post.persistence;

import com.hodebba.back.post.models.Source;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SourceDao extends JpaRepository<Source, Long> {
}
