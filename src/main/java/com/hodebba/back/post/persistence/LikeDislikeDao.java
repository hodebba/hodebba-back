package com.hodebba.back.post.persistence;

import com.hodebba.back.post.models.LikeDislike;
import com.hodebba.back.post.models.Post;
import com.hodebba.back.user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Optional;

public interface LikeDislikeDao extends JpaRepository<LikeDislike, Long> {

}
