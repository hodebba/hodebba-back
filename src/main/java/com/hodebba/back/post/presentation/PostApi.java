package com.hodebba.back.post.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.dto.DebateUpdateDto;
import com.hodebba.back.debate.dto.TestimonialDto;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.post.dto.*;
import com.hodebba.back.post.services.ArgumentService;
import com.hodebba.back.post.services.CommentService;
import com.hodebba.back.post.services.TestimonialService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import com.hodebba.back.post.models.*;
import com.hodebba.back.post.services.PostService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

@RestController
@AllArgsConstructor
@CrossOrigin
public class PostApi {

	private static final Logger logger = LoggerFactory.getLogger(PostApi.class);

	@Autowired
	private final PostService postService;

	@Autowired
	private final ArgumentService argumentService;

	@Autowired
	private final TestimonialService testimonialService;

	@Autowired
	private final CommentService commentService;

	// ============== CREATE POST ============
	// create argument
	@PostMapping(value = "/posts/arguments", produces = "application/json")
	@ApiOperation(value = "Create argument", notes = "Create a new argument")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of a user", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Post.Views.FULL.class)
	public Argument createArgument(
			@ApiParam(value = "ArgumentDto model", required = true) @RequestBody @Valid ArgumentDto argumentDto,
			final Locale locale, Principal principal) {
		Argument argument = argumentService.createArgument(argumentDto, locale, principal);
		logger.info("A new Argument has been created : {}", argument);
		return argument;
	}

	// create testimonial
	@PostMapping(value = "/posts/testimonials", produces = "application/json")
	@ApiOperation(value = "Create testimonial", notes = "Create a new testimonial")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of a user", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Post.Views.FULL.class)
	public Testimonial createTestimonial(
			@ApiParam(value = "TestimonialDto model", required = true) @RequestBody @Valid TestimonialDto testimonialDto,
			final Locale locale, Principal principal) {
		Testimonial testimonial = testimonialService.createTestimonial(testimonialDto, locale, principal);
		logger.info("A new Testimonial has been created : {}", testimonial);
		return testimonial;
	}

	// create comment
	@PostMapping(value = "/posts/comments", produces = "application/json")
	@ApiOperation(value = "Create comment", notes = "Create a new comment")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of a user", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Post.Views.FULL.class)
	public Comment createComment(
			@ApiParam(value = "CommentDto model", required = true) @RequestBody @Valid CommentDto commentDto,
			final Locale locale, Principal principal) {
		Comment comment = commentService.createComment(commentDto, locale, principal);
		logger.info("A new Comment has been created : {}", comment);
		return comment;
	}

	// create comment of argument
	@PostMapping(value = "/posts/argument-comments", produces = "application/json")
	@ApiOperation(value = "Create Comment of Argument", notes = "Create a new Comment of Argument")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of a user", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Post.Views.FULL.class)
	public ArgumentComment createCommentOfArgument(
			@ApiParam(value = "CommentDto model", required = true) @RequestBody @Valid ArgumentCommentDto argumentCommentDto,
			final Locale locale, Principal principal) {
		ArgumentComment comment = commentService.createArgumentComment(argumentCommentDto, locale, principal);
		logger.info("A new ArgumentComment has been created : {}", comment);
		return comment;
	}

	// ============== UPDATE POST ============
	@PutMapping(value = "/posts", produces = "application/json")
	@ApiOperation(value = "Update Posts", notes = "Update data of a post.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Post.Views.FULL.class)
	public Post updatePost(
			@ApiParam(value = "UpdatePostDto model", required = true) @RequestBody @Valid UpdatePostDto updatePostDto) {
		try {
			Post post = postService.update(updatePostDto);
			logger.info("A post has been updated : {}.", post);
			return post;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== GET ============

	// - all arguments
	@GetMapping(value = "/posts/arguments", produces = "application/json")
	@ApiOperation(value = "Find all arguments", notes = "Find the list of all arguments.")
	@JsonView(Post.Views.LIGHT.class)
	public List<Argument> findAllArguments() {
		return argumentService.findAllArguments();

	}

	// - all debates
	@GetMapping(value = "/posts/testimonials", produces = "application/json")
	@ApiOperation(value = "Find all testimonials", notes = "Find the list of all testimonials.")
	@JsonView(Post.Views.LIGHT.class)
	public List<Testimonial> findAllTestimonials() {
		return testimonialService.findAllTestimonials();

	}

	// -by id
	@GetMapping(value = "/posts/{id}", produces = "application/json")
	@ApiOperation(value = "Find Post by id", notes = "Find a Post by its id.")
	@JsonView(Post.Views.FULL.class)
	public Post getPostById(@ApiParam(value = "id of the Post to find", required = true) @PathVariable long id) {
		try {
			return postService.findPostById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// -by user
	@GetMapping(value = "/posts/users/{id}", produces = "application/json")
	@ApiOperation(value = "Find Post by user", notes = "Find a Post by user id.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Post.Views.ForDashboard.class)
	public DashboardDto getPostsByUser(
			@ApiParam(value = "id of the User whose posts we want to find", required = true) @PathVariable Long id,
			Locale locale, Principal principal) {
		try {
			return postService.findPostsDataByUser(id, locale, principal);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - arguments by debate id
	@GetMapping(value = "/debates/{debateId}/arguments", produces = "application/json")
	@ApiOperation(value = "Find Argument by debate id", notes = "Find Arguments by its debate id.")
	@JsonView(ArgumentDisplayDto.Views.FULL.class)
	public Map<ArgumentType, List<ArgumentDisplayDto>> getArgumentsByDebateId(
			@ApiParam(value = "id of the debate", required = true) @PathVariable long debateId) {
		try {
			List<Argument> arguments = (List<Argument>) argumentService.findByDebate(debateId);
			Map<ArgumentType, List<ArgumentDisplayDto>> ret = new HashMap<>();

			if (arguments != null) {
				List<ArgumentDisplayDto> proList = new ArrayList<>();
				List<ArgumentDisplayDto> conList = new ArrayList<>();

				for (int i = 0; i < arguments.size(); i++) {
					if(arguments.get(i).getPostType().equals(ArgumentType.pro)){
						proList.add(new ArgumentDisplayDto(arguments.get(i).getId(), arguments.get(i).getTitle(),arguments.get(i).getCreatedDate(), arguments.get(i).getLikes(), arguments.get(i).getDislikes(), arguments.get(i).getContent(), arguments.get(i).getUser(), arguments.get(i).getComments().size(), arguments.get(i).getPostType(), arguments.get(i).getSources(), arguments.get(i).getDebate(), i));
					}else {
						conList.add(new ArgumentDisplayDto(arguments.get(i).getId(), arguments.get(i).getTitle(),arguments.get(i).getCreatedDate(), arguments.get(i).getLikes(), arguments.get(i).getDislikes(), arguments.get(i).getContent(), arguments.get(i).getUser(), arguments.get(i).getComments().size(), arguments.get(i).getPostType(), arguments.get(i).getSources(), arguments.get(i).getDebate(), i));
					}
				}
				ret.put(ArgumentType.pro, proList);
				ret.put(ArgumentType.con, conList);
			}
			return ret;

		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - testimonials by debate id
	@GetMapping(value = "/debates/{debateId}/testimonials", produces = "application/json")
	@ApiOperation(value = "Find Testimonial by debate id", notes = "Find testimonials by its debate id.")
	@JsonView(TestimonialDisplayDto.Views.FULL.class)
	public Collection<TestimonialDisplayDto> getTestimonialsByDebateId(
			@ApiParam(value = "id of the debate", required = true) @PathVariable long debateId) {
		try {
			Collection<Testimonial> testimonials = testimonialService.findTestimonialsByDebateId(debateId);
			List<TestimonialDisplayDto> ret = new ArrayList<>();

			if (testimonials != null) {
				testimonials.forEach(testimonial -> {
					ret.add(new TestimonialDisplayDto(testimonial.getId(), testimonial.getTitle(),
							testimonial.getCreatedDate(), testimonial.getLikes(), testimonial.getDislikes(),
							testimonial.getContent(), testimonial.getUser(), testimonial.getDebate(),
							testimonial.getComments().size()));
				});
			}

			return ret;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== DELETE ============
	// -by id
	@DeleteMapping(value = "/posts/{id}", produces = "application/json")
	@ApiOperation(value = "Delete a post by id", notes = "Delete a post by id.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void deletePostById(@ApiParam(value = "id of the Post to delete", required = true) @PathVariable long id,
			final Locale locale, Principal principal) {
		try {
			postService.deletePostById(id, locale, principal);
			logger.info("The Post with the id : {} has been deleted.", id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}
}
