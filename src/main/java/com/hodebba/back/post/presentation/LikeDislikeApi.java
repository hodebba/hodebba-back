package com.hodebba.back.post.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.post.models.LikeDislikePost;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import com.hodebba.back.post.dto.LikeDislikePostDto;
import com.hodebba.back.post.models.LikeDislike;
import com.hodebba.back.post.services.LikeDislikeService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collection;
import java.util.Locale;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
public class LikeDislikeApi {

	private static final Logger logger = LoggerFactory.getLogger(LikeDislikeApi.class);

	@Autowired
	private final LikeDislikeService likeDislikeService;

	// ============== CREATE OR UPDATE LIKE / DISLIKE =============
	@PostMapping(value = "/posts/likes", produces = "application/json")
	@ApiOperation(value = "Add like", notes = "Add a new like")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of the user", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(LikeDislike.Views.FULL.class)
	public LikeDislike createLikeDislike(
			@ApiParam(value = "LikeDislikePostDto model", required = true) @RequestBody @Valid LikeDislikePostDto likeDislikePostDto,
			final Locale locale, Principal principal) {
		return likeDislikeService.create(likeDislikePostDto, locale, principal);
	}

	// ============== DELETE LIKE / DISLIKE ==============
	@DeleteMapping(value = "/likes/{id}", produces = "application/json")
	@ApiOperation(value = "Delete Like / Dislike", notes = "Delete an existing Like / Dislike")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of the user", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void deleteLikeDislike(@ApiParam(value = "id of the Post to delete", required = true) @PathVariable long id,
			final Locale locale, Principal principal) {
		try {
			likeDislikeService.delete(id, locale, principal);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== GET LIKE / DISLIKE ==============
	// - by user
	@GetMapping(value = "/posts/likes", produces = "application/json")
	@ApiOperation(value = "Find User likes / dislikes by user", notes = "Find User likes / dislikes with the user id.")
	@JsonView(LikeDislikePost.Views.Light.class)
	public Collection<LikeDislikePost> getLikeDislikeByUserAndDebate(
			@ApiParam(value = "id of the User", required = true) @RequestParam long userId,
			@ApiParam(value = "id of the Debate", required = true) @RequestParam long debateId) {
		try {
			return likeDislikeService.findLikeDislikeByUserAndDebate(userId, debateId);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}
}
