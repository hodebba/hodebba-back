package com.hodebba.back.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "ReportPostDto", description = "Model for getting message when a post is reported.")
public class ReportPostDto {

	@NotNull
	@ApiModelProperty(notes = "Id of the post", required = true, position = 0)
	private Long postId;

	@NotNull
	@ApiModelProperty(notes = "Content of the post", required = true, position = 1)
	private String postContent;

	@NotNull
	@ApiModelProperty(notes = "Reason of this report", required = true, position = 2)
	private String reason;

	@ApiModelProperty(notes = "Email of the user who report the post", position = 3)
	private String userEmail;
}
