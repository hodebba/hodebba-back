package com.hodebba.back.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "ContactUsDto", description = "Model for getting message from contact us form.")
public class ContactUsDto {

	@NotNull
	@ApiModelProperty(notes = "Name of the customer", required = true, position = 0)
	private String name;

	@NotNull
	@ApiModelProperty(notes = "Email of the customer", required = true, position = 1)
	private String email;

	@NotNull
	@Lob
	@ApiModelProperty(notes = "Message sent by the customer", required = true, position = 2)
	private String message;
}
