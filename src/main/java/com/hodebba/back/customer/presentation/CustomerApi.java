package com.hodebba.back.customer.presentation;

import com.hodebba.back.customer.dto.ContactUsDto;
import com.hodebba.back.customer.dto.ReportPostDto;
import com.hodebba.back.user.util.GenericResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Locale;

@RestController
@AllArgsConstructor
@CrossOrigin
public class CustomerApi {

	private static final Logger logger = LoggerFactory.getLogger(CustomerApi.class);

	@Autowired
	private JavaMailSender mailSender;

	private MessageSource messages;

	@Autowired
	private Environment env;

	@PostMapping(value = "/customers/contact-us", produces = "application/json")
	@ApiOperation(value = "Contact us message", notes = "Get message from contact us form")
	public GenericResponse contactUs(
			@ApiParam(value = "ContactUsDto model", required = true) @RequestBody @Valid final ContactUsDto contactUsDto,
			final Locale locale) {

		String subject = "[CONTACT] Message de " + contactUsDto.getName();
		String message = contactUsDto.getMessage() + "\n" + "Email: " + contactUsDto.getEmail();

		SimpleMailMessage email = constructEmail(subject, message);
		mailSender.send(email);
		logger.info("{} sent a message to Hodebba.", contactUsDto.getName());

		message = messages.getMessage("email.contactUs.sent", null, locale);
		return new GenericResponse(message);
	}

	@PostMapping(value = "/customers/report-post", produces = "application/json")
	@ApiOperation(value = "Report a post", notes = "Send email when getting a post report")
	public GenericResponse reportPost(
			@ApiParam(value = "ReportPostDto model", required = true) @RequestBody @Valid final ReportPostDto reportPostDto,
			final Locale locale) {

		String userEmail = "email inconnu";

		if (reportPostDto.getUserEmail() != null) {
			userEmail = reportPostDto.getUserEmail();
		}

		String subject = "[SIGNALEMENT] Message provenant de " + userEmail;
		String message = reportPostDto.getReason() + "\n\n" + "Post: " + reportPostDto.getPostContent() + "\n"
				+ "Id du post: " + reportPostDto.getPostId();

		SimpleMailMessage email = constructEmail(subject, message);
		mailSender.send(email);
		logger.info("The post {} has been reported.", reportPostDto.getPostId());

		message = messages.getMessage("email.reportPost.sent", null, locale);
		return new GenericResponse(message);
	}

	private SimpleMailMessage constructEmail(String subject, String body) {
		final SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject(subject);
		email.setText(body);
		email.setTo(env.getProperty("support.email"));
		email.setFrom(env.getProperty("support.email"));
		return email;
	}

}
