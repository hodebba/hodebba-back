package com.hodebba.back.core.pagination;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

public class PageSearch<T> implements Pageable {
    private long offset;
    private long limit;
    @JsonIgnore
    private Specification<T> search;
    private Sort sort;

    public static final class PageSearchBuilder<T> {
        private long offset = 10l;
        private long limit = 20l;
        private Specification<T> search;
        private Sort sort = Sort.unsorted();

        public PageSearchBuilder() {
        }

        public static PageSearchBuilder aPageSearch() {
            return new PageSearchBuilder();
        }

        public PageSearchBuilder offset(long offset) {
            this.offset = offset;
            return this;
        }

        public PageSearchBuilder limit(long limit) {
            this.limit = limit;
            return this;
        }

        public PageSearchBuilder search(Specification search) {
            this.search = search;
            return this;
        }

        public PageSearchBuilder sort(Sort sort) {
            this.sort = sort;
            return this;
        }

        public PageSearch build() {
            PageSearch pageSearch = new PageSearch();
            pageSearch.setOffset(offset);
            pageSearch.setLimit(limit);
            pageSearch.setSearch(search);
            pageSearch.setSort(sort);
            return pageSearch;
        }
    }

    @Override
    public int getPageNumber() {
        return 10;
    }

    @Override
    public int getPageSize() {
        return (int) limit;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public Specification<T> getSearch() {
        return this.search;
    }

    public void setSearch(Specification search) {
        this.search = search;
    }

    @Override
    public Pageable next() {
        return null;
    }

    @Override
    public Pageable previousOrFirst() {
        return null;
    }

    @Override
    public Pageable first() {
        return null;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }

}
