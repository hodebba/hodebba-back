package com.hodebba.back.core.pagination;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Objects;

public class SearchSpecification<T> implements Specification<T> {

    private final String attribute;
    private final Operator operator;
    private final Object value;

    /**
     * Create a new SearchSpecification with given operator and operands
     *
     * @param field    the left operand
     * @param operator the operator
     * @param value    the right operand
     */
    public SearchSpecification(String field, Operator operator, Object value) {
        this.attribute = field;
        this.operator = operator;
        this.value = value;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {

        Path<String> path = resolvePath(root, this.attribute);

        switch (operator) {
            case LT:
            case NOT_GTE:
                return cb.lessThan(path, value.toString());
            case LTE:
            case NOT_GT:
                return cb.lessThanOrEqualTo(path, value.toString());
            case GT:
            case NOT_LTE:
                return cb.greaterThan(path, value.toString());
            case GTE:
            case NOT_LT:
                return cb.greaterThanOrEqualTo(path, value.toString());
            case IN:
                return cb.in(path).value(value.toString());
            case LIKE:
                return cb.like(cb.lower(path), "%" + value.toString().toLowerCase() + "%");
            case SW:
                return cb.like(cb.lower(path), value.toString().toLowerCase() + "%");
            case EW:
                return cb.like(cb.lower(path), "%" + value.toString().toLowerCase());
            case NULL:
                return cb.isNull(path);
            case NOT_NULL:
                return cb.isNotNull(path);
            case EQ:
                return cb.equal(path, value);
            case NOT_IN:
                return cb.not(cb.in(path).value(value.toString()));
            case NOT_LIKE:
                return cb.not(cb.like(cb.lower(path), "%" + value.toString().toLowerCase() + "%"));
            case NOT_SW:
                return cb.not(cb.like(cb.lower(path), value.toString().toLowerCase() + "%"));
            case NOT_EW:
                return cb.not(cb.like(cb.lower(path), "%" + value.toString().toLowerCase()));
            case NOT_EQ:
                return cb.not(cb.equal(path, value));
            default:
                throw new UnsupportedOperationException("Unsupported operator: " + operator);
        }
    }

    /**
     * Parses an expression to Specification
     *
     * @param exp the expression to parse
     * @param <T> type of specification
     * @return the specification that matches to the expression
     */
    public static <T> Specification<T> parse(String exp) {
        return Arrays.stream(Operator.values())
                .map(o -> {
                    var matcher = o.getPattern().matcher(exp);
                    if (matcher.matches()) {
                        String attributeName = matcher.group(1);
                        String value = matcher.group(2);
                        return new SearchSpecification<T>(attributeName, o, value);
                    }


                    return null;
                })
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("no operator matching expression '%s'", exp)));
    }

    private Path resolvePath(Root<T> root, String attribute) {
        String[] parts = attribute.split("\\.");
        Path<String> path = root.get(parts[0]);
        String[] updatedPath = Arrays.copyOfRange(parts, 1, parts.length);
        for (String s : updatedPath) {
            path = path.get(s);
        }
        return path;
    }
}
