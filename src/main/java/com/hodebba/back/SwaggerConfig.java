package com.hodebba.back;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.security.Principal;
import java.util.Collections;
import java.util.Locale;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select() // provides a way to control the endpoints exposed by
																// Swagger.
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).paths(PathSelectors.any()) // select
																													// paths
																													// swagger
																													// should
																													// generate
																													// the
																													// doc
																													// for
				.build().apiInfo(apiInfo())
				.ignoredParameterTypes(Pageable.class, Resource.class, Page.class, Sort.class, Locale.class,
						Principal.class) // do not consider these entities as models
				.pathMapping("/");
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("Hodebba REST api", "Offers basic CRUD to control the hodebba api.", "v1.0.0", null,
				new Contact("Hodebba", "https://www.hodebba.fr/", "hodebba@gmail.com"), "MIT",
				"https://opensource.org/licenses/MIT", Collections.emptyList());
	}
}
