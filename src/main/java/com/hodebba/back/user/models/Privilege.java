package com.hodebba.back.user.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

import javax.persistence.*;
import java.util.*;

@Entity(name = "privilege")
@Getter
public class Privilege {

	@Id
	@SequenceGenerator(name = "privilege_id_generator", sequenceName = "privilege_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "privilege_id_generator")
	private Long id;

	private String name;

	@ManyToMany(mappedBy = "privileges")
	@JsonIgnoreProperties("privileges")
	private transient Collection<Role> roles;

	public Privilege() {
	}

	public Privilege(String name) {
		this.name = name;
	}

	private String rolesToString() {
		List l = new ArrayList();
		if (this.roles != null) {
			Iterator<Role> it = this.roles.iterator();
			while (it.hasNext()) {
				l.add(it.next().getId());
			}
		}
		return l.toString();
	}

	@Override
	public String toString() {
		return "Privilege{" + "id = " + id + ", name = '" + name + "', roles = " + rolesToString() + "}";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Privilege privilege = (Privilege) o;
		return id.equals(privilege.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
