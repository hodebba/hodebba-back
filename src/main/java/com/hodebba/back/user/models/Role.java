package com.hodebba.back.user.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@Entity(name = "role")
@Getter
public class Role {

	@Id
	@SequenceGenerator(name = "role_id_generator", sequenceName = "role_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_id_generator")
	private Long id;

	private String name;

	@ManyToMany(mappedBy = "roles")
	@JsonIgnoreProperties("roles")
	private transient Collection<User> users;

	@Setter
	@ManyToMany
	@JoinTable(name = "roles_privileges", joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "privilege_id", referencedColumnName = "id"))
	@JsonIgnoreProperties("roles")
	private Collection<Privilege> privileges;

	public Role() {
	}

	public Role(String name) {
		this.name = name;
	}

	private String usersToString() {
		List l = new ArrayList();
		if (this.users != null) {
			Iterator<User> it = this.users.iterator();
			while (it.hasNext()) {
				User user = it.next();
				l.add(user.getId());
			}
		}
		return l.toString();
	}

	private String privilegesToString() {
		List l = new ArrayList();
		if (this.privileges != null) {
			Iterator<Privilege> it = this.privileges.iterator();
			while (it.hasNext()) {
				Privilege privilege = it.next();
				l.add(privilege.getId());
			}
		}
		return l.toString();
	}

	@Override
	public String toString() {
		return "Role{" + "id = " + id + ", name = '" + name + "', users = " + usersToString() + ", privileges = "
				+ privilegesToString() + "}";
	}
}
