package com.hodebba.back.user.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.*;

@Entity(name = "users")
@JsonView(User.Views.FULL.class)
@Getter
public class User {

	@Id
	@SequenceGenerator(name = "users_id_generator", sequenceName = "users_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_generator")
	@JsonView(Views.ID.class)
	Long id;

	@Setter
	@Size(min = 2, max = 50)
	@JsonView(Views.LIGHT.class)
	private String username;

	@Setter
	private String password;

	@Setter
	@JsonView(Views.LIGHT.class)
	private String email;

	@Setter
	@JsonView(Views.LIGHT.class)
	private boolean enabled;

	@Setter
	private String description;

	@Setter
	private String location;

	@Getter
	@Setter
	@JsonView(Views.LIGHT.class)
	private String imageLink;

	@Setter
	@ManyToMany
	@JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	@JsonIgnoreProperties("users")
	private Collection<Role> roles;

	@Setter
	@ElementCollection
	@CollectionTable(name = "user_socials_mapping", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id")})
	@MapKeyColumn(name = "socials_name")
	@MapKeyEnumerated(EnumType.STRING)
	@Column(name = "link")
	private Map<SocialsName, String> socials;

	public User() {
		super();
		this.enabled = false;
		this.imageLink = "profile/defaultUser_zdniza";
	}

	public void addSocials(SocialsName socialsName, String link) {
		if (this.socials == null) {
			this.socials = new HashMap<SocialsName, String>();
		}
		this.socials.put(socialsName, link);
	}

	public static class Views {
		public interface ID {
		}

		public interface LIGHT extends ID {
		}

		public interface FULL {
		}
	}

	private String rolesToString() {
		List l = new ArrayList();
		if (this.roles != null) {
			Iterator<Role> it = this.roles.iterator();
			while (it.hasNext()) {
				l.add(it.next().getName());
			}
		}
		return l.toString();
	}

	@Override
	public String toString() {
		return "User{" + "id=" + id + ", username='" + username + '\'' + ", password='" + password + '\'' + ", email='"
				+ email + '\'' + ", enabled='" + enabled + '\'' + ", description='" + description + '\''
				+ ", location='" + location + '\'' + ", imageLink='" + imageLink + '\'' + ", roles='" + rolesToString()
				+ '\'' + ", socials='" + socials + '\'' + '}';
	}

}