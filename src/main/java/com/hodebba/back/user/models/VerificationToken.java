package com.hodebba.back.user.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity(name = "verification_token")
@Getter
public class VerificationToken {

	private static final int EXPIRATION = 60 * 24; // 24 hours

	@Id
	@SequenceGenerator(name = "verification_token_id_generator", sequenceName = "verification_token_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "verification_token_id_generator")
	private Long id;

	@Setter
	private String token;

	@Setter
	@OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(nullable = false, name = "user_id")
	private User user;

	@Temporal(TemporalType.TIMESTAMP)
	private Date expiryDate;

	public VerificationToken() {
		super();
	}

	public VerificationToken(final String token) {
		super();

		this.token = token;
		this.expiryDate = calculateExpiryDate(EXPIRATION);
	}

	public VerificationToken(final String token, final User user) {
		super();

		this.token = token;
		this.user = user;
		this.expiryDate = calculateExpiryDate(EXPIRATION);
	}

	private Date calculateExpiryDate(int expiryTimeInMinutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(new Date().getTime());
		cal.add(Calendar.MINUTE, expiryTimeInMinutes);
		return new Date(cal.getTime().getTime());
	}

	public void updateToken(final String token) {
		this.token = token;
		this.expiryDate = calculateExpiryDate(EXPIRATION);
	}

	@Override
	public String toString() {
		return "Token [String=" + token + "]" + "[Expires" + expiryDate + "]";
	}

}
