package com.hodebba.back.user.models;

public enum SocialsName {
    Facebook, Twitter, Instagram, Blog, Telegram
}
