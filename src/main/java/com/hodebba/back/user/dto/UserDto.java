package com.hodebba.back.user.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.hodebba.back.user.validation.PasswordMatches;
import com.hodebba.back.user.validation.ValidEmail;
import com.hodebba.back.user.validation.ValidPassword;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@PasswordMatches
@Getter
@ApiModel(value = "UserDto", description = "Model for creation of a new User, contain only necessary info.")
public class UserDto {
	@NotNull
	@Size(min = 1)
	@ApiModelProperty(notes = "Username of the User, can't be an username already used by another User.", required = true, position = 0)
	private String username;

	@ValidPassword
	@ApiModelProperty(notes = "Password of the User.", required = true, position = 2)
	private String password;

	@NotNull
	@Size(min = 1)
	@ApiModelProperty(notes = "Second occurrence of password, must match the first one.", required = true, position = 3)
	private String matchingPassword;

	@ValidEmail
	@NotNull
	@Size(min = 1)
	@ApiModelProperty(notes = "Email of the User, can't be an email already used by another User.", required = true, position = 1)
	private String email;

	@Override
	public String toString() {
		return "UserDto{" + "username='" + username + '\'' + ", password='" + password + '\'' + ", matchingPassword='"
				+ matchingPassword + '\'' + ", email='" + email + '\'' + '}';
	}
}
