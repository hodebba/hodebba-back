package com.hodebba.back.user.dto;

import com.hodebba.back.user.validation.ValidPassword;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "PasswordDto", description = "Model for updating the password of a User.")
public class PasswordDto {

	@ApiModelProperty(notes = "id of the User to update.", required = false, position = 0)
	private long userId;

	@ApiModelProperty(notes = "token received by email when reset password request has been done", required = false, position = 1)
	private String token;

	@ApiModelProperty(notes = "current password of the User", required = false, position = 2)
	private String oldPassword;

	@NotNull
	@ApiModelProperty(notes = "second occurrence of password, must match the first one", required = true, position = 4)
	private String matchingPassword;

	@NotNull
	@ValidPassword
	@ApiModelProperty(notes = "new password of the User", required = true, position = 3)
	private String newPassword;

}
