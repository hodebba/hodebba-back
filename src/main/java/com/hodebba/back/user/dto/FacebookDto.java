package com.hodebba.back.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@ApiModel(value = "FacebookDto", description = "Model for sign up or login with Facebook.")
public class FacebookDto {

	@NotNull
	@Size(min = 1)
	@ApiModelProperty(notes = "Username of the User", required = true, position = 0)
	private String username;

	@NotNull
	@Size(min = 1)
	@ApiModelProperty(notes = "Email of the User.", required = true, position = 1)
	private String email;

	private String accessToken;
}
