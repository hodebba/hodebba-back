package com.hodebba.back.user.dto;

import com.hodebba.back.user.validation.ValidEmail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@ApiModel(value = "EmailDto", description = "Model for updating email of a User.")
public class EmailDto {

	@ValidEmail
	@NotNull
	@Size(min = 1)
	@ApiModelProperty(notes = "New email value", required = true, position = 1)
	private String email;

	@NotNull
	@ApiModelProperty(notes = "id of the User to update.", required = true, position = 0)
	private long userId;
}