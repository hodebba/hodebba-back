package com.hodebba.back.user.dto;

import com.hodebba.back.user.models.SocialsName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Getter
@ApiModel(value = "UserUpdateDto", description = "Model for updating info of a User.")
public class UserUpdateDto {

	@NotNull
	@ApiModelProperty(notes = "id of the User to update.", required = true, position = 0)
	private Long id;

	@ApiModelProperty(notes = "Username of the User, can't be an username already used by another User.", required = false, position = 1)
	private String username;

	@ApiModelProperty(notes = "Description of the User.", required = false, position = 2)
	private String description;

	@ApiModelProperty(notes = "Location of the User.", required = false, position = 3)
	private String location;

	@ApiModelProperty(notes = "Url to an image for the User avatar.", required = false, position = 4)
	private String imageLink;

	@ApiModelProperty(notes = "Map (key/value) of socials url of the User. The key of the map must be one of the following string : Facebook, Twitter, Instagram, Blog. The value is the url to the social media.", required = false, position = 5)
	private Map<SocialsName, String> socials;

}
