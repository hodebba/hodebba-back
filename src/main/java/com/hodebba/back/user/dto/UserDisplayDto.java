package com.hodebba.back.user.dto;

import com.hodebba.back.user.models.Role;
import com.hodebba.back.user.models.SocialsName;
import com.hodebba.back.user.models.User;
import lombok.Getter;

import java.util.Collection;
import java.util.Map;

@Getter
public class UserDisplayDto {

	private Long id;
	private String username;
	private String email;
	private Boolean enabled;
	private String description;
	private String location;
	private String imageLink;
	private Collection<Role> roles;
	private Map<SocialsName, String> socials;

	public UserDisplayDto(User user) {
		this.id = user.getId();
		this.username = user.getUsername();
		this.email = user.getEmail();
		this.enabled = user.isEnabled();
		this.description = user.getDescription();
		this.location = user.getLocation();
		this.imageLink = user.getImageLink();
		this.roles = user.getRoles();
		this.socials = user.getSocials();
	}

}
