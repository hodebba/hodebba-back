package com.hodebba.back.user.validation;

import com.hodebba.back.user.dto.UserDto;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {

	@Override
	public boolean supports(final Class<?> clazz) {
		return UserDto.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(final Object obj, final Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "message.email.required", "Email is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "message.password.required",
				"Password is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "matchingPassword", "message.matchingPassword.required",
				"Password is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "message.username.required",
				"UserName is required.");
	}

}
