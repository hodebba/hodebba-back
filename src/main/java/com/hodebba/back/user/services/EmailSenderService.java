package com.hodebba.back.user.services;

import com.hodebba.back.user.models.Mail;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Locale;

public interface EmailSenderService {

	void sendEmail(Mail mail, String template, Locale locale) throws MessagingException, IOException;

}
