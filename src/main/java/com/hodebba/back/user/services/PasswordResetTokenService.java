package com.hodebba.back.user.services;

import com.hodebba.back.user.models.User;

import java.util.Optional;

public interface PasswordResetTokenService {

	Optional<User> getUserByPasswordResetToken(final String token);

	void deleteByToken(String token);

	void createPasswordResetTokenForUser(final User user, final String token);

}
