package com.hodebba.back.user.services;

import com.hodebba.back.core.pagination.PageSearch;
import com.hodebba.back.user.dto.FacebookDto;
import com.hodebba.back.user.dto.UserDto;
import com.hodebba.back.user.dto.UserUpdateDto;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.models.VerificationToken;
import org.springframework.data.domain.Page;

import java.util.Locale;

public interface UserService {

	public static final String TOKEN_INVALID = "invalidToken";
	public static final String TOKEN_EXPIRED = "expired";
	public static final String TOKEN_VALID = "valid";

	User findById(long id);

	User findByEmail(String email);

	User findByUsername(String username);

	Page<User> findAll(PageSearch pageSearch);

	void deleteById(long userId);

	User create(UserDto userDto, Locale locale);

	User update(UserUpdateDto userUpdateDto, Locale locale);

	User updateEmail(User user, String email, Locale locale);

	boolean checkIfValidOldPassword(final User user, final String oldPassword);

	void changeUserPassword(final User user, final String password);

	void createVerificationTokenForUser(final User user, final String token);

	VerificationToken generateNewVerificationToken(long userId);

	User getUserByRegistrationToken(String token);

	String validateVerificationToken(String token);

	String promoteRole(long id, String role);

	boolean isAdmin(User user);

	User continueWithFacebookOrGoogle(FacebookDto facebookDto, Locale locale);

}
