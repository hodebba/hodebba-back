package com.hodebba.back.user.services.impl;

import com.hodebba.back.user.models.PasswordResetToken;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.persistence.PasswordResetTokenDao;
import com.hodebba.back.user.services.PasswordResetTokenService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class PasswordResetTokenImpl implements PasswordResetTokenService {

	private final PasswordResetTokenDao passwordResetTokenDao;

	@Override
	public void createPasswordResetTokenForUser(final User user, final String token) {
		final PasswordResetToken myToken = new PasswordResetToken(token, user);
		passwordResetTokenDao.save(myToken);
	}

	@Override
	public Optional<User> getUserByPasswordResetToken(final String token) {
		return Optional.ofNullable(passwordResetTokenDao.findByToken(token).getUser());
	}

	@Override
	public void deleteByToken(String token) {
		this.passwordResetTokenDao.deleteByToken(token);
	}

}