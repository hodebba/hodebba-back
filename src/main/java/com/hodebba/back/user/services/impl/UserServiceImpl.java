package com.hodebba.back.user.services.impl;

import com.hodebba.back.core.pagination.PageSearch;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.user.dto.FacebookDto;
import com.hodebba.back.user.dto.UserDto;
import com.hodebba.back.user.dto.UserUpdateDto;
import com.hodebba.back.user.error.UserAlreadyExistException;
import com.hodebba.back.user.models.*;
import com.hodebba.back.user.persistence.DataJpaUserDao;
import com.hodebba.back.user.persistence.PasswordResetTokenDao;
import com.hodebba.back.user.persistence.RoleDao;
import com.hodebba.back.user.persistence.VerificationTokenDao;
import com.hodebba.back.user.services.UserService;
import lombok.AllArgsConstructor;
import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Lazy
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {


	@Autowired
	private DebateService debateService;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private PasswordResetTokenDao passwordResetTokenDao;

	@Autowired
	private VerificationTokenDao verificationTokenDao;

	private final DataJpaUserDao userDao;

	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private MessageSource messages;

	@Override
	public User findById(long id) {
		Optional<User> optUser = userDao.findById(id);
		if (optUser.isPresent()) {
			return optUser.get();
		} else {
			throw new NoSuchElementException("No user with this id:" + id);
		}
	}

	@Override
	public User findByEmail(String email) {
		User user = userDao.findByEmail(email);
		return user;
	}

	@Override
	public User findByUsername(String username) {
		User user = userDao.findByUsername(username);
		return user;
	}

	private boolean emailExists(final String email) {
		return userDao.findByEmail(email) != null;
	}

	private boolean usernameExists(final String username) {
		return userDao.findByUsername(username) != null;
	}

	@Override
	public Page<User> findAll(PageSearch pageSearch) {
		return userDao.findAll(pageSearch);
	}

	@Transactional
	@Override
	public void deleteById(long userId) {
		User user = null;
		Optional<User> optUser = userDao.findById(userId);

		if (optUser.isPresent()) {
			user = optUser.get();
		}
		// check if this user exists
		if (user == null) {
			throw new NoSuchElementException("No user found with this id: " + userId);
		}

		List <Debate> debates = debateService.findByUser(user);
		User anonymous = userDao.findByUsername("Anonyme");
		User finalUser = user;
		for(Debate debate: debates){
			debate.setUser(anonymous);
			debateService.update(debate);
		}

		// delete the verification token if exists
		if (!user.isEnabled()) {
			VerificationToken verificationToken = this.verificationTokenDao.findByUser(user);
			if (verificationToken != null) {
				this.verificationTokenDao.deleteById(verificationToken.getId());
			}
		}
		// delete the reset password token if exists
		if (this.passwordResetTokenDao.findByUser(user) != null) {
			PasswordResetToken passwordResetToken = this.passwordResetTokenDao.findByUser(user);
			this.passwordResetTokenDao.deleteById(passwordResetToken.getId());
		}
		// delete the User
		this.userDao.deleteById(userId);
	}

	@Override
	@Transactional
	public User create(UserDto userDto, Locale locale) {

		if (emailExists(userDto.getEmail())) {
			String message = messages.getMessage("message.email.exists", null, locale);
			throw new UserAlreadyExistException(message);
		}

		if (usernameExists(userDto.getUsername())) {
			String message = messages.getMessage("message.username.exist", null, locale);
			throw new UserAlreadyExistException(message);
		}

		final User user = new User();

		user.setUsername(userDto.getUsername());
		user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
		user.setEmail(userDto.getEmail());
		user.setRoles(Arrays.asList(roleDao.findByName("ROLE_ANONYMOUS")));

		return this.userDao.save(user);
	}

	@Override
	@Transactional
	public User update(UserUpdateDto userUpdateDto, Locale locale) {
		User user = null;
		Optional<User> optUser = userDao.findById(userUpdateDto.getId());
		if (optUser.isPresent()) {
			user = optUser.get();
		}
		// check if this user exists
		if (user == null) {
			throw new NoSuchElementException("No user found with this id: " + userUpdateDto.getId());
		}
		// update username
		if (userUpdateDto.getUsername() != null) {
			if (!user.getUsername().equals(userUpdateDto.getUsername())) {
				if (userDao.findByUsername(userUpdateDto.getUsername()) == null) {
					user.setUsername(userUpdateDto.getUsername());
				} else {
					String message = messages.getMessage("message.username.exist", null, locale);
					throw new UserAlreadyExistException(message);
				}
			}
		}
		// update description
		if (userUpdateDto.getDescription() != null) {
			if (!userUpdateDto.getDescription().equals(user.getDescription())) {
				user.setDescription(userUpdateDto.getDescription());
			}
		}
		// update location
		if (userUpdateDto.getLocation() != null) {
			if (!userUpdateDto.getLocation().equals(user.getLocation())) {
				user.setLocation(userUpdateDto.getLocation());
			}
		}
		// update imageLink
		if (userUpdateDto.getImageLink() != null) {
			if (!userUpdateDto.getImageLink().equals(user.getImageLink())) {
				user.setImageLink(userUpdateDto.getImageLink());
			}
		}
		// update socials
		if (userUpdateDto.getSocials() != null) {
			Set<SocialsName> mapKeys = userUpdateDto.getSocials().keySet();
			if (!mapKeys.isEmpty()) {
				Iterator<SocialsName> iterator = mapKeys.iterator();
				while (iterator.hasNext()) {
					SocialsName socialsName = iterator.next();
					String link = userUpdateDto.getSocials().get(socialsName);
					user.getSocials().put(socialsName, link);
				}
			}
		}
		return userDao.save(user);
	}

	@Override
	@Transactional
	public User continueWithFacebookOrGoogle(FacebookDto facebookDto, Locale locale) {
		User user = userDao.findByEmail(facebookDto.getEmail());
		if (user == null) {
			user = new User();
			user.setUsername(facebookDto.getUsername());
			user.setPassword(bCryptPasswordEncoder.encode(generatePassayPassword()));
			user.setEmail(facebookDto.getEmail());
			user.setEnabled(true);
			user.setRoles(Arrays.asList(roleDao.findByName("ROLE_USER")));
			user = userDao.save(user);
		}
		return user;
	}

	@Override
	@Transactional
	public User updateEmail(User user, String email, Locale locale) {
		if (emailExists(email)) {
			String message = messages.getMessage("message.email.exists", null, locale);
			throw new UserAlreadyExistException(message);
		}
		user.setEmail(email);
		return this.userDao.save(user);
	}

	@Override
	public boolean checkIfValidOldPassword(final User user, final String oldPassword) {
		return bCryptPasswordEncoder.matches(oldPassword, user.getPassword());
	}

	@Override
	@Transactional
	public void changeUserPassword(final User user, final String password) {
		user.setPassword(bCryptPasswordEncoder.encode(password));
		userDao.save(user);
	}

	@Override
	@Transactional
	public void createVerificationTokenForUser(final User user, final String token) {
		final VerificationToken myToken = new VerificationToken(token, user);
		verificationTokenDao.save(myToken);
	}

	@Override
	@Transactional
	public VerificationToken generateNewVerificationToken(long userId) {
		User user = findById(userId);
		VerificationToken token = verificationTokenDao.findByUser(user);
		if (token == null) {
			createVerificationTokenForUser(user, UUID.randomUUID().toString());
			token = verificationTokenDao.findByUser(user);
		} else {
			token.updateToken(UUID.randomUUID().toString());
		}
		verificationTokenDao.save(token);
		return token;
	}

	public User getUserByRegistrationToken(String token) {
		VerificationToken verificationToken = verificationTokenDao.findByToken(token);
		if (verificationToken == null) {
			return null;
		}
		User user = verificationToken.getUser();
		return user;
	}

	@Override
	@Transactional
	public String validateVerificationToken(String token) {
		final VerificationToken verificationToken = verificationTokenDao.findByToken(token);
		if (verificationToken == null) {
			return TOKEN_INVALID;
		}

		final User user = verificationToken.getUser();
		final Calendar cal = Calendar.getInstance();
		if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			verificationTokenDao.delete(verificationToken);
			return TOKEN_EXPIRED;
		}

		user.setEnabled(true);
		verificationTokenDao.delete(verificationToken);
		userDao.save(user);
		promoteRole(user.getId(), "ROLE_USER");
		return TOKEN_VALID;
	}

	@Override
	@Transactional
	public String promoteRole(long id, String role) {
		Optional<User> userOpt = userDao.findById(id);
		if (userOpt.isPresent()) {
			User user = userOpt.get();
			Role roleObj = roleDao.findByName(role);
			user.setRoles(new ArrayList<>(Arrays.asList(roleObj)));
			this.userDao.save(user);
			return "valid";
		} else {
			// id invalid throw error
			return "message.promote.idInvalid";
		}
	}

	@Override
	public boolean isAdmin(User user) {
		Role admin = roleDao.findByName("ROLE_ADMIN");
		Role userRole = new ArrayList<Role>(user.getRoles()).get(0);
		if (userRole.getId() == admin.getId()) {
			return true;
		} else {
			return false;
		}
	}

	private String generatePassayPassword() {
		PasswordGenerator gen = new PasswordGenerator();
		CharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
		CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
		lowerCaseRule.setNumberOfCharacters(2);

		CharacterData upperCaseChars = EnglishCharacterData.UpperCase;
		CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
		upperCaseRule.setNumberOfCharacters(2);

		CharacterData digitChars = EnglishCharacterData.Digit;
		CharacterRule digitRule = new CharacterRule(digitChars);
		digitRule.setNumberOfCharacters(2);

		CharacterData specialChars = new CharacterData() {
			public String getErrorCode() {
				return "error";
			}

			public String getCharacters() {
				return "!@#$%^&*()_+";
			}
		};
		CharacterRule splCharRule = new CharacterRule(specialChars);
		splCharRule.setNumberOfCharacters(2);

		String password = gen.generatePassword(10, splCharRule, lowerCaseRule, upperCaseRule, digitRule);
		return password;
	}

}
