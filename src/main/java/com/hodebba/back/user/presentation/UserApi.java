package com.hodebba.back.user.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.core.pagination.PageSearch;
import com.hodebba.back.core.pagination.SearchSpecification;
import com.hodebba.back.security.JWTAuthenticationFilter;
import com.hodebba.back.security.service.SecurityUserService;
import com.hodebba.back.user.dto.*;
import com.hodebba.back.user.error.UserAlreadyExistException;
import com.hodebba.back.user.event.OnRegistrationCompleteEvent;
import com.hodebba.back.user.models.Mail;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.models.VerificationToken;
import com.hodebba.back.user.services.EmailSenderService;
import com.hodebba.back.user.services.PasswordResetTokenService;
import com.hodebba.back.user.services.UserService;
import com.hodebba.back.user.util.GenericResponse;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.*;

@RestController
@AllArgsConstructor
@CrossOrigin
public class UserApi {

	private static final Logger logger = LoggerFactory.getLogger(UserApi.class);

	@Autowired
	private ApplicationEventPublisher eventPublisher;
	private final UserService userService;
	private final PasswordResetTokenService passwordResetTokenService;
	private AuthenticationManager authenticationManager;
	private JavaMailSender mailSender;
	private MessageSource messages;
	private Environment env;
	private SecurityUserService securityUserService;
	private EmailSenderService emailSenderService;

	// ============== FIND USER ============

	// - all
	@JsonView(User.Views.LIGHT.class)
	@GetMapping(value = "/users", produces = "application/json")
	@ApiOperation(value = "Find all Users", notes = "Search for a list of Users, can specify various parameter for the search.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public Page<User> findAllUsers(@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "50") int limit,
			@RequestParam(required = false, defaultValue = "id>0") String search,
			@SortDefault.SortDefaults({@SortDefault(sort = "id", direction = Sort.Direction.ASC)}) Sort sort) {
		logger.info("Get all Users request.");
		Specification spec = SearchSpecification.parse(search);
		PageSearch pageSearch = PageSearch.PageSearchBuilder.aPageSearch().search(spec).limit(limit).offset(offset)
				.sort(sort).build();
		Page<User> pageUser = userService.findAll(pageSearch);
		Iterator<User> it = pageUser.iterator();
		List<User> listUser = new ArrayList<>();
		while (it.hasNext()) {
			User tmpUser = it.next();
			listUser.add(tmpUser);
		}
		return new PageImpl<User>(listUser, pageUser.getPageable(), pageUser.getTotalElements());
	}

	// - by id
	@GetMapping(value = "/users/{id}", produces = "application/json")
	@ApiOperation(value = "Find User by id", notes = "Find a User by its id.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public UserDisplayDto getUser(@ApiParam(value = "id of the User to find", required = true) @PathVariable long id)
			throws IOException, MessagingException {
		try {
			User user = userService.findById(id);
			return new UserDisplayDto(user);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== REGISTER USER ============

	// - create new user
	@PostMapping(value = "/users", produces = "application/json")
	@ApiOperation(value = "Create User", notes = "Create a new User and send an email for validation.")
	public void create(@ApiParam(value = "UserDto model", required = true) @RequestBody @Valid final UserDto userDto,
			HttpServletResponse res, final Locale locale, final HttpServletRequest request)
			throws ServletException, IOException {
		User user = userService.create(userDto, locale);
		logger.info("A new User has been created : " + user.toString());

		JWTAuthenticationFilter jwtAuthenticationFilter = new JWTAuthenticationFilter(authenticationManager,
				userService);
		jwtAuthenticationFilter.getTokenAndSendUserInformation(user.getEmail(), res);
		eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user, locale, request));
	}

	// - continue with Facebook or Google
	@PostMapping(value = "/users/socials", produces = "application/json")
	@ApiOperation(value = "Create user or log in with facebook or Google", notes = "Create user or log in with facebook or Google.")
	public void continueWithFacebookOrGoogle(
			@ApiParam(value = "FacebookDto model", required = true) @RequestBody @Valid final FacebookDto facebookDto,
			HttpServletResponse res, final Locale locale, final HttpServletRequest request)
			throws ServletException, IOException {
		User user = userService.continueWithFacebookOrGoogle(facebookDto, locale);
		logger.info("A new User has been logged with Facebook or Google : " + user.toString());

		JWTAuthenticationFilter jwtAuthenticationFilter = new JWTAuthenticationFilter(authenticationManager,
				userService);
		jwtAuthenticationFilter.getTokenAndSendUserInformation(user.getEmail(), res);
	}

	// - confirm user registration with email
	@GetMapping("/users/registration/confirm/{token}")
	@ApiOperation(value = "Confirm registration", notes = "Confirm the registration of a new User with its token received by email.")
	public GenericResponse confirmRegistration(final Locale locale,
			@ApiParam(value = "Registration token sent by email when the User has been created.", required = true) @PathVariable String token) {
		User user = userService.getUserByRegistrationToken(token);
		String result = userService.validateVerificationToken(token);
		if (result.equals("valid")) {
			logger.info("The User with the id : " + user.getId() + " has confirmed his registration.");
			String message = messages.getMessage("email.regitrationConfirm.valid", null, locale);
			return new GenericResponse(message);
		} else if (result.equals("invalidToken")) {
			String message = messages.getMessage("email.regitrationConfirm.invalidToken", null, locale);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
		} else if (result.equals("expired")) {
			String message = messages.getMessage("email.regitrationConfirm.expired", null, locale);
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
		} else {
			String message = messages.getMessage("email.regitrationConfirm.error", null, locale);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
		}
	}

	// - resend email for user confirm registration
	@GetMapping("/users/registration/resend-token/{id}")
	@ApiOperation(value = "Resend registration token", notes = "Send a new email to the User with a new token for validation.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public GenericResponse resendRegistrationToken(final Locale locale,
			@ApiParam(value = "id of the User to send the token", required = true) @PathVariable long id,
			final HttpServletRequest request) throws IOException, MessagingException {
		User user = null;
		try {
			user = userService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
		if (user.isEnabled()) {
			// this user is already confirmed
			String message = messages.getMessage("email.regitrationConfirm.alreadyConfirmed", null, locale);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
		}
		VerificationToken token = userService.generateNewVerificationToken(id);

		Mail mail = new Mail();
		mail.setFrom(env.getProperty("support.email"));
		mail.setMailTo(user.getEmail());
		mail.setSubject(messages.getMessage("email.regitrationConfirm.subject", null, locale));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("link", env.getProperty("frontEnd.localUrl") + "/confirm-email/" + token.getToken());
		model.put("username", user.getUsername());
		mail.setProps(model);

		emailSenderService.sendEmail(mail, "confirm-account", locale);

		logger.info("A registration email has been sent again to {}.", user.getEmail());

		String message = messages.getMessage("email.regitrationConfirm.sent", null, locale);
		return new GenericResponse(message);
	}

	// ============== DELETE USER ============
	@DeleteMapping(value = "/users/{id}", produces = "application/json")
	@ApiOperation(value = "Delete User", notes = "Delete a User.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of the user or of an admin", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void delete(@ApiParam(value = "id of the User to delete", required = true) @PathVariable long id,
			final Locale locale, Principal principal) {
		String loggedEmail = principal.getName();
		User loggedUser = userService.findByEmail(loggedEmail);
		if (!userService.isAdmin(loggedUser)) {
			try {
				User user = userService.findById(id);

				if (!loggedEmail.equals(user.getEmail())) {
					String message = messages.getMessage("deleteUser.unauthorized", null, locale);
					throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
				}
			} catch (NoSuchElementException e) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
			}
		}

		try {
			userService.deleteById(id);
			logger.info("The User with the id : " + id + " has been deleted.");
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== UPDATE USER ============

	@PutMapping(value = "users", produces = "application/json")
	@ApiOperation(value = "Update User", notes = "Update info of a User.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public UserDisplayDto updateUser(
			@ApiParam(value = "UserUpdateDto model", required = true) @RequestBody @Valid UserUpdateDto userUpdateDto,
			final Locale locale) {
		try {
			User user = userService.update(userUpdateDto, locale);
			logger.info("A User has been updated : " + user.toString());
			return new UserDisplayDto(user);
		} catch (NoSuchElementException | UserAlreadyExistException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	// - Update email when user is already connected
	@PutMapping(value = "/users/email", produces = "application/json")
	@ApiOperation(value = "Update User email", notes = "Update the email of a User.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public UserDisplayDto updateEmail(
			@ApiParam(value = "EmailDto model", required = true) @RequestBody @Valid final EmailDto emailDto,
			final Locale locale, Principal principal) {
		String loggedEmail = principal.getName();

		try {
			User user = userService.findById(emailDto.getUserId());

			if (!loggedEmail.equals(user.getEmail())) {
				String message = messages.getMessage("deleteUser.unauthorized", null, locale);
				throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
			}

			user = userService.updateEmail(user, emailDto.getEmail(), locale);
			logger.info("The User with the id : " + user.getId() + " has updated his email to " + user.getEmail());
			return new UserDisplayDto(user);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - Update password when user is already connected
	@PutMapping(value = "/users/password", produces = "application/json")
	@ApiOperation(value = "Update User password", notes = "Update the password of a User if the user is logged in.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public GenericResponse changeUserPassword(final Locale locale, Principal principal,
			@ApiParam(value = "PasswordDto model\nThe parameter \"token\" is not necessary here.", required = true) @RequestBody @Valid PasswordDto passwordDto) {
		String loggedEmail = principal.getName();
		User user = null;
		try {
			user = userService.findById(passwordDto.getUserId());
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
		if (!loggedEmail.equals(user.getEmail())) {
			String message = messages.getMessage("updatePassword.unauthorized", null, locale);
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
		}
		if (!userService.checkIfValidOldPassword(user, passwordDto.getOldPassword())) {
			String message = messages.getMessage("oldPassword.invalid", null, locale);
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
		}
		userService.changeUserPassword(user, passwordDto.getNewPassword());
		logger.info("The User with the id : " + user.getId() + " has updated his password.");
		return new GenericResponse(messages.getMessage("message.updatePasswordSuc", null, locale));
	}

	// - Update role of a user
	// - to admin role
	@GetMapping(value = "/users/promote/admin/{id}", produces = "application/json")
	@ApiOperation(value = "Promote to Administrator", notes = "Promote a User to the role Administrator")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public GenericResponse promoteAdmin(final Locale locale,
			@ApiParam(value = "id of the User to promote", required = true) @PathVariable long id) {
		String messageCode = userService.promoteRole(id, "ROLE_ADMIN");
		if (messageCode.equals("valid")) {
			messageCode = "message.promote.admin";
			logger.info("The User with the id : " + id + " has been promoted to the administrator role.");
		}
		String message = messages.getMessage(messageCode, null, locale);
		return new GenericResponse(message);
	}

	// - to moderator role
	@GetMapping(value = "/users/promote/moderator/{id}", produces = "application/json")
	@ApiOperation(value = "Promote to Moderator", notes = "Promote a User to the role Moderator")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public GenericResponse promoteModerator(final Locale locale,
			@ApiParam(value = "id of the User to promote", required = true) @PathVariable long id) {
		String messageCode = userService.promoteRole(id, "ROLE_MODERATOR");
		if (messageCode.equals("valid")) {
			messageCode = "message.promote.mod";
			logger.info("The User with the id : " + id + " has been promoted to the moderator role.");
		}
		String message = messages.getMessage(messageCode, null, locale);
		return new GenericResponse(message);
	}

	// - to user role
	@GetMapping(value = "/users/promote/user/{id}", produces = "application/json")
	@ApiOperation(value = "Promote to User", notes = "Promote a User to the role User")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public GenericResponse promoteUser(final Locale locale,
			@ApiParam(value = "id of the User to promote", required = true) @PathVariable long id) {
		String messageCode = userService.promoteRole(id, "ROLE_USER");
		if (messageCode.equals("valid")) {
			messageCode = "message.promote.user";
			logger.info("The User with the id : " + id + " has been promoted to the user role.");
		}
		String message = messages.getMessage(messageCode, null, locale);
		return new GenericResponse(message);
	}

	// ============== RESET PASSWORD PROCESS ============

	// - Send reset password email
	@PostMapping(value = "/users/password/reset/{email}", produces = "application/json")
	@ApiOperation(value = "Reset password", notes = "Reset the password of a User and send a token by email.")
	public GenericResponse resetPassword(
			@ApiParam(value = "email of the User to reset", required = true) @PathVariable final String email,
			final HttpServletRequest request, final Locale locale, HttpServletResponse servletResponse)
			throws MessagingException, IOException {
		final User user = userService.findByEmail(email);

		if (user != null) {
			final String token = UUID.randomUUID().toString();
			passwordResetTokenService.createPasswordResetTokenForUser(user, token);

			logger.info("The User with the id : " + user.getId() + " has reset his password.");

			Mail mail = new Mail();
			mail.setFrom(env.getProperty("support.email"));
			mail.setMailTo(user.getEmail());
			mail.setSubject(messages.getMessage("email.resetPassword.subject", null, locale));
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("link", env.getProperty("frontEnd.localUrl") + "/save-password/" + token);
			mail.setProps(model);
			emailSenderService.sendEmail(mail, "reset-password-email", locale);

			logger.info("A reset password email has been sent to " + user.getEmail());

			String message = messages.getMessage("message.resetPassword.emailSent", null, locale);
			return new GenericResponse(message);
		} else {
			String message = messages.getMessage("message.invalidUser", null, locale);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
		}

	}

	// - check if reset password token is still valid
	@GetMapping(value = "/users/password/valid/{token}", produces = "application/json")
	@ApiOperation(value = "Check password token", notes = "Check if the token is a valid reset password token.")
	public GenericResponse checkPasswordToken(final Locale locale, final Model model,
			@ApiParam(value = "token to check", required = true) @PathVariable final String token) {
		final String result = securityUserService.validatePasswordResetToken(token);

		if (result != null) {
			String message = messages.getMessage("message.resetPassword." + result, null, locale);
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
		} else {
			model.addAttribute("token", token);
			String message = messages.getMessage("message.resetPassword.validToken", null, locale);
			return new GenericResponse(message);
		}
	}

	// - Save new password
	@PostMapping(value = "/users/password/save", produces = "application/json")
	@ApiOperation(value = "Save password", notes = "Save the new password of a User after it has been reset.")
	public GenericResponse savePassword(final Locale locale,
			@ApiParam(value = "PasswordDto model\nThe parameters \"userId\" and \"oldPassword\" are not necessary here.", required = true) @RequestBody @Valid PasswordDto passwordDto) {
		final String result = securityUserService.validatePasswordResetToken(passwordDto.getToken());
		if (result != null) {
			String message = messages.getMessage("message.resetPassword." + result, null, locale);
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
		}

		if (!passwordDto.getNewPassword().equals(passwordDto.getMatchingPassword())) {
			String message = messages.getMessage("PasswordMatches.not", null, locale);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
		}

		Optional<User> user = passwordResetTokenService.getUserByPasswordResetToken(passwordDto.getToken());
		if (user.isPresent()) {
			userService.changeUserPassword(user.get(), passwordDto.getNewPassword());
			passwordResetTokenService.deleteByToken(passwordDto.getToken());
			logger.info("The User with the id : " + user.get().getId() + " has saved his new password.");
			return new GenericResponse(messages.getMessage("message.resetPasswordSuc", null, locale));
		} else {
			String message = messages.getMessage("message.invalidUser", null, locale);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
		}
	}

	// ============== NON-API ============
	private SimpleMailMessage constructEmail(String subject, String body, User user) {
		final SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject(subject);
		email.setText(body);
		email.setTo(user.getEmail());
		email.setFrom(env.getProperty("support.email"));
		return email;
	}

}
