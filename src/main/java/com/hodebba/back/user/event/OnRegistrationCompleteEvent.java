package com.hodebba.back.user.event;

import com.hodebba.back.user.models.User;
import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class OnRegistrationCompleteEvent extends ApplicationEvent {

	private final Locale locale;
	private final User user;
	private final HttpServletRequest request;

	public OnRegistrationCompleteEvent(final User user, final Locale locale, final HttpServletRequest request) {
		super(user);
		this.user = user;
		this.locale = locale;
		this.request = request;
	}

	public Locale getLocale() {
		return locale;
	}

	public User getUser() {
		return user;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

}
