package com.hodebba.back.user.event;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.hodebba.back.user.models.Mail;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.EmailSenderService;
import com.hodebba.back.user.services.UserService;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationListener.class);

	@Autowired
	private UserService userService;

	@Autowired
	private MessageSource messages;

	@Autowired
	private Environment env;

	@Autowired
	private EmailSenderService emailSenderService;

	@SneakyThrows
	@Override
	public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
		this.confirmRegistration(event);
	}

	private void confirmRegistration(final OnRegistrationCompleteEvent event) throws IOException, MessagingException {
		final User user = event.getUser();
		final String token = UUID.randomUUID().toString();
		userService.createVerificationTokenForUser(user, token);

		Mail mail = new Mail();
		mail.setFrom(env.getProperty("support.email"));
		mail.setMailTo(user.getEmail());
		mail.setSubject(messages.getMessage("email.regitrationConfirm.subject", null, event.getLocale()));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("link", env.getProperty("frontEnd.localUrl") + "/confirm-email/" + token);
		model.put("username", user.getUsername());
		mail.setProps(model);

		emailSenderService.sendEmail(mail, "confirm-account", event.getLocale());

		logger.info("A registration email has been sent to " + user.getEmail());
	}

}
