package com.hodebba.back.user.persistence;

import com.hodebba.back.core.pagination.PageSearch;
import com.hodebba.back.user.models.User;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DataJpaUserDao extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
	default Page<User> findAll(PageSearch<User> page) {
		return findAll(page.getSearch(), page);
	}
	User findByEmail(String email);
	User findByUsername(String username);
	default long count(PageSearch page) {
		return count(page.getSearch());
	}

	@Override
	<S extends User> S save(S s);

	@Override
	void delete(User user);
}
