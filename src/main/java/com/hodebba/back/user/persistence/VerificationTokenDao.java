package com.hodebba.back.user.persistence;

import com.hodebba.back.user.models.User;
import com.hodebba.back.user.models.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.stream.Stream;

public interface VerificationTokenDao extends JpaRepository<VerificationToken, Long> {
	VerificationToken findByToken(String token);

	VerificationToken findByUser(User user);

	Stream<VerificationToken> findAllByExpiryDateLessThan(Date now);

	void deleteByExpiryDateLessThan(Date now);

	@Modifying
	@Query("delete from verification_token t where t.expiryDate <= ?1")
	void deleteAllExpiredSince(Date now);
}
