package com.hodebba.back.user.persistence;

import com.hodebba.back.user.models.PasswordResetToken;
import com.hodebba.back.user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.stream.Stream;

@Repository
@Transactional
public interface PasswordResetTokenDao extends JpaRepository<PasswordResetToken, Long> {

	PasswordResetToken findByToken(String token);

	PasswordResetToken findByUser(User user);

	Stream<PasswordResetToken> findAllByExpiryDateLessThan(Date now);

	void deleteByExpiryDateLessThan(Date now);

	@Override
	<S extends PasswordResetToken> S save(S s);

	void deleteByToken(String token);

	/* TODO: Must be used in a weekly cleanup script */
	@Modifying
	@Query("delete from password_reset_token t where t.expiryDate <= ?1")
	void deleteAllExpiredSince(Date now);
}
