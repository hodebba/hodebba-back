package com.hodebba.back.debate.persistence;

import com.hodebba.back.debate.models.Tweet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;

public interface TweetDao extends JpaRepository<Tweet, Long> {
	Optional<Tweet> findByLink(String link);
}
