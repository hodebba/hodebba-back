package com.hodebba.back.debate.persistence;


import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.Participant;
import com.hodebba.back.user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ParticipantDao extends JpaRepository<Participant, Long> {
    Optional<Participant> findByUserAndDebate(User user, Debate debate);
    List<Participant> findByUser(User user);
}
