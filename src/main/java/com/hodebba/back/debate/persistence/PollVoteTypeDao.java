package com.hodebba.back.debate.persistence;

import com.hodebba.back.debate.models.PollVoteType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PollVoteTypeDao extends JpaRepository<PollVoteType, Long> {

}
