package com.hodebba.back.debate.persistence;

import com.hodebba.back.debate.models.Poll;
import com.hodebba.back.debate.models.PollVote;
import com.hodebba.back.user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PollVoteDao extends JpaRepository<PollVote, Long> {

	List<PollVote> findByUser(User user);
	List<PollVote> findByPoll(Poll poll);
	List<PollVote> findByUserAndPoll(User user, Poll poll);
	void deleteAllByPoll(Poll poll);

}
