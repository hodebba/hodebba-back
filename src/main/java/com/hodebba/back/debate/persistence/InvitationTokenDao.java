package com.hodebba.back.debate.persistence;

import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.InvitationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvitationTokenDao extends JpaRepository<InvitationToken, Long> {
        InvitationToken findByToken(String token);
        InvitationToken findByDebate(Debate debate);
        void deleteAllByDebate(Debate debate);
}

