package com.hodebba.back.debate.persistence;

import com.hodebba.back.debate.models.Poll;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PollDao extends JpaRepository<Poll, Long> {

}
