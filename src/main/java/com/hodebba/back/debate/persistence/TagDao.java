package com.hodebba.back.debate.persistence;

import com.hodebba.back.debate.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TagDao extends JpaRepository<Tag, Long> {

	Optional<Tag> findByLabel(String label);

}
