package com.hodebba.back.debate.persistence;

import com.hodebba.back.debate.models.Theme;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ThemeDao extends JpaRepository<Theme, Long> {

	Optional<Theme> findByLabel(String label);

}
