package com.hodebba.back.debate.persistence;

import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.DebateState;
import com.hodebba.back.user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DebateDao extends JpaRepository<Debate, Long> {

	List<Debate> findByUser(User user);
	List<Debate> findByDebateState(DebateState debateState);

	@Query(value = "SELECT * FROM public.debate d WHERE d.is_private=false AND d.debate_state='published' ORDER BY d.created_date DESC", nativeQuery = true)
	List<Debate> findAllByOrderByCreatedDateDesc();

	List<Debate> findAllByIsPrivateTrue();
	List<Debate> findAllByIsPrivateFalse();
}
