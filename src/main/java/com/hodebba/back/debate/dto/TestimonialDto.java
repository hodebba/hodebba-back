package com.hodebba.back.debate.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "TestimonialDto", description = "Model for creation of a new Testimonial, contain only necessary info.")
public class TestimonialDto {

	@ApiModelProperty(notes = "Title of the testimonial", required = true, position = 0)
	private String title;

	@NotNull
	@Lob
	@ApiModelProperty(notes = "Content of the testimonial", required = true, position = 1)
	private String content;

	@NotNull
	@ApiModelProperty(notes = "Id of the User who created the testimonial", required = true, position = 4)
	private Long userId;

	@NotNull
	@ApiModelProperty(notes = "Id of the Debate to which belongs the testimonial", required = true, position = 5)
	private Long debateId;

}
