package com.hodebba.back.debate.dto;

import com.hodebba.back.debate.models.DebateState;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;

@Getter
@ApiModel(value = "DebateUpdateDto", description = "Model for updating a Debate.")
public class DebateUpdateDto {

	@NotNull
	@ApiModelProperty(notes = "id of the debate to update.", required = true, position = 0)
	private Long id;

	@Size(min = 5)
	@ApiModelProperty(notes = "Title of the debate", required = false, position = 1)
	private String title;

	@ApiModelProperty(notes = "Id of the Theme of the debate", required = false, position = 2)
	private Long themeId;

	@ApiModelProperty(notes = "Tags of the debate", required = false, position = 3)
	private Collection<String> tags;

	@ApiModelProperty(notes = "Background image link of the debate", required = false, position = 4)
	private String imageLink;

	@Lob
	@ApiModelProperty(notes = "Presentation of the debate (contains HTML nodes)", required = false, position = 5)
	private String presentation;

	@ApiModelProperty(notes = "State of the debate (draft, published or archived)", required = false, position = 6)
	private DebateState debateState;

	@ApiModelProperty(notes = "Tweets of the debate", required = false, position = 7)
	private Collection<String> tweets;

	@ApiModelProperty(notes = "isPrivate", required = false, position = 8)
	private Boolean isPrivate;

	@ApiModelProperty(notes = "createdByCommunity", required = false, position = 9)
	private Boolean createdByCommunity;

}
