package com.hodebba.back.debate.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "ThemeDto", description = "Model for creation of a new Theme, contain only necessary info.")
public class ThemeDto {

	@NotNull
	@ApiModelProperty(notes = "Label of the Theme", required = true, position = 0)
	private String label;

}
