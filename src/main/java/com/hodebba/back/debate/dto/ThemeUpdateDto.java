package com.hodebba.back.debate.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "ThemeUpdateDto", description = "Model to update an existing Theme.")
public class ThemeUpdateDto {

	@NotNull
	@ApiModelProperty(notes = "Id of the Theme to update", required = true, position = 0)
	private long id;

	@NotNull
	@ApiModelProperty(notes = "New label of the Theme", required = true, position = 1)
	private String label;

}
