package com.hodebba.back.debate.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "PollVoteDto", description = "Model for creation of a new Vote in a poll.")
public class PollVoteDto {

	@NotNull
	@ApiModelProperty(notes = "Id of the poll", required = true, position = 0)
	private long pollId;

	@NotNull
	@ApiModelProperty(notes = "Id of the poll option the user wants to vote for", required = true, position = 0)
	private long pollOptionId;

	@NotNull
	@ApiModelProperty(notes = "Id of the user voting", required = true, position = 0)
	private long userId;
}
