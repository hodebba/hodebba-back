package com.hodebba.back.debate.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "DebateIdDto", description = "Model for updating views of a debate.")
public class DebateIdDto {
	@NotNull
	@ApiModelProperty(notes = "Id of the debate", required = true, position = 0)
	private Long debateId;
}
