package com.hodebba.back.debate.dto;

import com.hodebba.back.debate.models.DebateRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(value = "InvitationDto", description = "Model or sending an invitation to a private debate.")
public class InvitationDto {
    @NotNull
    @ApiModelProperty(notes = "Id of the debate", required = true, position = 1)
    private Long debateId;

    @NotNull
    @ApiModelProperty(notes = "Guest's email", required = true, position = 2)
    private String email;

    @NotNull
    @ApiModelProperty(notes = "Guest's role", required = true, position = 3)
    private DebateRole role;

    @ApiModelProperty(notes = "Optional sent to the guest", required = false, position = 4)
    private String message;
}

