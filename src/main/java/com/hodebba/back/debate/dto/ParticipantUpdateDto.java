package com.hodebba.back.debate.dto;

import com.hodebba.back.debate.models.DebateRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;


@Getter
@ApiModel(value = "ParticipantUpdateDto", description = "Model for updating a Participant.")
public class ParticipantUpdateDto {

    @NotNull
    @ApiModelProperty(notes = "Id of the participant", required = true, position = 1)
    private Long id;

    @NotNull
    @ApiModelProperty(notes = "Participant's role", required = true, position = 2)
    private DebateRole role;

    @NotNull
    @ApiModelProperty(notes = "Debate ID", required = true, position = 3)
    private Long debateId;

}
