package com.hodebba.back.debate.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;

@Getter
@ApiModel(value = "DebateDto", description = "Model for creation of a new Debate, contain only necessary info.")
public class DebateDto {
	@NotNull
	@Size(min = 5)
	@ApiModelProperty(notes = "Title of the debate", required = true, position = 0)
	private String title;

	@NotNull
	@ApiModelProperty(notes = "Id of the Theme of the debate", required = true, position = 1)
	private Long themeId;

	@ApiModelProperty(notes = "List of tag of the debate", required = true, position = 2)
	private Collection<String> tags;

	@NotNull
	@ApiModelProperty(notes = "Background image link of the debate", required = true, position = 3)
	private String imageLink;

	@NotNull
	@Lob
	@ApiModelProperty(notes = "Presentation of the debate (contains HTML nodes)", required = true, position = 4)
	private String presentation;

	@NotNull
	@ApiModelProperty(notes = "Id of the User who created the debate", required = true, position = 5)
	private Long userId;

	@NotNull
	@ApiModelProperty(notes = "PollDto model", required = true, position = 6)
	private PollDto poll;

	@NotNull
	@ApiModelProperty(notes = "Debate public or private", required = true, position = 7)
	private Boolean isPrivate;

	@NotNull
	@ApiModelProperty(notes = "Debate created By Community or not", required = true, position = 8)
	private Boolean createdByCommunity;
}
