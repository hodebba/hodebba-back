package com.hodebba.back.debate.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Getter
@ApiModel(value = "PollDto", description = "Model for creation of a new Poll, contain only necessary info.")
public class PollDto {

	@NotNull
	@ApiModelProperty(notes = "Question of the poll", required = true, position = 0)
	private String question;

	@NotNull
	@NotEmpty
	@ApiModelProperty(notes = "List of options of the poll", required = true, position = 1)
	private Collection<String> pollOptions;

}
