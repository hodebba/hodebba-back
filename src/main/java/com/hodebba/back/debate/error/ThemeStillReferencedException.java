package com.hodebba.back.debate.error;

public class ThemeStillReferencedException extends Exception {

	public ThemeStillReferencedException() {
	}

	public ThemeStillReferencedException(String message) {
		super(message);
	}

	public ThemeStillReferencedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ThemeStillReferencedException(Throwable cause) {
		super(cause);
	}

	public ThemeStillReferencedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
