package com.hodebba.back.debate.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.dto.DebateDto;
import com.hodebba.back.debate.dto.DebateIdDto;
import com.hodebba.back.debate.dto.DebateUpdateDto;
import com.hodebba.back.debate.dto.InvitationDto;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.DebateState;
import com.hodebba.back.debate.models.Theme;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.debate.services.ElasticSearchService;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.util.GenericResponse;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.TreeMap;

@RestController
@AllArgsConstructor
@CrossOrigin
public class DebateApi {

	private static final Logger logger = LoggerFactory.getLogger(DebateApi.class);

	@Autowired
	private final DebateService debateService;

	@Autowired
	private final ElasticSearchService elasticSearchService;

	@Autowired
	private MessageSource messages;

	// ============== GET DEBATE ============
	// - by id
	@GetMapping(value = "/debates/{id}", produces = "application/json")
	@ApiOperation(value = "Find debate by id", notes = "Find a debate by its id.")
	@JsonView(Debate.Views.Full.class)
	public Debate getDebate(@ApiParam(value = "id of the Debate to find", required = true) @PathVariable long id) {
		try {
			Debate debate = debateService.findById(id);
			if(debate.getIsPrivate()){
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}else{
				return debate;
			}
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - by id (private)
	@GetMapping(value = "/debates/{id}/private", produces = "application/json")
	@ApiOperation(value = "Find private debate by id", notes = "Find a private debate by its id.")
	@JsonView(Debate.Views.Full.class)
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public Debate getPrivateDebate(@ApiParam(value = "id of the private Debate to find", required = true) @PathVariable long id, Principal principal) {
		try {
			String userEmail = principal.getName();
			return debateService.findPrivateById(id, userEmail);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - by id (private debate, public route)
	@GetMapping(value = "/debates/{id}/private-public", produces = "application/json")
	@ApiOperation(value = "Find private debate by id", notes = "Public route returning only id, imageLink, title and debateState")
	@JsonView(Debate.Views.DebatePreview.class)
	public Debate getPrivateDebatePublicRoute(@ApiParam(value = "id of the private Debate to find", required = true) @PathVariable long id) {
		try {
			Debate debate = debateService.findById(id);
			if(!debate.getIsPrivate()){
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			}else{
				return debate;
			}
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}


	// - all
	@GetMapping(value = "/debates", produces = "application/json")
	@ApiOperation(value = "Find all debates", notes = "Find the list of all debates.")
	@JsonView(Debate.Views.Title.class)
	public List<Debate> findAllDebates() {
		return debateService.findAll();

	}

	// - all privates
	@GetMapping(value = "/debates/private", produces = "application/json")
	@ApiOperation(value = "Find all private debates", notes = "Find the list of all private debates.")
	@JsonView(Debate.Views.Title.class)
	public List<Debate> findAllPrivateDebates() {
		return debateService.findAllPrivate();

	}

	// - by debateState
	@GetMapping(value = "/debates/debate-state/{debateState}", produces = "application/json")
	@ApiOperation(value = "Find all debates by DebateState", notes = "Find the list of all debates with a certain debateState.")
	@JsonView(Debate.Views.ForSearch.class)
	public List<Debate> findDebatesByDebateState(
			@ApiParam(value = "debateState of the debates to find", required = true) @PathVariable DebateState debateState) {
		return debateService.findByDebateState(debateState);
	}

	// - group by theme
	@GetMapping(value = "/debates/home", produces = "application/json")
	@ApiOperation(value = "Find all debates ", notes = "Find the list of all debates.")
	@JsonView(Debate.Views.Light.class)
	public TreeMap<Theme, List<Debate>> findDebatesGroupByTheme() {
		return debateService.findDebatesGroupByTheme();
	}

	// ============== CREATE DEBATE ============
	// - create new debate (draft state)
	@PostMapping(value = "/debates", produces = "application/json")
	@ApiOperation(value = "Create debate", notes = "Create a new debate (draft state).")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Debate.Views.Full.class)
	public Debate create(
			@ApiParam(value = "DebateDto model", required = true) @RequestBody @Valid final DebateDto debateDto,
			final Locale locale, Principal principal) {
		try {
			Debate debate = debateService.create(debateDto, locale, principal);
			logger.info("A new Debate has been created : {}.", debate);
			return debate;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== UPDATE DEBATE ============
	@PutMapping(value = "/debates", produces = "application/json")
	@ApiOperation(value = "Update Debate", notes = "Update data of a debate.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Debate.Views.Full.class)
	public Debate updateDebate(
			@ApiParam(value = "DebateUpdateDto model", required = true) @RequestBody @Valid DebateUpdateDto debateUpdateDto,
			final Locale locale, Principal principal) {
		try {
			Debate debate = debateService.update(debateUpdateDto, locale, principal);
			logger.info("A debate has been updated : {}.", debate);
			return debate;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== UPDATE ES INDEX ============
	@PostMapping(value = "/debates/update-es", produces = "application/json")
	@ApiOperation(value = "Copy all debates to ES", notes = "Copy all debates to ES.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void updateEs() {
		try {
			elasticSearchService.sendAllDebatesToEs();
			logger.info("ES debates index has been updated.");
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== DELETE DEBATE ============
	@DeleteMapping(value = "/debates/{id}", produces = "application/json")
	@ApiOperation(value = "Delete Debate", notes = "Delete an existing Debate")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void deleteDebate(@ApiParam(value = "id of the Debate to delete", required = true) @PathVariable long id,
			final Locale locale, Principal principal) {
		try {
			debateService.delete(id, locale, principal);
			logger.info("The Debate with the id : {}  has been deleted.", id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============ DEBATE VIEWS ==============
	@PostMapping(value = "/debates/views", produces = "application/json")
	@ApiOperation(value = "Debate views", notes = "Increase the number of views of the debate.")
	public void incrementViews(@RequestBody DebateIdDto debateIdDto) {
		try {
			debateService.incrementViews(debateIdDto.getDebateId());
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== SEND INVITATION ============
	@PostMapping(value = "/debates/invite", produces = "application/json")
	@ApiOperation(value = "Invite user", notes = "Invite user")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Debate.Views.Full.class)
	public void invite(
			@ApiParam(value = "DebateDto model", required = true) @RequestBody @Valid final InvitationDto invitationDto,
			final Locale locale, Principal principal) {
		String loggedEmail = principal.getName();
		debateService.sendInvitation(loggedEmail, invitationDto, locale);
	}

	// ============== CONFIRM INVITATION ============
	@GetMapping("/debates/invite/{token}")
	@ApiOperation(value = "Confirm invitation", notes = "User can join this private debate.")
	public GenericResponse confirmInvitation(final Locale locale,
											   @ApiParam(value = "Invitation token sent by email.", required = true) @PathVariable String token, Principal principal) {
		String loggedEmail = principal.getName();
		String result = debateService.validateInvitationToken(token, loggedEmail);
		if (result.equals("valid")) {
			logger.info("The User with the email : " + loggedEmail+ " has join a new debate.");
			String message = messages.getMessage("email.invitationConfirm.valid", null, locale);
			return new GenericResponse(message);
		} else if (result.equals("invalidToken")) {
			String message = messages.getMessage("email.invitationConfirm.invalidToken", null, locale);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
		} else if (result.equals("expired")) {
			String message = messages.getMessage("email.invitationConfirm.expired", null, locale);
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
		} else {
			String message = messages.getMessage("email.invitationConfirm.error", null, locale);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
		}
	}

}