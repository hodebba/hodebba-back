package com.hodebba.back.debate.presentation;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@AllArgsConstructor
@CrossOrigin
public class CloudinaryApi {

	// ============== CLOUDINARY ============
	// Delete one background image of debates folder
	@DeleteMapping(value = "/cloudinary/debates/{id}", produces = "application/json")
	public void deleteImageDebates(@PathVariable String id) throws IOException {
		// TODO: Make it with Singleton and environnement variable
		// private Cloudinary cloudinary = Singleton.getCloudinary();
		Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap("cloud_name", "dbqmtrilv", "api_key",
				"798681417477252", "api_secret", "mn06kGKRVW6XlZr3ctM5Z8ssnxY"));
		cloudinary.uploader().destroy("debates/" + id, ObjectUtils.emptyMap());
	}
}
