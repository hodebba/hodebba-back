package com.hodebba.back.debate.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.Tweet;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.debate.services.TweetService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/debates")
public class TweetApi {

	private static final Logger logger = LoggerFactory.getLogger(TweetApi.class);

	@Autowired
	private final TweetService tweetService;

	@Autowired
	private final DebateService debateService;

	// ============== DELETE TWEET ==============
	// -by id
	@DeleteMapping(value = "/tweets/{id}", produces = "application/json")
	@ApiOperation(value = "Delete a tweet by id", notes = "Delete a tweet by id.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void deleteTweetById(@ApiParam(value = "id of the Tweet to delete", required = true) @PathVariable long id) {
		try {
			tweetService.delete(id);
			logger.info("The Tweet with the id : {} has been deleted.", id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== GET TWEETS ==============
	// -by debate id
	@GetMapping(value = "/{debateId}/tweets", produces = "application/json")
	@ApiOperation(value = "Find Tweets by debate id", notes = "Get tweets by its debate id.")
	@JsonView(Debate.Views.Tweets.class)
	public Debate getTweetsByDebateId(
			@ApiParam(value = "id of the debate", required = true) @PathVariable long debateId) {
		try {
			return debateService.findById(debateId);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - all
	@GetMapping(value = "/tweets", produces = "application/json")
	@ApiOperation(value = "Find all tweets", notes = "Find the list of all tweets.")
	@JsonView(Tweet.Views.Light.class)
	public List<Tweet> findAllTweets() {
		return tweetService.findAll();
	}
}
