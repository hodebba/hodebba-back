package com.hodebba.back.debate.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.models.Tag;
import com.hodebba.back.debate.services.TagService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/debates")
public class TagApi {

	private static final Logger logger = LoggerFactory.getLogger(TagApi.class);

	@Autowired
	private final TagService tagService;

	// ============== GET TAG ===============
	// - by id
	@GetMapping(value = "/tags/{id}", produces = "application/json")
	@ApiOperation(value = "Find Tag by id", notes = "Find a Tag by its id.")
	@JsonView(Tag.Views.Full.class)
	public Tag getTagById(@ApiParam(value = "id of the Tag to find", required = true) @PathVariable long id) {
		try {
			return tagService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - all
	@GetMapping(value = "/tags", produces = "application/json")
	@ApiOperation(value = "Find all Tags", notes = "Find the list of all Tag.")
	@JsonView(Tag.Views.Full.class)
	public List<Tag> findAllTag() {
		return tagService.findAll();
	}

	// ============== DELETE TAG ==============
	@DeleteMapping(value = "/tags/{id}", produces = "application/json")
	@ApiOperation(value = "Delete Tag", notes = "Delete an existing Tag")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void deleteTag(@ApiParam(value = "id of the Tag to delete", required = true) @PathVariable long id) {
		try {
			tagService.delete(id);
			logger.info("The Tag with the id : {} has been deleted.", id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}
}
