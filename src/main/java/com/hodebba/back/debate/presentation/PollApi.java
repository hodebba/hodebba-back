package com.hodebba.back.debate.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.dto.PollVoteDto;
import com.hodebba.back.debate.models.Poll;
import com.hodebba.back.debate.models.PollVote;
import com.hodebba.back.debate.services.PollService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/debates")
public class PollApi {

	private static final Logger logger = LoggerFactory.getLogger(PollApi.class);

	@Autowired
	private final PollService pollService;

	// ============= GET POLL ===============
	// - by id
	@GetMapping(value = "/poll/{id}", produces = "application/json")
	@ApiOperation(value = "Find Poll by id", notes = "Find a Poll by its id.")
	@JsonView(Poll.Views.Full.class)
	public Poll getPollById(@ApiParam(value = "id of the Poll to find", required = true) @PathVariable long id) {
		try {
			return pollService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	@GetMapping(value = "/user/{id}/poll", produces = "application/json")
	@ApiOperation(value = "Find user votes", notes = "Find user votes.")
	@JsonView(PollVote.Views.ForUserVotes.class)
	public List<PollVote> getVotesByUser(
			@ApiParam(value = "id of the user for which we want to find the votes", required = true) @PathVariable long id,
			final Locale locale, Principal principal) {
		try {
			return pollService.getPollByUser(locale, principal, id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============ VOTE POLL ==============
	@PostMapping(value = "/poll", produces = "application/json")
	@ApiOperation(value = "Poll Vote", notes = "Add or edit a vote in a poll.")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void addPollVote(
			@ApiParam(value = "PollVoteDto model", required = true) @RequestBody @Valid PollVoteDto pollVoteDto,
			final Locale locale, Principal principal) {
		try {
			PollVote pollVote = pollService.addVote(pollVoteDto, locale, principal);
			logger.info("A new vote has been created : {}", pollVote);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}
}
