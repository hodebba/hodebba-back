package com.hodebba.back.debate.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.dto.ThemeDto;
import com.hodebba.back.debate.dto.ThemeUpdateDto;
import com.hodebba.back.debate.error.ThemeStillReferencedException;
import com.hodebba.back.debate.models.Theme;
import com.hodebba.back.debate.services.impl.ThemeServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/debates")
public class ThemeApi {

	private static final Logger logger = LoggerFactory.getLogger(ThemeApi.class);

	@Autowired
	private final ThemeServiceImpl themeService;

	// ============== GET THEME =============
	// - by id
	@GetMapping(value = "/themes/{id}", produces = "application/json")
	@ApiOperation(value = "Find Theme by id", notes = "Find a Theme by its id.")
	@JsonView(Theme.Views.Full.class)
	public Theme getThemeById(@ApiParam(value = "id of the Theme to find", required = true) @PathVariable long id) {
		try {
			return themeService.findById(id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// - all
	@GetMapping(value = "/themes", produces = "application/json")
	@ApiOperation(value = "Find all Themes", notes = "Find the list of all Theme.")
	@JsonView(Theme.Views.Light.class)
	public List<Theme> findAllTheme() {
		return themeService.findAll();
	}

	// ============== CREATE THEME ============
	@PostMapping(value = "/themes", produces = "application/json")
	@ApiOperation(value = "Create Theme", notes = "Create a new Theme")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Theme.Views.Full.class)
	public Theme createTheme(
			@ApiParam(value = "ThemeDto model", required = true) @RequestBody @Valid ThemeDto themeDto) {
		Theme theme = themeService.create(themeDto.getLabel());
		logger.info("A new Theme has been created : {}.", theme);
		return theme;
	}

	// ============== UPDATE THEME ============
	@PutMapping(value = "/themes", produces = "application/json")
	@ApiOperation(value = "Update Theme", notes = "Update an existing Theme")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@JsonView(Theme.Views.Full.class)
	public Theme updateTheme(
			@ApiParam(value = "ThemeUpdateDto model", required = true) @RequestBody @Valid ThemeUpdateDto themeUpdateDto) {
		try {
			Theme theme = themeService.findById(themeUpdateDto.getId());
			theme.setLabel(themeUpdateDto.getLabel());
			Theme newTheme = themeService.update(theme);
			logger.info("A Theme has been updated : {}.", newTheme);
			return newTheme;
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	// ============== DELETE THEME ============
	@DeleteMapping(value = "/themes/{id}", produces = "application/json")
	@ApiOperation(value = "Delete Theme", notes = "Delete an existing Theme")
	@ApiImplicitParam(name = "Authorization", value = "Access Token of an Administrator", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public void deleteTheme(@ApiParam(value = "id of the Theme to delete", required = true) @PathVariable long id) {
		try {
			Theme theme = themeService.findById(id);
			themeService.delete(theme);
			logger.info("The Theme with the id : {}  has been deleted.", id);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		} catch (ThemeStillReferencedException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
		}
	}
}
