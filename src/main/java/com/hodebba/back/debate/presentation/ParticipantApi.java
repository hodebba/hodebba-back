package com.hodebba.back.debate.presentation;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.debate.dto.DebateUpdateDto;
import com.hodebba.back.debate.dto.ParticipantUpdateDto;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.DebateState;
import com.hodebba.back.debate.models.Participant;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.debate.services.ParticipantService;
import com.hodebba.back.user.models.User;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@CrossOrigin
public class ParticipantApi {

    private static final Logger logger = LoggerFactory.getLogger(ParticipantApi.class);

    @Autowired
    private final ParticipantService participantService;

    // ============== GET PARTICIPANTS ============
    // - by user
    @GetMapping(value = "/participants/users", produces = "application/json")
    @ApiOperation(value = "Find all private debates in which a user is participating.", notes = "Find all private debates in which a user is participating.")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @JsonView(Participant.Views.Full.class)
    public List<Participant> findAllParticipantsByUser(Principal principal)  {
        try {
            return participantService.findAllParticipantsByUser(principal.getName());
        }catch (NoSuchElementException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    // ============== UPDATE ROLE ============
    @PutMapping(value = "/participants", produces = "application/json")
    @ApiOperation(value = "Update participant", notes = "Update role of a participant.")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @JsonView(Debate.Views.Light.class)
    public Participant updateParticipant(
            @ApiParam(value = "ParticipantUpdateDto model", required = true) @RequestBody @Valid ParticipantUpdateDto participantUpdateDto, Principal principal) {
        try {
            String emailAdmin = principal.getName();
            Participant participant = participantService.update(participantUpdateDto, emailAdmin);
            logger.info("A participant has been updated : {}.", participant);
            return participant;
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    // ============== DELETE USER ============
    @DeleteMapping(value = "/participants/{id}", produces = "application/json")
    @ApiOperation(value = "Delete Participant", notes = "Delete a Participant.")
    @ApiImplicitParam(name = "Authorization", value = "Access Token of the user or of an admin", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public void delete(@ApiParam(value = "id of the Participant to delete", required = true) @PathVariable long id,
                       final Locale locale, Principal principal) {
        String emailAdmin = principal.getName();
        try{
            Participant participant = participantService.delete(id, emailAdmin);
            logger.info("A participant has been deleted : {}.", participant);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
