package com.hodebba.back.debate.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "poll_vote_type")
@Getter
public class PollVoteType {

	@Id
	@SequenceGenerator(name = "poll_vote_type_id_generator", sequenceName = "poll_vote_type_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "poll_vote_type_id_generator")
	@JsonView(PollVoteType.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(PollVoteType.Views.LABEL.class)
	private String label;

	@JsonView(PollVoteType.Views.COUNT.class)
	private Long count;

	@Setter
	@ManyToOne(targetEntity = Poll.class)
	@JoinColumn(name = "poll_id", referencedColumnName = "id", nullable = false)
	@JsonView(PollVoteType.Views.POLL.class)
	private Poll poll;

	@Override
	public String toString() {
		return "PollVoteType{" + "id=" + id + ", label='" + label + '\'' + ", count=" + count + ", poll=" + poll.getId()
				+ '}';
	}

	public static class Views {
		private interface ID {
		}
		private interface LABEL {
		}
		private interface COUNT {
		}
		private interface POLL {
		}

		public interface Id extends ID {
		}
		public interface Light extends ID, LABEL, COUNT {
		}
		public interface Full extends ID, LABEL, COUNT, POLL, Poll.Views.Light {
		}
		public interface ForPoll extends ID, LABEL {
		}
		public interface Count extends COUNT {
		}
	}

	public PollVoteType() {
		count = 0L;
	}

	public PollVoteType(String label, Poll poll) {
		this.label = label;
		this.poll = poll;
		this.count = 0L;
	}

	public Long incCount() {
		this.count += 1;
		return this.count;
	}

	public Long decCount() {
		this.count -= 1;
		return this.count;
	}
}
