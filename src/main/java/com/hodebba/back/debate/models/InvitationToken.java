package com.hodebba.back.debate.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity(name = "invitation_token")
@Getter
public class InvitationToken {

    private static final int EXPIRATION = 60 * 24; // 24 hours

    @Id
    @SequenceGenerator(name = "invitation_token_id_generator", sequenceName = "invitation_token_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invitation_token_id_generator")
    private Long id;

    @Setter
    private String token;

    @Setter
    @Enumerated(EnumType.STRING)
    private DebateRole role;

    @Setter
    @OneToOne(targetEntity = Debate.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "debate_id")
    private Debate debate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;

    public InvitationToken() {
        super();
    }

    public InvitationToken(final String token) {
        super();

        this.token = token;
        this.expiryDate = calculateExpiryDate(EXPIRATION);
    }

    public InvitationToken(final String token, final Debate debate, DebateRole role) {
        super();

        this.token = token;
        this.debate = debate;
        this.role = role;
        this.expiryDate = calculateExpiryDate(EXPIRATION);
    }

    private Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(new Date().getTime());
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }

    public void updateToken(final String token) {
        this.token = token;
        this.expiryDate = calculateExpiryDate(EXPIRATION);
    }

    @Override
    public String toString() {
        return "Token [String=" + token + "]" + "[Expires" + expiryDate + "]";
    }

}
