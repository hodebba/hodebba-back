package com.hodebba.back.debate.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity(name = "participant")
@Getter
public class Participant {

    @Id
    @SequenceGenerator(name = "participant_id_generator", sequenceName = "participant_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "participant_id_generator")
    @JsonView(Participant.Views.ID.class)
    private Long id;

    @Setter
    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    @JsonView(Participant.Views.USER.class)
    private User user;

    @Setter
    @ManyToOne(targetEntity = Debate.class)
    @JoinColumn(name = "debate_id", referencedColumnName = "id", nullable = false)
    @JsonView(Participant.Views.DEBATE.class)
    private Debate debate;

    @Setter
    @Enumerated(EnumType.STRING)
    @JsonView(Participant.Views.ROLE.class)
    private DebateRole role;

    public Participant() {
    }

    public Participant(User user, DebateRole role, Debate debate) {
        this.user = user;
        this.role = role;
        this.debate = debate;
    }


    public static class Views {
        private interface ID {
        }
        private interface USER {
        }
        private interface ROLE {
        }
        private interface DEBATE {
        }
        public interface Full extends ROLE, USER, ID, DEBATE, Debate.Views.Light {
        }
        public interface Light extends ROLE, USER, ID {
        }
    }
}
