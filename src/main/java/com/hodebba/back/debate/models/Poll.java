package com.hodebba.back.debate.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity(name = "poll")
@Getter
public class Poll {

	@Id
	@SequenceGenerator(name = "poll_id_generator", sequenceName = "poll_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "poll_id_generator")
	@JsonView(Poll.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(Poll.Views.QUESTION.class)
	private String question;

	@Setter
	@OneToOne(targetEntity = Debate.class)
	@JoinColumn(name = "debate_id", referencedColumnName = "id", unique = true, nullable = false)
	@JsonView(Poll.Views.DEBATES.class)
	private Debate debate;

	@Setter
	@OneToMany(targetEntity = PollVoteType.class, mappedBy = "poll", cascade = CascadeType.REMOVE, orphanRemoval = true)
	@JsonView(Poll.Views.POLLVOTETYPES.class)
	private Collection<PollVoteType> pollVoteTypes;

	@Setter
	@OneToMany(targetEntity = PollVote.class, mappedBy = "poll", cascade = CascadeType.REMOVE, orphanRemoval = true)
	@JsonView(Poll.Views.POLLVOTES.class)
	private Collection<PollVote> pollVotes;

	@Override
	public String toString() {
		return "Poll{" + "id=" + id + ", question='" + question + '\'' + ", debate=" + debate.getId()
				+ ", pollVoteTypes=" + pollVoteTypes.toString() + ", pollVotes=" + pollVotes.toString() + '}';
	}

	public static class Views {
		private interface ID {
		}
		private interface QUESTION {
		}
		private interface DEBATES {
		}
		private interface POLLVOTETYPES {
		}
		private interface POLLVOTES {
		}

		public interface Id extends ID {
		}
		public interface Light extends ID, QUESTION {
		}
		public interface Full
				extends
					Light,
					DEBATES,
					Debate.Views.Light,
					POLLVOTES,
					PollVote.Views.ForPoll,
					POLLVOTETYPES,
					PollVoteType.Views.Light {
		}
		public interface ForDebate extends ID, QUESTION, POLLVOTETYPES, PollVoteType.Views.Light {
		}
		public interface ForUserVotes extends ID, POLLVOTETYPES, PollVoteType.Views.Id {
		}
		public interface ForSearch extends ID, POLLVOTETYPES, PollVoteType.Views.Count {
		}
	}

	public Poll() {
	}

	public Poll(String question, Debate debate) {
		this.question = question;
		this.debate = debate;
	}

}
