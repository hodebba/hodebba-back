package com.hodebba.back.debate.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@Entity(name = "theme")
@Getter
public class Theme {

	@Id
	@SequenceGenerator(name = "theme_id_generator", sequenceName = "theme_id_seq", initialValue = 50)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "theme_id_generator")
	@JsonView(Theme.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(Theme.Views.LABEL.class)
	private String label;

	@OneToMany(targetEntity = Debate.class, mappedBy = "theme")
	@JsonView(Theme.Views.DEBATES.class)
	private Collection<Debate> debates;

	public static class Views {
		private interface ID {
		}
		private interface LABEL {
		}
		private interface DEBATES {
		}

		public interface Id extends ID {
		}
		public interface Light extends ID, LABEL {
		}
		public interface Full extends Light, DEBATES, Debate.Views.ForTheme {
		}
	}

	public Theme() {
	}

	public Theme(String label) {
		this.label = label;
	}

	private String debatesToString() {
		List l = new ArrayList();
		if (debates != null && !debates.isEmpty()) {
			Iterator<Debate> it = debates.iterator();
			while (it.hasNext()) {
				l.add(it.next().getId());
			}
			return l.toString();
		} else {
			return null;
		}

	}

	public String toString() {
		return "Theme{" + "id=" + id + ", label=\"" + label + "\", debates=" + debatesToString() + "}";
	}

}
