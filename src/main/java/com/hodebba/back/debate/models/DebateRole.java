package com.hodebba.back.debate.models;

public enum DebateRole {
    viewer, writer, admin
}
