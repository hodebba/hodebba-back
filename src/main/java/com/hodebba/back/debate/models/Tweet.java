package com.hodebba.back.debate.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity(name = "tweet")
@Getter
public class Tweet {

	@Id
	@SequenceGenerator(name = "tweet_id_generator", sequenceName = "tweet_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tweet_id_generator")
	@JsonView(Tweet.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(Tweet.Views.LINK.class)
	private String link;

	@ManyToMany(targetEntity = Debate.class, mappedBy = "tweets")
	@JsonView(Tweet.Views.DEBATES.class)
	private Collection<Debate> debates;

	public Tweet() {
	}

	public Tweet(String link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "Tweet{" + "id=" + id + ", link='" + link + '\'' + '}';
	}

	public static class Views {
		private interface ID {
		};
		private interface LINK {
		};
		private interface DEBATES {
		};

		public interface Light extends ID, LINK {
		};
	}

}
