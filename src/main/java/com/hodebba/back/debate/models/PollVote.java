package com.hodebba.back.debate.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "poll_vote")
@Getter
public class PollVote {

	@Id
	@SequenceGenerator(name = "poll_vote_id_generator", sequenceName = "poll_vote_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "poll_vote_id_generator")
	@JsonView(PollVote.Views.ID.class)
	private Long id;

	@Setter
	@ManyToOne(targetEntity = Poll.class)
	@JoinColumn(name = "poll_id", referencedColumnName = "id", nullable = false)
	@JsonView(PollVote.Views.POLL.class)
	private Poll poll;

	@Setter
	@ManyToOne(targetEntity = PollVoteType.class, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "poll_vote_type_id", referencedColumnName = "id", nullable = false)
	@JsonView(PollVote.Views.POLLVOTETYPE.class)
	private PollVoteType pollVoteType;

	@Setter
	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	@JsonView(PollVote.Views.USER.class)
	private User user;

	public static class Views {
		private interface ID {
		}
		private interface POLL {
		}
		private interface POLLVOTETYPE {
		}
		private interface USER {
		}

		public interface Id extends ID {
		}
		public interface Full
				extends
					ID,
					POLL,
					Poll.Views.Light,
					POLLVOTETYPE,
					PollVoteType.Views.Light,
					USER,
					User.Views.LIGHT {
		}
		public interface ForPoll extends ID, POLLVOTETYPE, PollVoteType.Views.ForPoll, USER, User.Views.LIGHT {
		}

		public interface ForUserVotes extends POLLVOTETYPE, PollVoteType.Views.Id, POLL, Poll.Views.ForUserVotes {
		}
	}

	public PollVote() {
	}

	public PollVote(Poll poll, PollVoteType pollVoteType, User user) {
		this.poll = poll;
		this.pollVoteType = pollVoteType;
		this.user = user;
	}

	public String toString() {
		return "PollVote{" + "id:" + id + ", poll:" + poll.getId() + ", pollVoteType:\"" + pollVoteType.getLabel()
				+ "\", user:" + user.getId() + "}";
	}
}
