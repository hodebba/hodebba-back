package com.hodebba.back.debate.models;

public enum DebateState {
	draft, published, archived
}
