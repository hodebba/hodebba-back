package com.hodebba.back.debate.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity(name = "tag")
@Getter
public class Tag {

	@Id
	@SequenceGenerator(name = "tag_id_generator", sequenceName = "tag_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tag_id_generator")
	@JsonView(Tag.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(Tag.Views.LABEL.class)
	private String label;

	@ManyToMany(targetEntity = Debate.class, mappedBy = "tags")
	@JsonView(Tag.Views.DEBATES.class)
	private Collection<Debate> debates;

	@Override
	public String toString() {
		return "Tag{" + "id=" + id + ", label='" + label + '\'' + '}';
	}

	public static class Views {
		private interface ID {
		}
		private interface LABEL {
		}
		private interface DEBATES {
		}

		public interface Id extends ID {
		}
		public interface Light extends ID, LABEL {
		}
		public interface Full extends Light, DEBATES, Debate.Views.Light {
		}
	}

	public Tag() {
	}

	public Tag(String label) {
		this.label = label;
	}

}
