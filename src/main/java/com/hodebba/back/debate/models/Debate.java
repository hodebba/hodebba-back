package com.hodebba.back.debate.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.user.models.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.*;

@Entity(name = "debate")
@Getter
public class Debate {

	@Id
	@SequenceGenerator(name = "debate_id_generator", sequenceName = "debate_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "debate_id_generator")
	@JsonView(Debate.Views.ID.class)
	private Long id;

	@Setter
	@JsonView(Debate.Views.TITLE.class)
	private String title;

	@Setter
	@JsonView(Debate.Views.IMAGELINK.class)
	private String imageLink;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonView(Debate.Views.CREATEDDATE.class)
	private Date createdDate;

	@JsonView(Debate.Views.VIEWNUMBER.class)
	private Long viewNumber;

	@Setter
	@Lob
	@Type(type = "text")
	@JsonView(Debate.Views.PRESENTATION.class)
	private String presentation;

	@Setter
	@JsonView(Debate.Views.ISPRIVATE.class)
	private Boolean isPrivate = false;

	@Setter
	@JsonView(Debate.Views.CREATED_BY_COMMUNITY.class)
	private Boolean createdByCommunity = false;

	@Setter
	@JsonView(Debate.Views.PARTICIPANTS.class)
	@OneToMany(targetEntity = Participant.class, mappedBy = "debate", cascade = CascadeType.REMOVE)
	private Collection<Participant> participants;

	@Setter
	@Enumerated(EnumType.STRING)
	@JsonView(Debate.Views.DEBATESTATE.class)
	private DebateState debateState;

	@Setter
	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	@JsonView(Debate.Views.USER.class)
	private User user;

	@Setter
	@ManyToOne(targetEntity = Theme.class)
	@JoinColumn(name = "theme_id", referencedColumnName = "id")
	@JsonView(Debate.Views.THEME.class)
	private Theme theme;

	@Setter
	@ManyToMany
	@JoinTable(name = "debate_tag", joinColumns = @JoinColumn(name = "debate_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
	@JsonView(Debate.Views.TAG.class)
	private Collection<Tag> tags;

	@Setter
	@OneToOne(targetEntity = Poll.class, mappedBy = "debate", cascade = CascadeType.REMOVE)
	@JsonView(Debate.Views.POLL.class)
	private Poll poll;

	@Setter
	@ManyToMany(cascade = CascadeType.REMOVE)
	@JoinTable(name = "debate_tweet", joinColumns = @JoinColumn(name = "debate_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "tweet_id", referencedColumnName = "id"))
	@JsonView(Debate.Views.TWEETS.class)
	private Collection<Tweet> tweets;

	@Setter
	@ManyToMany(cascade = CascadeType.REMOVE)
	@JoinTable(name = "debate_statement", joinColumns = @JoinColumn(name = "debate_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "statement_id", referencedColumnName = "id"))
	@JsonView(Debate.Views.STATEMENTS.class)
	private Collection<Statement> statements;

	@Override
	public String toString() {
		return "Debate{" + "id=" + id + ", title='" + title + '\'' + ", imageLink='" + imageLink + '\''
				+ ", createdDate=" + createdDate + ", viewNumber=" + viewNumber + ", presentation='" + presentation
				+ '\'' + ", debateState=" + debateState + ", theme=" + theme.getId() + ", tags=" + tagListToString()
				+ ", tweets=" + tweetListToString() + ",user=" + user.getId() + ", poll=" + poll.getId() + '}';
	}

	public static class Views {
		private interface ID {
		}
		private interface TITLE {
		}
		private interface IMAGELINK {
		}
		private interface CREATEDDATE {
		}
		private interface VIEWNUMBER {
		}
		private interface PRESENTATION {
		}
		private interface DEBATESTATE {
		}
		private interface USER {
		}
		private interface THEME {
		}
		private interface TAG {
		}
		private interface POLL {
		}
		private interface ARGUMENTS {
		}
		private interface TWEETS {
		}
		private interface STATEMENTS {
		}
		private interface ISPRIVATE {
		}
		private interface PARTICIPANTS {
		}
		private interface CREATED_BY_COMMUNITY{

		}

		public interface Id extends ID {
		}
		public interface Light
				extends
					ID,
					TITLE,
				    ISPRIVATE,
					IMAGELINK,
					CREATEDDATE,
					DEBATESTATE,
					THEME,
					Theme.Views.Light,
					USER,
					User.Views.LIGHT {
		}
		public interface Full
				extends
					Light,
					VIEWNUMBER,
				    PARTICIPANTS,
					PRESENTATION,
					POLL,
					ARGUMENTS,
				    CREATED_BY_COMMUNITY,
				    ISPRIVATE,
					Poll.Views.ForDebate,
					Participant.Views.Light,
					TAG,
					Tag.Views.Light {
		}
		public interface ForTheme
				extends
					ID,
					TITLE,
				    ISPRIVATE,
					IMAGELINK,
					CREATEDDATE,
					DEBATESTATE,
					USER,
					User.Views.LIGHT,
					VIEWNUMBER {
		}

		public interface ForSearch
				extends
					ID,
					TITLE,
					IMAGELINK,
					CREATEDDATE,
					DEBATESTATE,
					USER,
					User.Views.LIGHT,
					THEME,
					Theme.Views.Light,
					PRESENTATION,
					POLL,
					Poll.Views.ForSearch,
					VIEWNUMBER {
		}
		public interface Tweets extends TWEETS, Full, Tweet.Views.Light {
		}
		public interface Title extends ID, TITLE {
		}
		public interface DebatePreview extends ID, TITLE, IMAGELINK, DEBATESTATE, THEME,Theme.Views.Light, PARTICIPANTS,Participant.Views.Light{
		}
	}

	public Debate() {
		this.createdDate = new Date();
		this.viewNumber = Long.valueOf(0);
		this.debateState = DebateState.draft;
	}

	public Debate(String title, User user) {
		this.title = title;
		this.user = user;
		this.createdDate = new Date();
		this.viewNumber = Long.valueOf(0);
		this.debateState = DebateState.draft;
	}

	public void incViewNumber() {
		this.viewNumber = this.viewNumber + 1;
	}

	private String tagListToString() {
		List l = new ArrayList<>();
		if (tags != null && !tags.isEmpty()) {
			Iterator<Tag> it = tags.iterator();
			while (it.hasNext()) {
				l.add(it.next().getLabel());
			}
			return l.toString();
		} else {
			return null;
		}
	}

	private String tweetListToString() {
		List l = new ArrayList<>();
		if (tweets != null && !tweets.isEmpty()) {
			Iterator<Tweet> it = tweets.iterator();
			while (it.hasNext()) {
				l.add(it.next().getLink());
			}
			return l.toString();
		} else {
			return null;
		}
	}

}
