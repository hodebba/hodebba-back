package com.hodebba.back.debate.services;

import com.hodebba.back.debate.models.Tag;

import java.util.List;

public interface TagService {

	List<Tag> findAll();

	Tag findById(long id);

	Tag findByLabel(String label);

	Tag create(String label);

	Tag update(long id, String label);

	void delete(long id);
}
