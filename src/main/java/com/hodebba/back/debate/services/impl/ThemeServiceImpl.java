package com.hodebba.back.debate.services.impl;

import com.hodebba.back.debate.error.ThemeStillReferencedException;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.Theme;
import com.hodebba.back.debate.persistence.ThemeDao;
import com.hodebba.back.debate.services.ThemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ThemeServiceImpl implements ThemeService {

	@Autowired
	private ThemeDao themeDao;

	@Override
	public List<Theme> findAll() {
		return themeDao.findAll();
	}

	@Override
	public Theme findById(long id) {
		Optional<Theme> optTheme = themeDao.findById(id);
		if (optTheme.isPresent()) {
			return optTheme.get();
		} else {
			throw new NoSuchElementException("No theme with this id:" + id);
		}
	}

	@Override
	public Theme findByLabel(String label) {
		Optional<Theme> optTheme = themeDao.findByLabel(label);
		if (optTheme.isPresent()) {
			return optTheme.get();
		} else {
			throw new NoSuchElementException("No theme with this label:" + label);
		}
	}

	@Override
	@Transactional
	public Theme create(String label) {
		Optional<Theme> optTheme = themeDao.findByLabel(label);
		if (optTheme.isPresent()) {
			return optTheme.get();
		} else {
			Theme theme = new Theme(label);
			return themeDao.save(theme);
		}
	}

	@Override
	@Transactional
	public Theme update(Theme theme) {
		Optional<Theme> optTheme = themeDao.findById(theme.getId());
		if (optTheme.isPresent()) {
			Theme oldTheme = optTheme.get();
			oldTheme.setLabel(theme.getLabel());
			return themeDao.save(oldTheme);
		} else {
			throw new NoSuchElementException("No theme with this id:" + theme.getId());
		}
	}

	@Override
	@Transactional
	public void delete(Theme theme) throws ThemeStillReferencedException {
		Collection<Debate> debateCollection = theme.getDebates();
		if (debateCollection != null && !debateCollection.isEmpty()) {
			throw new ThemeStillReferencedException("This theme is still referenced in existing debates.");
		}
		themeDao.delete(theme);
	}

}
