package com.hodebba.back.debate.services.impl;

import com.hodebba.back.debate.dto.ParticipantUpdateDto;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.DebateRole;
import com.hodebba.back.debate.models.Participant;
import com.hodebba.back.debate.persistence.ParticipantDao;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.debate.services.ParticipantService;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import org.apache.http.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ParticipantServiceImpl implements ParticipantService {

    @Autowired
    private ParticipantDao participantDao;

    @Lazy
    @Autowired
    private UserService userService;

    @Autowired
    private DebateService debateService;


    public Participant create(User user, DebateRole debateRole, Debate debate){
        if (debate.getParticipants() != null && debate.getParticipants().stream().anyMatch(p -> p.getUser().getId() == user.getId())
        ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return participantDao.save(new Participant(user, debateRole, debate));
    }

    private Participant findById(long id) {
        Optional<Participant> optParticipant = participantDao.findById(id);
        if (optParticipant.isPresent()) {
            return optParticipant.get();
        } else {
            throw new NoSuchElementException("No participant with this id:" + id);
        }
    }

    @Override
    public Participant findByUserAndDebate(User user, Debate debate) {
        Optional<Participant> optParticipant= participantDao.findByUserAndDebate(user, debate);
        if (optParticipant.isPresent()) {
            return optParticipant.get();
        } else {
            throw new NoSuchElementException("No participant with this debate and user:" + user.getId());
        }
    }

    @Override
    @Transactional
    public Participant update(ParticipantUpdateDto participantUpdateDto, String emailAdmin){
        Debate debate = debateService.findById(participantUpdateDto.getDebateId());
        User loggedAdmin = userService.findByEmail(emailAdmin);
        Participant adminParticipant = findByUserAndDebate(loggedAdmin, debate);
        Participant userParticipant = findById(participantUpdateDto.getId());

        if(adminParticipant.getRole().equals(DebateRole.admin) && !userParticipant.equals(adminParticipant)) {
            userParticipant.setRole(participantUpdateDto.getRole());
            return  participantDao.save(userParticipant);
        }else{
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @Transactional
    public void update(Participant participant){
        participantDao.save(participant);
    }


    @Override
    public List<Participant> findAllParticipantsByUser(String userEmail){
        User user = userService.findByEmail(userEmail);
        return participantDao.findByUser(user);
    }

    @Override
    @Transactional
    public Participant delete (Long id, String emailAdmin){
        User loggedAdmin = userService.findByEmail(emailAdmin);
        Participant userParticipant = findById(id);
        Participant adminParticipant = findByUserAndDebate(loggedAdmin, userParticipant.getDebate());

        if(adminParticipant.getRole().equals(DebateRole.admin) && !userParticipant.getRole().equals(DebateRole.admin) ){
            participantDao.delete(userParticipant);
            return userParticipant;
        }else{
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }
}
