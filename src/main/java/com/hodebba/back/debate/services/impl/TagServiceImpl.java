package com.hodebba.back.debate.services.impl;

import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.Tag;
import com.hodebba.back.debate.persistence.TagDao;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.debate.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class TagServiceImpl implements TagService {

	@Autowired
	private TagDao tagDao;

	@Autowired
	DebateService debateService;

	@Override
	public List<Tag> findAll() {
		return tagDao.findAll();
	}

	@Override
	public Tag findById(long id) {
		Optional<Tag> optTag = tagDao.findById(id);
		if (optTag.isPresent()) {
			return optTag.get();
		} else {
			throw new NoSuchElementException("No tag with this id:" + id);
		}
	}

	@Override
	public Tag findByLabel(String label) {
		Optional<Tag> optTag = tagDao.findByLabel(label);
		if (optTag.isPresent()) {
			return optTag.get();
		} else {
			throw new NoSuchElementException("No tag with this label:" + label);
		}
	}

	@Override
	@Transactional
	public Tag create(String label) {
		Optional<Tag> optTag = tagDao.findByLabel(label);
		if (optTag.isPresent()) {
			return optTag.get();
		} else {
			Tag tag = new Tag(label);
			return tagDao.save(tag);
		}
	}

	@Override
	@Transactional
	public Tag update(long id, String label) {
		Optional<Tag> optTag = tagDao.findById(id);
		if (optTag.isPresent()) {
			Tag tag = optTag.get();
			tag.setLabel(label);
			return tagDao.save(tag);
		} else {
			throw new NoSuchElementException("No tag with this id:" + id);
		}
	}

	@Override
	@Transactional
	public void delete(long id) {
		Optional<Tag> optTag = tagDao.findById(id);
		if (optTag.isPresent()) {
			Tag tag = optTag.get();
			deleteTagFromDebates(tag.getDebates(), tag);
			tagDao.delete(tag);
		} else {
			throw new NoSuchElementException("No tag with this id:" + id);
		}
	}

	private void deleteTagFromDebates(Collection<Debate> debateList, Tag tag) {
		if (debateList != null && !debateList.isEmpty()) {
			Iterator<Debate> it = debateList.iterator();
			while (it.hasNext()) {
				Debate debate = it.next();
				debateService.deleteTag(debate, tag);
			}
		}
	}

}
