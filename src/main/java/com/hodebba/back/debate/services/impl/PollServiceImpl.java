package com.hodebba.back.debate.services.impl;

import com.hodebba.back.debate.dto.PollDto;
import com.hodebba.back.debate.dto.PollVoteDto;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.Poll;
import com.hodebba.back.debate.models.PollVote;
import com.hodebba.back.debate.models.PollVoteType;
import com.hodebba.back.debate.persistence.PollDao;
import com.hodebba.back.debate.persistence.PollVoteDao;
import com.hodebba.back.debate.persistence.PollVoteTypeDao;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.debate.services.PollService;
import com.hodebba.back.security.utils.TokenChecker;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.*;

@Service
public class PollServiceImpl implements PollService {

	@Autowired
	private PollDao pollDao;

	@Autowired
	private PollVoteDao pollVoteDao;

	@Autowired
	private PollVoteTypeDao pollVoteTypeDao;

	@Autowired
	private DebateService debateService;

	@Lazy
	@Autowired
	private UserService userService;

	@Override
	public Poll findById(long id) {
		Optional<Poll> optPoll = pollDao.findById(id);
		if (optPoll.isPresent()) {
			return optPoll.get();
		} else {
			throw new NoSuchElementException("No Poll with this id:" + id);
		}
	}

	@Override
	@Transactional
	public Poll create(PollDto pollDto, Debate debate) {
		debate = debateService.findById(debate.getId());
		Poll poll = new Poll(pollDto.getQuestion(), debate);
		poll = pollDao.save(poll);

		List<PollVoteType> pollVoteTypeList = new ArrayList<>();
		Iterator<String> it = pollDto.getPollOptions().iterator();
		while (it.hasNext()) {
			String type = it.next();
			PollVoteType pollVoteType = new PollVoteType(type, poll);
			pollVoteType = pollVoteTypeDao.save(pollVoteType);
			pollVoteTypeList.add(pollVoteType);
		}
		poll.setPollVoteTypes(pollVoteTypeList);
		return poll;
	}

	@Override
	@Transactional
	public PollVote addVote(PollVoteDto pollVoteDto, Locale locale, Principal principal) {
		User user = userService.findById(pollVoteDto.getUserId());
		TokenChecker.checkUserToken(locale, principal, user);
		Poll poll = findById(pollVoteDto.getPollId());

		Optional<PollVoteType> optPollVoteType = pollVoteTypeDao.findById(pollVoteDto.getPollOptionId());
		if (optPollVoteType.isPresent()) {
			PollVoteType pollVoteType = optPollVoteType.get();
			if (poll.getPollVoteTypes().contains(pollVoteType)) {

				List<PollVote> votes = pollVoteDao.findByUserAndPoll(user, poll);
				PollVote vote;
				if (votes.isEmpty()) {
					vote = new PollVote(poll, pollVoteType, user);
				} else {
					vote = votes.get(0);
					PollVoteType oldVoteType = vote.getPollVoteType();
					oldVoteType.decCount();
					pollVoteTypeDao.save(oldVoteType);
					vote.setPollVoteType(pollVoteType);
				}
				pollVoteType.incCount();
				pollVoteTypeDao.save(pollVoteType);
				return pollVoteDao.save(vote);
			} else {
				throw new NoSuchElementException(
						"This Poll option doesn't exist for the poll with the id:" + pollVoteDto.getPollId());
			}
		} else {
			throw new NoSuchElementException("No Poll Option with this id:" + pollVoteDto.getPollOptionId());
		}
	}

	@Override
	public List<PollVote> getPollByUser(Locale locale, Principal principal, Long userId) {
		User user = userService.findById(userId);
		TokenChecker.checkUserToken(locale, principal, user);
		return pollVoteDao.findByUser(user);
	}

	@Override
	@Transactional
	public void delete(long id) {
		Poll poll = findById(id);
		pollVoteDao.deleteAllByPoll(poll);
		pollDao.deleteById(id);
	}

	@Override
	@Transactional
	public void delete(Poll poll) {
		pollVoteDao.deleteAllByPoll(poll);
		pollDao.delete(poll);
	}
}
