package com.hodebba.back.debate.services;

import com.hodebba.back.debate.models.Debate;

public interface ElasticSearchService {
	void createDebatesIndex();
	void sendDebateToES(Debate debate);
	boolean checkIndexExists(String index);
	void updateDebateES(Debate debate);
	void sendAllDebatesToEs();
}
