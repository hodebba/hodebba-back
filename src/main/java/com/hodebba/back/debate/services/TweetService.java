package com.hodebba.back.debate.services;

import com.hodebba.back.debate.models.Tweet;

import java.util.Collection;
import java.util.List;

public interface TweetService {
	Tweet findByLink(String link);
	Tweet create(String link);
	void delete(long id);
	Tweet findById(long id);
	List<Tweet> findAll();
}
