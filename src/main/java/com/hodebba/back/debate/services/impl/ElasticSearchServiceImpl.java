package com.hodebba.back.debate.services.impl;

import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.presentation.DebateApi;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.debate.services.ElasticSearchService;
//import com.hodebba.back.post.models.Argument;
import com.hodebba.back.post.services.ArgumentService;
import lombok.SneakyThrows;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ElasticSearchServiceImpl implements ElasticSearchService {

	@Autowired
	private RestHighLevelClient client;

	// @Autowired
	// private ArgumentService argumentService;

	@Autowired
	private DebateService debateService;

	private static final Logger logger = LoggerFactory.getLogger(DebateApi.class);

	@SneakyThrows
	public boolean checkIndexExists(String index) {
		GetIndexRequest request = new GetIndexRequest(index);
		return client.indices().exists(request, RequestOptions.DEFAULT);
	}

	@SneakyThrows
	private XContentBuilder configureSettings() {
		XContentBuilder builder = XContentFactory.jsonBuilder();
		builder.startObject();
		{
			builder.startObject("analysis");
			{
				builder.startObject("analyzer");
				{
					builder.startObject("my_analyzer");
					{
						builder.field("type", "french");
						builder.array("stopwords", "_french_", "h2", "p");
					}
					builder.endObject();
				}
				builder.endObject();
			}
			builder.endObject();
		}
		builder.endObject();
		return builder;
	}

	@SneakyThrows
	public void createDebatesIndex() {
		CreateIndexRequest request = new CreateIndexRequest("debates");
		request.settings(configureSettings());

		Map<String, Object> text = new HashMap<>();
		text.put("type", "text");
		Map<String, Object> number = new HashMap<>();
		number.put("type", "long");
		Map<String, Object> date = new HashMap<>();
		date.put("type", "date");

		Map<String, Object> presentation = new HashMap<>();
		presentation.put("type", "text");
		presentation.put("analyzer", "my_analyzer");

		Map<String, Object> arguments = new HashMap<>();
		arguments.put("type", "text");
		arguments.put("analyzer", "my_analyzer");

		Map<String, Object> statements = new HashMap<>();
		statements.put("type", "text");
		statements.put("analyzer", "my_analyzer");

		Map<String, Object> properties = new HashMap<>();
		properties.put("createdDate", date);
		properties.put("title", text);
		properties.put("presentation", presentation);
		properties.put("imageLink", text);
		properties.put("tags", text);
		properties.put("viewNumber", number);
		properties.put("theme", text);
		properties.put("debateState", text);
		/// properties.put("statements", statements);
		// properties.put("arguments", arguments);
		Map<String, Object> mapping = new HashMap<>();
		mapping.put("properties", properties);
		request.mapping(mapping);
		CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
		logger.info("A new ES index {} has been created.", createIndexResponse);
	}

	@SneakyThrows
	public void sendDebateToES(Debate debate) {
		// Collection<Argument> arguments =
		// argumentService.findByDebate(debate.getId());
		Map<String, Object> jsonMap = new HashMap<>();
		jsonMap.put("title", debate.getTitle());
		jsonMap.put("presentation", debate.getPresentation());
		jsonMap.put("tags", debate.getTags().stream().map(d -> d.getLabel()).collect(Collectors.toList()));
		jsonMap.put("theme", debate.getTheme().getLabel());
		jsonMap.put("viewNumber", debate.getViewNumber());
		jsonMap.put("createdDate", new Date(debate.getCreatedDate().getTime()));
		jsonMap.put("imageLink", debate.getImageLink());
		jsonMap.put("debateState", debate.getDebateState());

		/*
		 * if (debate.getStatements() != null) { jsonMap.put("statements",
		 * debate.getStatements().stream().map(statement -> statement.getContent())
		 * .collect(Collectors.toList())); }
		 * 
		 * if (arguments != null) { jsonMap.put("arguments",
		 * arguments.stream().map(argument ->
		 * argument.getContent()).collect(Collectors.toList())); }
		 */

		IndexRequest indexRequest = new IndexRequest("debates").id(debate.getId().toString()).source(jsonMap);
		IndexResponse indexResponse = null;

		indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);

		logger.info("A new Debate has been sent to ES : {}.", indexResponse);
	}

	@SneakyThrows
	public void updateDebateES(Debate debate) {
		// Collection<Argument> arguments =
		// argumentService.findByDebate(debate.getId());

		Map<String, Object> jsonMap = new HashMap<>();
		jsonMap.put("title", debate.getTitle());
		jsonMap.put("presentation", debate.getPresentation());
		jsonMap.put("tags", debate.getTags().stream().map(d -> d.getLabel()).collect(Collectors.toList()));
		jsonMap.put("theme", debate.getTheme().getLabel());
		jsonMap.put("viewNumber", debate.getViewNumber());
		jsonMap.put("imageLink", debate.getImageLink());
		jsonMap.put("debateState", debate.getDebateState());

		/*
		 * if (debate.getStatements() != null) { jsonMap.put("statements",
		 * debate.getStatements().stream().map(statement -> statement.getContent())
		 * .collect(Collectors.toList())); }
		 * 
		 * if (arguments != null) { jsonMap.put("arguments",
		 * arguments.stream().map(argument ->
		 * argument.getContent()).collect(Collectors.toList())); }
		 */

		UpdateRequest request = new UpdateRequest("debates", debate.getId().toString()).doc(jsonMap);
		UpdateResponse updateResponse = client.update(request, RequestOptions.DEFAULT);
		logger.info("A Debate has been updated to ES : {}.", updateResponse);
	}

	@Override
	public void sendAllDebatesToEs() {
		List<Debate> debates = debateService.findAll();

		if (!checkIndexExists("debates")) {
			createDebatesIndex();
		}

		debates.forEach(debate -> {
			sendDebateToES(debate);
		});
	}

}
