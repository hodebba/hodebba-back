package com.hodebba.back.debate.services;

import com.hodebba.back.debate.error.ThemeStillReferencedException;
import com.hodebba.back.debate.models.Theme;

import java.util.List;

public interface ThemeService {

	public List<Theme> findAll();

	public Theme findById(long id);

	public Theme findByLabel(String label);

	public Theme create(String label);

	public Theme update(Theme theme);

	public void delete(Theme theme) throws ThemeStillReferencedException;

}
