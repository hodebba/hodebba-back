package com.hodebba.back.debate.services;

import com.hodebba.back.debate.dto.PollDto;
import com.hodebba.back.debate.dto.PollVoteDto;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.Poll;
import com.hodebba.back.debate.models.PollVote;
import com.hodebba.back.user.models.User;

import java.security.Principal;
import java.util.List;
import java.util.Locale;

import java.security.Principal;
import java.util.Locale;

public interface PollService {

	Poll findById(long id);

	Poll create(PollDto pollDto, Debate debate);

	PollVote addVote(PollVoteDto pollVoteDto, Locale locale, Principal principal);

	void delete(long id);

	void delete(Poll poll);

	List<PollVote> getPollByUser(Locale locale, Principal principal, Long userId);
}
