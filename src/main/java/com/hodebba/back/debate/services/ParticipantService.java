package com.hodebba.back.debate.services;

import com.hodebba.back.debate.dto.ParticipantUpdateDto;
import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.DebateRole;
import com.hodebba.back.debate.models.Participant;
import com.hodebba.back.user.models.User;

import java.util.List;

public interface ParticipantService {
    Participant create(User user, DebateRole debateRole, Debate debate);
    Participant findByUserAndDebate(User user, Debate debate);
    Participant update(ParticipantUpdateDto participantUpdateDto, String emailAdmin);
    void update(Participant participant);
    List<Participant> findAllParticipantsByUser(String userEmail);
    Participant delete (Long id, String emailAdmin);
}
