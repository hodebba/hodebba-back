package com.hodebba.back.debate.services.impl;

import com.hodebba.back.debate.models.Debate;
import com.hodebba.back.debate.models.Tweet;
import com.hodebba.back.debate.persistence.TweetDao;
import com.hodebba.back.debate.services.DebateService;
import com.hodebba.back.debate.services.TweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class TweetServiceImpl implements TweetService {

	@Autowired
	private TweetDao tweetDao;

	@Autowired
	private DebateService debateService;

	@Override
	@Transactional
	public Tweet create(String link) {
		Optional<Tweet> optTweet = tweetDao.findByLink(link);
		if (optTweet.isPresent()) {
			return optTweet.get();
		} else {
			Tweet tweet = new Tweet(link);
			return tweetDao.save(tweet);
		}
	}

	@Transactional
	public void delete(long id) {
		Optional<Tweet> optTweet = tweetDao.findById(id);
		if (optTweet.isPresent()) {
			Tweet tweet = optTweet.get();
			deleteTweetFromDebates(tweet.getDebates(), tweet);
			tweetDao.delete(tweet);
		} else {
			throw new NoSuchElementException("No tweet with this id:" + id);
		}
	}

	@Override
	public Tweet findByLink(String link) {
		Optional<Tweet> optTweet = tweetDao.findByLink(link);
		if (optTweet.isPresent()) {
			return optTweet.get();
		} else {
			throw new NoSuchElementException("No tweet with this link:" + link);
		}
	}

	@Override
	public Tweet findById(long id) {
		Optional<Tweet> optTweet = tweetDao.findById(id);
		if (optTweet.isPresent()) {
			return optTweet.get();
		} else {
			throw new NoSuchElementException("No tweet with this id:" + id);
		}
	}

	private void deleteTweetFromDebates(Collection<Debate> debateList, Tweet tweet) {
		if (debateList != null && !debateList.isEmpty()) {
			Iterator<Debate> it = debateList.iterator();
			while (it.hasNext()) {
				Debate debate = it.next();
				debateService.deleteTweet(debate, tweet);
			}
		}
	}

	@Override
	public List<Tweet> findAll() {
		return this.tweetDao.findAll();
	}

}
