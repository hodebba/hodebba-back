package com.hodebba.back.debate.services.impl;

import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.debate.dto.DebateDto;
import com.hodebba.back.debate.dto.DebateUpdateDto;
import com.hodebba.back.debate.dto.InvitationDto;
import com.hodebba.back.debate.models.*;
import com.hodebba.back.debate.persistence.DebateDao;
import com.hodebba.back.debate.persistence.InvitationTokenDao;
import com.hodebba.back.debate.services.*;
import com.hodebba.back.post.services.PostService;
import com.hodebba.back.security.utils.TokenChecker;
import com.hodebba.back.user.models.Mail;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.EmailSenderService;
import com.hodebba.back.user.services.UserService;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DebateServiceImpl implements DebateService {

	@Autowired
	private DebateDao debateDao;

	@Autowired
	private InvitationTokenDao invitationTokenDao;

	@Autowired
	private TagService tagService;

	@Lazy
	@Autowired
	private UserService userService;

	@Autowired
	private ThemeService themeService;

	@Autowired
	private PollService pollService;

	@Autowired
	private TweetService tweetService;

	@Autowired
	private PostService postService;

	@Autowired
	private ParticipantService participantService;

	@Autowired
	private ElasticSearchService elasticSearchService;

	@Autowired
	private EmailSenderService emailSenderService;

	@Autowired
	private Environment env;

	@Autowired
	private MessageSource messages;

	private static final Logger logger = LoggerFactory.getLogger(DebateServiceImpl.class);

	@SneakyThrows
	@Override
	@Transactional
	public Debate create(DebateDto debateDto, Locale locale, Principal principal) {
		User user = userService.findById(debateDto.getUserId());
		TokenChecker.checkUserToken(locale, principal, user);

		Debate debate = new Debate();
		debate.setUser(user);
		debate.setTitle(debateDto.getTitle());
		debate.setImageLink(debateDto.getImageLink());
		debate.setPresentation(debateDto.getPresentation());
		debate.setIsPrivate(debateDto.getIsPrivate());
		debate.setCreatedByCommunity(debateDto.getCreatedByCommunity());

		Theme theme = themeService.findById(debateDto.getThemeId());
		debate.setTheme(theme);

		if (debateDto.getTags() != null) {
			List<Tag> tags = new ArrayList<>();
			if (!debateDto.getTags().isEmpty()) {
				Iterator<String> it = debateDto.getTags().iterator();
				while (it.hasNext()) {
					String label = it.next();
					if (label != null) {
						Tag tag = tagService.create(label);
						if (!tags.contains(tag)) {
							tags.add(tag);
						}
					}
				}
			}
			debate.setTags(tags);
		}

		debate = debateDao.save(debate);

		if(debate.getIsPrivate() == true){
			List <Participant> participants = new ArrayList<>();
			participants.add(participantService.create(user, DebateRole.admin, debate));
			debate.setParticipants(participants);
			debate.setDebateState(DebateState.published);
		}

		Poll poll = pollService.create(debateDto.getPoll(), debate);
		debate.setPoll(poll);

		if (!elasticSearchService.checkIndexExists("debates")) {
			elasticSearchService.createDebatesIndex();
		}
		elasticSearchService.sendDebateToES(debate);

		Mail mail = new Mail();
		mail.setFrom(env.getProperty("support.email"));
		mail.setMailTo(env.getProperty("support.email"));
		mail.setSubject("Un nouveau débat a été créé !");
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("title", debate.getTitle());
		model.put("text", debate.getPresentation());
		model.put("username", user.getEmail());
		mail.setProps(model);

		emailSenderService.sendEmail(mail, "new-debate", locale);

		return debate;
	}

	@Override
	public List<Debate> findAll() {
		return this.debateDao.findAllByIsPrivateFalse();
	}

	@Override
	public List<Debate> findAllPrivate() {
		return this.debateDao.findAllByIsPrivateTrue();
	}

	@Override
	public Debate findById(long id) {
		Optional<Debate> optDebate = debateDao.findById(id);
		if (optDebate.isPresent()) {
			return optDebate.get();
		} else {
			throw new NoSuchElementException("No debate with this id:" + id);
		}
	}

	@Override
	public Debate findPrivateById(long id, String userEmail){
		Debate debate = findById(id);
		User user = userService.findByEmail(userEmail);
		try{
			participantService.findByUserAndDebate(user, debate);
		}catch (NoSuchElementException elementException){
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}
		return debate;
	}

	@Override
	public TreeMap<Theme, List<Debate>> findDebatesGroupByTheme() {
		final int[] compare = new int[1];
		String mostRecentThemeLabel = "Les plus récents";
		List<Theme> themes = themeService.findAll();
		Comparator<Theme> byDebates = (t1, t2) -> {
			if (t1.getLabel().equals(mostRecentThemeLabel))
				return 1;
			else if (t2.getLabel().equals(mostRecentThemeLabel))
				return -1;
			else
				compare[0] = Integer.compare(t1.getDebates().size(), t2.getDebates().size());
			if (compare[0] == 0) {
				return 1;
			} else {
				return compare[0];
			}
		};

		Comparator<Debate> byViewNumber = Comparator.comparingLong(Debate::getViewNumber);


		TreeMap<Theme, List<Debate>> treeMap = new TreeMap<>(byDebates.reversed());
		themes.forEach(theme -> {
			if(!theme.getLabel().equals("Mes débats")) {
				treeMap.put(theme,
						theme.getDebates().stream().filter(x -> x.getDebateState().equals(DebateState.published)).sorted(byViewNumber.reversed()).collect(Collectors.toList()));
			}
		});
		List<Debate> debates = debateDao.findAllByOrderByCreatedDateDesc();
		List<Debate> mostRecentDebates = debates.subList(0, Math.min(debates.size(), 6));
		Theme mostRecent = new Theme(mostRecentThemeLabel);
		treeMap.put(mostRecent, mostRecentDebates);
		return treeMap;
	}

	@Override
	@Transactional
	public Debate deleteTag(Debate debate, Tag tag) {
		tag = tagService.findById(tag.getId());
		Collection<Tag> tagList = debate.getTags();
		if (tagList != null && !tagList.isEmpty() && tag != null) {
			tagList.remove(tag);
			debate.setTags(tagList);
			return debateDao.save(debate);
		} else {
			return debate;
		}
	}

	@Override
	@Transactional
	public void deleteStatement(Debate debate, Statement statement) {
		Collection<Statement> statements = debate.getStatements();

		if (statements != null && !statements.isEmpty() && statement != null) {
			statements.remove(statement);
			debate.setStatements(statements);
			elasticSearchService.updateDebateES(debate);
			debateDao.save(debate);
		}
	}

	@Override
	@Transactional
	public Debate deleteTweet(Debate debate, Tweet tweet) {
		Collection<Tweet> tweetList = debate.getTweets();
		if (tweetList != null && !tweetList.isEmpty() && tweet != null) {
			tweetList.remove(tweet);
			debate.setTweets(tweetList);
			return debateDao.save(debate);
		} else {
			return debate;
		}
	}

	@Override
	@Transactional
	public Debate update(DebateUpdateDto debateUpdateDto, Locale locale, Principal principal) {
		Optional<Debate> optDebate = debateDao.findById(debateUpdateDto.getId());
		if (optDebate.isPresent()) {
			Debate debate = optDebate.get();
			TokenChecker.checkAdminOrUserToken(locale, principal, debate.getUser(), userService);
			// update title
			if (debateUpdateDto.getTitle() != null) {
				if (!debateUpdateDto.getTitle().equals(debate.getTitle())) {
					debate.setTitle(debateUpdateDto.getTitle());
				}
			}
			// update theme
			if (debateUpdateDto.getThemeId() != null) {
				if (!debateUpdateDto.getThemeId().equals(debate.getTheme().getId())) {
					Theme theme = themeService.findById(debateUpdateDto.getThemeId());
					debate.setTheme(theme);
				}
			}
			// update tags
			if (debateUpdateDto.getTags() != null) {
				List<Tag> newTags = new ArrayList<>();
				if (!debateUpdateDto.getTags().isEmpty()) {
					Iterator<String> it = debateUpdateDto.getTags().iterator();
					while (it.hasNext()) {
						String label = it.next();
						Tag tag = tagService.create(label);
						newTags.add(tag);
					}
				}
				debate.setTags(newTags);
			}
			// update imageLink
			if (debateUpdateDto.getImageLink() != null) {
				if (!debateUpdateDto.getImageLink().equals(debate.getImageLink())) {
					debate.setImageLink(debateUpdateDto.getImageLink());
				}
			}
			// update presentation
			if (debateUpdateDto.getPresentation() != null) {
				if (!debateUpdateDto.getPresentation().equals(debate.getPresentation())) {
					debate.setPresentation(debateUpdateDto.getPresentation());
				}
			}
			// update debate state
			if (debateUpdateDto.getDebateState() != null) {
				if (!debateUpdateDto.getDebateState().equals(debate.getDebateState())) {
					debate.setDebateState(debateUpdateDto.getDebateState());
				}
			}

			// update isPrivate
			if (debateUpdateDto.getIsPrivate() != null) {
				if (!debateUpdateDto.getIsPrivate().equals(debate.getIsPrivate())) {
					debate.setIsPrivate(debateUpdateDto.getIsPrivate());
				}
			}

			// update createdByCommunity
			if (debateUpdateDto.getCreatedByCommunity() != null) {
				if (!debateUpdateDto.getCreatedByCommunity().equals(debate.getCreatedByCommunity())) {
					debate.setCreatedByCommunity(debateUpdateDto.getCreatedByCommunity());
				}
			}

			if (debateUpdateDto.getTweets() != null) {
				List<Tweet> newTweets = new ArrayList<>();
				if (!debateUpdateDto.getTweets().isEmpty()) {
					Iterator<String> it = debateUpdateDto.getTweets().iterator();
					while (it.hasNext()) {
						String link = it.next();
						Tweet tweet = tweetService.create(link);
						newTweets.add(tweet);
					}
				}
				List<Tweet> allTweets = Stream.concat(debate.getTweets().stream(), newTweets.stream())
						.collect(Collectors.toList());
				debate.setTweets(allTweets);
			}

			elasticSearchService.updateDebateES(debate);
			return debateDao.save(debate);
		} else {
			throw new NoSuchElementException("No debate found with this id: " + debateUpdateDto.getId());
		}
	}

	@Override
	@Transactional
	public void update (Debate debate){
		try{
			debateDao.save(debate);
		}catch (NoSuchElementException elementException){
			throw new NoSuchElementException("No debate found" + elementException.getMessage());
		}
	}


	@Override
	@Transactional
	public void incrementViews(Long debateId) {
		Optional<Debate> optDebate = debateDao.findById(debateId);

		if (optDebate.isPresent()) {
			Debate debate = optDebate.get();
			debate.incViewNumber();
			elasticSearchService.updateDebateES(debate);
			debateDao.save(debate);
		} else {
			throw new NoSuchElementException("No debate found with this id: " + debateId);
		}
	}

	@Override
	@Transactional
	public void delete(long id, Locale locale, Principal principal) {
		Debate debate = findById(id);
		User user =  userService.findByEmail(principal.getName());

		if(debate.getIsPrivate()){
			Participant participant = participantService.findByUserAndDebate(user,debate);
			if(!participant.getRole().equals(DebateRole.admin) && !userService.isAdmin(user)) {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		}else{
			if(!userService.isAdmin(user)){
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		}

		invitationTokenDao.deleteAllByDebate(debate);
		pollService.delete(debate.getPoll());
		postService.deleteAllByDebate(debate);
		debateDao.deleteById(id);
	}

	@Override
	public List<Debate> findByDebateState(DebateState debateState) {
		return this.debateDao.findByDebateState(debateState);
	}


	@Override
	public List<Debate> findByUser(User user) {
		return this.debateDao.findByUser(user);
	}

	@Override
	@Transactional
	public void saveStatement(Statement statement, Debate debate) {
		Collection<Statement> debateStatements = debate.getStatements();
		debateStatements.add(statement);
		debate.setStatements(debateStatements);
		//elasticSearchService.updateDebateES(debate);
		debateDao.save(debate);
	}

	@Transactional
	private void createInvitationToken(final Debate debate, final String token, final DebateRole role) {
		final InvitationToken myToken = new InvitationToken(token, debate, role);
		invitationTokenDao.save(myToken);
	}

	@SneakyThrows
	@Override
	@Transactional
	public void sendInvitation(String emailAdmin, InvitationDto invitationDto, Locale locale){
		Debate debate = findById(invitationDto.getDebateId());
		User loggedAdmin = userService.findByEmail(emailAdmin);
		Participant adminParticipant = participantService.findByUserAndDebate(loggedAdmin, debate);

		if(adminParticipant.getRole().equals(DebateRole.admin)){
			final String token = UUID.randomUUID().toString();
			createInvitationToken(debate, token, invitationDto.getRole());
			Mail mail = new Mail();
			mail.setFrom(env.getProperty("support.email"));
			mail.setMailTo(invitationDto.getEmail());
		    mail.setSubject(loggedAdmin.getUsername().concat(" ") + messages.getMessage("email.invitationDebate.subject", null, locale));
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("link", env.getProperty("frontEnd.localUrl") + "/debate/private/" + debate.getId() + "/invite/" + token);
			model.put("sender", loggedAdmin.getUsername());
			model.put("title", debate.getTitle());
			model.put("message", invitationDto.getMessage());
			mail.setProps(model);

			emailSenderService.sendEmail(mail, "debate-invitation", locale);

			logger.info("A invitation email has been sent to " + invitationDto.getEmail());
		}else{
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}
	}


	@Override
	@Transactional
	public String validateInvitationToken(String token, String userMail) {
		InvitationToken invitationToken = invitationTokenDao.findByToken(token);
		User user =  userService.findByEmail(userMail);

		if (invitationToken == null) {
			return TOKEN_INVALID;
		}

		final Debate debate = invitationToken.getDebate();
		final Calendar cal = Calendar.getInstance();
		if ((invitationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			invitationTokenDao.delete(invitationToken);
			return TOKEN_EXPIRED;
		}

		participantService.create(user, invitationToken.getRole(), debate);
		invitationTokenDao.delete(invitationToken);
		return TOKEN_VALID;
	}


}