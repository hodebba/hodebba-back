package com.hodebba.back.debate.services;

import com.hodebba.back.celebrity.models.Statement;
import com.hodebba.back.debate.dto.DebateDto;
import com.hodebba.back.debate.dto.DebateUpdateDto;
import com.hodebba.back.debate.dto.InvitationDto;
import com.hodebba.back.debate.models.*;
import com.hodebba.back.user.models.User;

import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

public interface DebateService {

	Debate create(DebateDto debateDtoDto, Locale locale, Principal principal);

	Debate findById(long id);

	Debate findPrivateById(long id, String userEmail);

	List<Debate> findAll();

	List<Debate> findAllPrivate();

	List<Debate> findByDebateState(DebateState debateState);

	List<Debate> findByUser(User user);

	TreeMap<Theme, List<Debate>> findDebatesGroupByTheme();

	Debate deleteTag(Debate debate, Tag tag);

	Debate deleteTweet(Debate debate, Tweet tweet);

	void deleteStatement(Debate debate, Statement statement);

	Debate update(DebateUpdateDto debateUpdateDto, Locale locale, Principal principal);

	void update (Debate debate);

	void incrementViews(Long debateId);

	void saveStatement(Statement statement, Debate debate);

	void delete(long id, Locale locale, Principal principal);

	void sendInvitation(String emailAdmin, InvitationDto invitationDto, Locale locale);

	String validateInvitationToken(String token, String userMail);
	public static final String TOKEN_INVALID = "invalidToken";
	public static final String TOKEN_EXPIRED = "expired";
	public static final String TOKEN_VALID = "valid";
}
