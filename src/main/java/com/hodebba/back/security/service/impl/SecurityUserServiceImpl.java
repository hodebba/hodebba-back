package com.hodebba.back.security.service.impl;

import com.hodebba.back.security.service.SecurityUserService;
import com.hodebba.back.user.models.PasswordResetToken;
import com.hodebba.back.user.persistence.PasswordResetTokenDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.Calendar;

@Service
@Transactional
public class SecurityUserServiceImpl implements SecurityUserService {

	@Autowired
	private PasswordResetTokenDao passwordResetTokenDao;

	@Override
	public String validatePasswordResetToken(String token) {
		final PasswordResetToken passToken = passwordResetTokenDao.findByToken(token);

		if (!isTokenFound(passToken)) {
			return "invalidToken";
		}

		if (isTokenExpired(passToken)) {
			passwordResetTokenDao.deleteByToken(token);
			return "expired";
		}
		return null;
	}

	private boolean isTokenFound(PasswordResetToken passToken) {
		return passToken != null;
	}

	private boolean isTokenExpired(PasswordResetToken passToken) {
		final Calendar cal = Calendar.getInstance();
		return passToken.getExpiryDate().before(cal.getTime());
	}
}