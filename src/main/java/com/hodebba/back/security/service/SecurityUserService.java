package com.hodebba.back.security.service;

public interface SecurityUserService {
	String validatePasswordResetToken(String token);
}
