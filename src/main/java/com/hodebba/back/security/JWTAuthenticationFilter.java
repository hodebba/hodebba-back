package com.hodebba.back.security;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hodebba.back.user.dto.UserDisplayDto;
import com.hodebba.back.user.models.User;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hodebba.back.user.services.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.hodebba.back.security.constant.SecurityConstants.EXPIRATION_TIME;
import static com.hodebba.back.security.constant.SecurityConstants.HEADER_STRING;
import static com.hodebba.back.security.constant.SecurityConstants.SECRET;
import static com.hodebba.back.security.constant.SecurityConstants.TOKEN_PREFIX;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private AuthenticationManager authenticationManager;
	private final UserService userService;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager, UserService userService) {
		this.authenticationManager = authenticationManager;
		this.userService = userService;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException {
		try {
			User creds = new ObjectMapper().readValue(req.getInputStream(), User.class);

			return authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(creds.getEmail(), creds.getPassword(), new ArrayList<>()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {

		getTokenAndSendUserInformation(
				((org.springframework.security.core.userdetails.User) auth.getPrincipal()).getUsername(), res);
	}

	public void getTokenAndSendUserInformation(String email, HttpServletResponse res) throws IOException {
		String token = Jwts.builder().setSubject(email)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SECRET.getBytes()).compact();
		User user = userService.findByEmail(email);
		res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
		res.setContentType("application/json");
		res.setCharacterEncoding("UTF-8");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.serializeNulls().create();
		String userString = gson.toJson(new UserDisplayDto(user));
		res.getWriter().write(userString);
		res.setHeader("Access-Control-Expose-Headers", "Authorization");
	}
}
