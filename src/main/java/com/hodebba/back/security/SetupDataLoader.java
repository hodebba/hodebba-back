package com.hodebba.back.security;

import com.hodebba.back.user.models.Privilege;
import com.hodebba.back.user.models.Role;
import com.hodebba.back.user.models.User;
import com.hodebba.back.user.persistence.DataJpaUserDao;
import com.hodebba.back.user.persistence.PrivilegeDao;
import com.hodebba.back.user.persistence.RoleDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.hodebba.back.security.constant.PrivilegeConstants.*;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger logger = LoggerFactory.getLogger(SetupDataLoader.class);

	private boolean alreadySetup = false;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private PrivilegeDao privilegeDao;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private DataJpaUserDao userDao;

	@Autowired
	private Environment env;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

		if (alreadySetup)
			return;
		Privilege read = createPrivilegeIfNotFound(PRIVILEGE_READ);
		Privilege readDebate = createPrivilegeIfNotFound(PRIVILEGE_READ_DEBATE);
		Privilege createDebate = createPrivilegeIfNotFound(PRIVILEGE_CREATE_DEBATE);
		Privilege deleteDebate = createPrivilegeIfNotFound(PRIVILEGE_DELETE_DEBATE);
		Privilege updateDebate = createPrivilegeIfNotFound(PRIVILEGE_UPDATE_DEBATE);
		Privilege votePoll = createPrivilegeIfNotFound(PRIVILEGE_CREATE_VOTE);
		Privilege createTheme = createPrivilegeIfNotFound(PRIVILEGE_CREATE_THEME);
		Privilege deleteTheme = createPrivilegeIfNotFound(PRIVILEGE_DELETE_THEME);
		Privilege deleteTag = createPrivilegeIfNotFound(PRIVILEGE_DELETE_TAG);
		Privilege deleteTweet = createPrivilegeIfNotFound(PRIVILEGE_DELETE_TWEET);
		Privilege updatePost = createPrivilegeIfNotFound(PRIVILEGE_UPDATE_POST);
		Privilege readArgument = createPrivilegeIfNotFound(PRIVILEGE_READ_ARGUMENT);
		Privilege createArgument = createPrivilegeIfNotFound(PRIVILEGE_CREATE_ARGUMENT);
		Privilege deleteArgument = createPrivilegeIfNotFound(PRIVILEGE_DELETE_ARGUMENT);
		Privilege promoteUser = createPrivilegeIfNotFound(PRIVILEGE_PROMOTE_USER);
		Privilege createLike = createPrivilegeIfNotFound(PRIVILEGE_CREATE_LIKE);
		Privilege deleteLike = createPrivilegeIfNotFound(PRIVILEGE_DELETE_LIKE);
		Privilege createCelebrity = createPrivilegeIfNotFound(PRIVILEGE_CREATE_CELEBRITY);
		Privilege createPosition = createPrivilegeIfNotFound(PRIVILEGE_CREATE_POSITION);
		Privilege deletePosition = createPrivilegeIfNotFound(PRIVILEGE_DELETE_POSITION);
		Privilege createStatement = createPrivilegeIfNotFound(PRIVILEGE_CREATE_STATEMENT);
		Privilege deleteStatement = createPrivilegeIfNotFound(PRIVILEGE_DELETE_STATEMENT);
		Privilege updateStatement = createPrivilegeIfNotFound(PRIVILEGE_UPDATE_STATEMENT);
		Privilege createProfession = createPrivilegeIfNotFound(PRIVILEGE_CREATE_PROFESSION);
		Privilege createPoliticalParties = createPrivilegeIfNotFound(PRIVILEGE_CREATE_POLITICAL_PARTIES);
		Privilege createPoliticalPositions = createPrivilegeIfNotFound(PRIVILEGE_CREATE_POLITICAL_POSITIONS);
		Privilege promoteParticipant = createPrivilegeIfNotFound(PRIVILEGE_PROMOTE_PARTICIPANT);

		List<Privilege> adminPrivileges = Arrays.asList(read,readDebate, createDebate, deleteDebate, votePoll, createTheme,
				deleteTheme, deleteTag, readArgument, createArgument, deleteArgument, promoteUser, createLike,
				deleteLike, deleteTweet, deleteTag, createCelebrity, createPosition, deletePosition, deleteStatement,
				createStatement, updateStatement, createProfession, createPoliticalParties, createPoliticalPositions,
				updatePost, updateDebate, promoteParticipant);
		List<Privilege> moderatorPrivileges = Arrays.asList(read,readDebate, votePoll, readArgument, createArgument,
				deleteArgument, createLike, deleteLike, createStatement, createDebate, promoteParticipant,deleteDebate);
		List<Privilege> userPrivileges = Arrays.asList(read,readDebate, votePoll, readArgument, createArgument, createLike,
				deleteLike, deleteArgument, createStatement, createDebate, promoteParticipant,deleteDebate);
		List<Privilege> anonymousPrivileges = Arrays.asList(read, votePoll);

		createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
		createRoleIfNotFound("ROLE_MODERATOR", moderatorPrivileges);
		createRoleIfNotFound("ROLE_USER", userPrivileges);
		createRoleIfNotFound("ROLE_ANONYMOUS", anonymousPrivileges);

		if (userDao.findByUsername("Admin") == null) {
			Role adminRole = roleDao.findByName("ROLE_ADMIN");
			User user = new User();
			user.setUsername(env.getProperty("hodebba.admin.username"));
			user.setPassword(bCryptPasswordEncoder.encode(env.getProperty("hodebba.admin.password")));
			user.setEmail(env.getProperty("hodebba.admin.email"));
			user.setEnabled(true);
			user.setRoles(Arrays.asList(adminRole));
			userDao.save(user);
			logger.info("A new User has been created : " + user.toString());
		}

		if(userDao.findByUsername("Anonyme") == null){
			Role anonymousRole = roleDao.findByName("ROLE_ANONYMOUS");
			User anonymous = new User();
			anonymous.setUsername("Anonyme");
			anonymous.setPassword(bCryptPasswordEncoder.encode("gjiejCVVSnzd448ve&#fez2Z2"));
			anonymous.setEmail("noemail@noemail.com");
			anonymous.setEnabled(true);
			anonymous.setRoles(Arrays.asList(anonymousRole));
			userDao.save(anonymous);
			logger.info("A new User has been created : " + anonymous.toString());
		}

		alreadySetup = true;
	}

	@Transactional
	private Privilege createPrivilegeIfNotFound(String name) {

		Privilege privilege = privilegeDao.findByName(name);
		if (privilege == null) {
			privilege = new Privilege(name);
			privilegeDao.save(privilege);
			logger.info("The privilege " + privilege.toString() + " has been created.");
		}
		return privilege;
	}

	@Transactional
	private Role createRoleIfNotFound(String name, Collection<Privilege> privileges) {

		Role role = roleDao.findByName(name);
		if (role == null) {
			role = new Role(name);
			role.setPrivileges(privileges);
			roleDao.save(role);
			logger.info("A new Role has been created : " + role.toString());
		} else if (!role.getPrivileges().containsAll(privileges) || !privileges.containsAll(role.getPrivileges())) {
			role.setPrivileges(privileges);
			roleDao.save(role);
			logger.info("A Role has been updated : " + role.toString());
		}
		return role;
	}
}
