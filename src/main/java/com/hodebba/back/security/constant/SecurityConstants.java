package com.hodebba.back.security.constant;

public class SecurityConstants {
	public static final String SECRET = "SecretKeyToGenJWTs";
	// public static final long EXPIRATION_TIME = 864_000_000; // 10 days
	public static final long EXPIRATION_TIME = 63_307_000_000L; // 732 days
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String SIGN_UP_URL = "/users";
	public static final String RESET_PASSWORD_URL = "/users/password/reset/*";
	public static final String CHECK_PASSWORD_URL = "/users/password/valid/*";
	public static final String UPDATE_PASSWORD_URL = "/users/password";
	public static final String SAVE_PASSWORD_URL = "/users/password/save";
	public static final String CONFIRM_EMAIL_URL = "/users/registration/confirm/*";
}