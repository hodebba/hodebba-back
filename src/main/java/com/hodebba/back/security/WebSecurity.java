package com.hodebba.back.security;

import com.google.common.collect.ImmutableList;
import com.hodebba.back.user.services.UserService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import static com.hodebba.back.security.constant.PrivilegeConstants.*;
import static com.hodebba.back.security.constant.SecurityConstants.*;

@EnableWebSecurity
@AllArgsConstructor
public class WebSecurity extends WebSecurityConfigurerAdapter {
	@Autowired
	private MyUserDetailsService userDetailsService;

	private static final Logger logger = LoggerFactory.getLogger(WebSecurity.class);

	@Autowired
	private Environment env;

	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private final UserService userService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests().antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
				.antMatchers(HttpMethod.POST, "/users/socials").permitAll()
				.antMatchers(HttpMethod.POST, RESET_PASSWORD_URL, SAVE_PASSWORD_URL).permitAll()
				.antMatchers(HttpMethod.GET, CHECK_PASSWORD_URL).permitAll()
				.antMatchers(HttpMethod.GET, CONFIRM_EMAIL_URL).permitAll()
				.antMatchers(HttpMethod.GET, "/users/promote/admin/*", "/users/promote/moderator/*",
						"/users/promote/user/*")
				.hasAuthority(PRIVILEGE_PROMOTE_USER).antMatchers(HttpMethod.DELETE, "/users/*").permitAll()

				.antMatchers(HttpMethod.GET, "/debates", "/debates/*", "/debates/home", "/debates/private", "/debates/debate-state/*", "/debates/*/private-public")
				.permitAll().antMatchers(HttpMethod.POST, "/debates", "/debates/invite", "/debates/invite/*").hasAuthority(PRIVILEGE_CREATE_DEBATE)
				.antMatchers(HttpMethod.GET, "/debates/*/private").hasAuthority(PRIVILEGE_READ)
				.antMatchers(HttpMethod.PUT, "/debates")
				.hasAuthority(PRIVILEGE_UPDATE_DEBATE).antMatchers(HttpMethod.DELETE, "/debates/*")
				.hasAuthority(PRIVILEGE_DELETE_DEBATE).antMatchers(HttpMethod.POST, "/debates/update-es")
				.hasAuthority(PRIVILEGE_UPDATE_DEBATE)

				.antMatchers(HttpMethod.POST, "/debates/views").permitAll()

				.antMatchers(HttpMethod.GET, "/debates/poll/*").permitAll()
				.antMatchers(HttpMethod.GET, "/debates/user/*/poll").hasAuthority(PRIVILEGE_CREATE_VOTE)
				.antMatchers(HttpMethod.POST, "/debates/poll").hasAuthority(PRIVILEGE_CREATE_VOTE)

				.antMatchers(HttpMethod.GET, "/debates/*/tweets", "/tweets").permitAll()
				.antMatchers(HttpMethod.DELETE, "/debates/tweets").hasAuthority(PRIVILEGE_CREATE_VOTE)

				.antMatchers(HttpMethod.PUT, "/participants").hasAuthority(PRIVILEGE_PROMOTE_PARTICIPANT)
				.antMatchers(HttpMethod.DELETE, "/participants/*").hasAuthority(PRIVILEGE_PROMOTE_PARTICIPANT)
				.antMatchers(HttpMethod.GET, "/participants/users").hasAuthority(PRIVILEGE_READ)

				.antMatchers(HttpMethod.PUT, "/posts").hasAuthority(PRIVILEGE_UPDATE_POST)

				.antMatchers(HttpMethod.POST, "/posts/argument", "/posts/comments", "posts/testimonials",
						"posts/argument-comments")
				.hasAuthority(PRIVILEGE_CREATE_ARGUMENT)
				.antMatchers(HttpMethod.GET, "/posts/*", "/debates/*/arguments", "/debates/*/testimonials",
						"/posts/users/*")
				.permitAll().antMatchers(HttpMethod.DELETE, "/posts/*").hasAuthority(PRIVILEGE_DELETE_ARGUMENT)

				.antMatchers(HttpMethod.POST, "/posts/likes", "/celebrities/statements/likes", "/statements/likes")
				.hasAuthority(PRIVILEGE_CREATE_LIKE).antMatchers(HttpMethod.DELETE, "/likes/*")
				.hasAuthority(PRIVILEGE_DELETE_LIKE).antMatchers(HttpMethod.GET, "/posts/likes", "/statements/likes")
				.permitAll()

				.antMatchers(HttpMethod.GET, "/debates/themes", "/debates/themes/*").permitAll()
				.antMatchers(HttpMethod.POST, "/debates/themes").hasAuthority(PRIVILEGE_CREATE_THEME)
				.antMatchers(HttpMethod.PUT, "/debates/themes").hasAuthority(PRIVILEGE_CREATE_THEME)
				.antMatchers(HttpMethod.DELETE, "/debates/themes/*").hasAuthority(PRIVILEGE_DELETE_THEME)

				.antMatchers(HttpMethod.GET, "/debates/tags/*", "/debates/tags").permitAll()
				.antMatchers(HttpMethod.DELETE, "/debates/tags/*").hasAuthority(PRIVILEGE_DELETE_TAG)

				.antMatchers(HttpMethod.GET, "/celebrities", "/celebrities/*", "/celebrities/topics",
						"/celebrities/*/positions", "/celebrities/positions/*", "/celebrities/statements/*",
						"/celebrities/*/statements", "/celebrities/debates/*/positions",
						"/celebrities/debates/*/statements")
				.permitAll()
				.antMatchers(HttpMethod.POST, "/celebrities", "celebrities/topics", "/celebrities/update-es")
				.hasAuthority(PRIVILEGE_CREATE_CELEBRITY).antMatchers(HttpMethod.POST, "/celebrities/positions")
				.hasAuthority(PRIVILEGE_CREATE_POSITION).antMatchers(HttpMethod.PUT, "/celebrities/positions")
				.hasAuthority(PRIVILEGE_CREATE_POSITION).antMatchers(HttpMethod.DELETE, "/celebrities/positions/*")
				.hasAuthority(PRIVILEGE_DELETE_POSITION).antMatchers(HttpMethod.POST, "/celebrities/statements")
				.hasAuthority(PRIVILEGE_CREATE_STATEMENT).antMatchers(HttpMethod.DELETE, "/celebrities/statements/*")
				.hasAuthority(PRIVILEGE_DELETE_STATEMENT).antMatchers(HttpMethod.PUT, "/celebrities/statements")
				.hasAuthority(PRIVILEGE_UPDATE_STATEMENT)

				.antMatchers(HttpMethod.GET, "/celebrities/professions/*", "/celebrities/professions").permitAll()
				.antMatchers(HttpMethod.GET, "/celebrities/professions").hasAuthority(PRIVILEGE_CREATE_PROFESSION)
				.antMatchers(HttpMethod.PUT, "/celebrities/professions").hasAuthority(PRIVILEGE_CREATE_PROFESSION)

				.antMatchers(HttpMethod.GET, "/celebrities/political-parties/*", "/celebrities/political-parties")
				.permitAll().antMatchers(HttpMethod.GET, "/celebrities/political-parties")
				.hasAuthority(PRIVILEGE_CREATE_POLITICAL_PARTIES)

				.antMatchers(HttpMethod.GET, "/celebrities/political-positions/*", "/celebrities/political-positions")
				.permitAll().antMatchers(HttpMethod.GET, "/celebrities/political-positions")
				.hasAuthority(PRIVILEGE_CREATE_POLITICAL_POSITIONS)

				.antMatchers(HttpMethod.POST, "/customers/contact-us", "/customers/report-post").permitAll()

				// Authorize Swagger routes
				.antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/security",
						"/swagger-ui.html", "/webjars/**")
				.permitAll().anyRequest().authenticated().and()
				.addFilter(new JWTAuthenticationFilter(authenticationManager(), userService))
				.addFilter(new JWTAuthorizationFilter(authenticationManager(), userService))
				// this disables session creation on Spring Security
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		logger.info("Front-end URL: {}", env.getProperty("frontEnd.localUrl"));
		final CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(ImmutableList.of(env.getProperty("frontEnd.localUrl")));
		configuration.setAllowedMethods(ImmutableList.of("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
		configuration.setAllowCredentials(true);
		configuration.setAllowedHeaders(ImmutableList.of("*"));
		configuration.setExposedHeaders(ImmutableList.of("X-Auth-Token", "Authorization", "Access-Control-Allow-Origin",
				"Access-Control-Allow-Credentials"));
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		logger.info("CORS Allowed origins {}.", configuration.getAllowedOrigins());
		return source;
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
}
