package com.hodebba.back.security.utils;

import com.hodebba.back.user.models.User;
import com.hodebba.back.user.services.UserService;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.Locale;

public class TokenChecker {

	private static MessageSource messages;

	public static void checkUserToken(Locale locale, Principal principal, User user) {
		String loggedEmail = principal.getName();
		if (!loggedEmail.equals(user.getEmail())) {
			String message = messages.getMessage("deleteUser.unauthorized", null, locale);
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
		}
	}

	public static void checkAdminOrUserToken(Locale locale, Principal principal, User user, UserService userService){
		String loggedEmail = principal.getName();
		User loggedUser = userService.findByEmail(loggedEmail);
		if (!userService.isAdmin(loggedUser) && !loggedEmail.equals(user.getEmail())){
			String message = messages.getMessage("deleteUser.unauthorized", null, locale);
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, message);
		}
	}

}
