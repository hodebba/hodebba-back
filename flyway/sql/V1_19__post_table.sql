
CREATE TABLE public.like_dislike_post
(
    id      BIGINT PRIMARY KEY REFERENCES public.like_dislike,
    post_id BIGSERIAL NOT NULL,
    FOREIGN KEY (post_id) REFERENCES public.post (id)
);

INSERT INTO public.like_dislike_post(id, post_id) SELECT id, post_id FROM public.like_dislike;

ALTER TABLE public.like_dislike DROP COLUMN post_id;

