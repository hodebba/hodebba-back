CREATE TABLE public.source_post
(
    id      BIGSERIAL PRIMARY KEY REFERENCES public.source,
    post_id BIGSERIAL NOT NULL,
    FOREIGN KEY (post_id) REFERENCES public.post(id)
);

INSERT INTO public.source_post(id, post_id) SELECT id, post_id FROM public.source;

ALTER TABLE public.source DROP COLUMN post_id;
