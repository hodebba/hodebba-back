ALTER TABLE public.debate
    ADD COLUMN is_private BOOLEAN,
    ADD COLUMN created_by_community BOOLEAN;

CREATE TABLE public.participant(
    id BIGSERIAL PRIMARY KEY,
    role VARCHAR NOT NULL,
    user_id      BIGSERIAL NOT NULL,
    debate_id BIGSERIAL NOT NULL,
    FOREIGN KEY (user_id) REFERENCES public.users (id),
    FOREIGN KEY (debate_id) REFERENCES public.debate (id)
);
ALTER SEQUENCE participant_id_seq INCREMENT BY 50;


CREATE TABLE public.invitation_token
(
    id BIGSERIAL PRIMARY KEY,
    token VARCHAR NOT NULL,
    role VARCHAR NOT NULL,
    debate_id BIGSERIAL,
    expiry_date TIMESTAMP,
    FOREIGN KEY(debate_id) REFERENCES public.debate(id)
);
ALTER SEQUENCE invitation_token_id_seq INCREMENT BY 50;

INSERT INTO public.theme
values (100, 'Mes débats');