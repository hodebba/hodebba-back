-- INITIALIZE THEMES
ALTER SEQUENCE theme_id_seq RESTART WITH 50 INCREMENT BY 50;

INSERT INTO public.theme
values (1, 'Arts');
INSERT INTO public.theme
values (2, 'Cinéma');
INSERT INTO public.theme
values (3, 'Economie');
INSERT INTO public.theme
values (4, 'Environnement');
INSERT INTO public.theme
values (5, 'Histoire');
INSERT INTO public.theme
values (6, 'International');
INSERT INTO public.theme
values (7, 'Musique');
INSERT INTO public.theme
values (8, 'Politique française');
INSERT INTO public.theme
values (9, 'Santé');
INSERT INTO public.theme
values (10, 'Société');
INSERT INTO public.theme
values (11, 'Sport');
INSERT INTO public.theme
values (12, 'Technologie');

-- INITIALIZE TOPICS
ALTER SEQUENCE topic_id_seq RESTART WITH 50 INCREMENT BY 50;

INSERT INTO public.topic
VALUES (1, 'L''agriculture');
INSERT INTO public.topic
VALUES (2, 'La démocratie');
INSERT INTO public.topic
VALUES (3, 'L''économie');
INSERT INTO public.topic
VALUES (4, 'L''éducation');
INSERT INTO public.topic
VALUES (5, 'L''Europe');
INSERT INTO public.topic
VALUES (6, 'L''énergie et l''environnement');
INSERT INTO public.topic
VALUES (7, 'Les impôts et les taxes');
INSERT INTO public.topic
VALUES (8, 'L''immigration');
INSERT INTO public.topic
VALUES (9, 'L''international');
INSERT INTO public.topic
VALUES (10, 'Le racisme');
INSERT INTO public.topic
VALUES (11, 'Les religions');
INSERT INTO public.topic
VALUES (12, 'La santé');
INSERT INTO public.topic
VALUES (13, 'Le sport');
INSERT INTO public.topic
VALUES (14, 'La sécurité');
INSERT INTO public.topic
VALUES (15, 'La technologie');
INSERT INTO public.topic
VALUES (16, 'Le travail');
INSERT INTO public.topic
VALUES (17, 'Autres');

-- INITIALIZE PROFESSIONS
ALTER SEQUENCE profession_id_seq RESTART WITH 50 INCREMENT BY 50;

INSERT INTO public.profession
VALUES (1, 'Politique');
INSERT INTO public.profession
VALUES (2, 'Journaliste');
INSERT INTO public.profession
VALUES (3, 'Activiste');
INSERT INTO public.profession
VALUES (4, 'Artiste');
INSERT INTO public.profession
VALUES (5, 'Philosophe');
INSERT INTO public.profession
VALUES (6, 'Scientifique');
INSERT INTO public.profession
VALUES (7, 'Avocat');
INSERT INTO public.profession
VALUES (8, 'Sportif');


-- INITIALIZE POLITICAL POSITIONS
ALTER SEQUENCE political_position_id_seq RESTART WITH 50 INCREMENT BY 50;

INSERT INTO public.political_position
VALUES (1, 'Extrême gauche / gauche radicale');
INSERT INTO public.political_position
VALUES (2, 'Gauche');
INSERT INTO public.political_position
VALUES (3, 'Centre gauche');
INSERT INTO public.political_position
VALUES (4, 'Centre');
INSERT INTO public.political_position
VALUES (5, 'Centre droit');
INSERT INTO public.political_position
VALUES (6, 'Droite');
INSERT INTO public.political_position
VALUES (7, 'Extrême droite / droite radicale');


-- INITIALIZE POLITICAL PARTIES
ALTER SEQUENCE political_party_id_seq RESTART WITH 50 INCREMENT BY 50;

INSERT INTO public.political_party
VALUES (1, 'Fédération anarchiste (FA)');
INSERT INTO public.political_party
VALUES (2, 'Lutte ouvrière (LO)');
INSERT INTO public.political_party
VALUES (3, 'Nouveau Parti anticapitaliste (NPA)');
INSERT INTO public.political_party
VALUES (4, 'Union communiste libertaire (UCL)');
INSERT INTO public.political_party
VALUES (5, 'La France insoumise (LFI)');
INSERT INTO public.political_party
VALUES (6, 'Parti communiste français (PCF)');
INSERT INTO public.political_party
VALUES (7, 'Gauche républicaine et socialiste (GRS)');
INSERT INTO public.political_party
VALUES (8, 'Génération.s (G·s)');
INSERT INTO public.political_party
VALUES (9, 'Parti socialiste (PS)');
INSERT INTO public.political_party
VALUES (10, 'Europe Écologie Les Verts (EELV)');
INSERT INTO public.political_party
VALUES (11, 'Parti radical de gauche (PRG)');
INSERT INTO public.political_party
VALUES (12, 'Place publique (PP)');
INSERT INTO public.political_party
VALUES (13, 'Union des démocrates et des écologistes (UDE)');
INSERT INTO public.political_party
VALUES (14, 'Alliance écologiste indépendante (AEI)');
INSERT INTO public.political_party
VALUES (15, 'La République en marche (LREM)');
INSERT INTO public.political_party
VALUES (16, 'Mouvement démocrate (MoDem)');
INSERT INTO public.political_party
VALUES (17, 'Mouvement radical (MR)');
INSERT INTO public.political_party
VALUES (18, 'Agir');
INSERT INTO public.political_party
VALUES (19, 'Union des démocrates et indépendants (UDI)');
INSERT INTO public.political_party
VALUES (20, 'Les Centristes (LC)');
INSERT INTO public.political_party
VALUES (21, 'Les Républicains (LR)');
INSERT INTO public.political_party
VALUES (22, 'Centre national des indépendants et paysans (CNIP)');
INSERT INTO public.political_party
VALUES (23, 'Action française (AF)');
INSERT INTO public.political_party
VALUES (24, 'Debout la France (DLF)');
INSERT INTO public.political_party
VALUES (25, 'Les Patriotes (LP)');
INSERT INTO public.political_party
VALUES (26, 'Rassemblement national (RN)');
INSERT INTO public.political_party
VALUES (27, 'Parti animaliste (PA)');
INSERT INTO public.political_party
VALUES (28, 'Régions et peuples solidaires (RPS)');
INSERT INTO public.political_party
VALUES (29, 'Union populaire républicaine (UPR)');

