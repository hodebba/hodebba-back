CREATE TABLE public.post
(
    id           BIGSERIAL PRIMARY KEY,
    title        VARCHAR,
    created_date TIMESTAMP NOT NULL,
    content      TEXT      NOT NULL,
    likes        BIGSERIAL,
    dislikes     BIGSERIAL,
    user_id      BIGSERIAL NOT NULL,
    debate_id    BIGSERIAL NOT NULL,
    FOREIGN KEY (user_id) REFERENCES public.users (id),
    FOREIGN KEY (debate_id) REFERENCES public.debate (id)
);
ALTER SEQUENCE post_id_seq INCREMENT BY 50;


CREATE TABLE public.comment
(
    id            BIGINT PRIMARY KEY REFERENCES public.post,
    parent_id     BIGSERIAL,
    is_last_level BOOLEAN,
    FOREIGN KEY (parent_id) REFERENCES public.post (id)
);

CREATE TABLE argument_comment
(
    id        BIGINT PRIMARY KEY REFERENCES public.comment,
    post_type VARCHAR
);

CREATE TABLE public.argument
(
    id        BIGINT PRIMARY KEY REFERENCES public.post,
    post_type VARCHAR NOT NULL
);

CREATE TABLE public.testimonial
(
    id BIGINT PRIMARY KEY REFERENCES public.post
);


CREATE TABLE public.like_dislike
(
    id      BIGSERIAL PRIMARY KEY,
    is_like BOOLEAN,
    user_id BIGSERIAL NOT NULL,
    post_id BIGSERIAL NOT NULL,
    FOREIGN KEY (user_id) REFERENCES public.users (id),
    FOREIGN KEY (post_id) REFERENCES public.post (id)
);
ALTER SEQUENCE like_dislike_id_seq INCREMENT BY 50;

CREATE TABLE public.source
(
    id      BIGSERIAL PRIMARY KEY,
    link    VARCHAR,
    post_id BIGSERIAL NOT NULL,
    FOREIGN KEY (post_id) REFERENCES public.post (id)
);
ALTER SEQUENCE source_id_seq INCREMENT BY 50;
