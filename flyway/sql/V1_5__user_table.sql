CREATE TABLE public.users
(
  id        BIGSERIAL PRIMARY KEY,
  username VARCHAR UNIQUE NOT NULL,
  password  VARCHAR NOT NULL,
  email     VARCHAR UNIQUE NOT NULL,
  enabled   BOOLEAN,
  description VARCHAR,
  location VARCHAR,
  image_link VARCHAR
);
ALTER SEQUENCE users_id_seq INCREMENT BY 50;

CREATE TABLE public.privilege
(
    id        BIGSERIAL PRIMARY KEY,
    name VARCHAR UNIQUE NOT NULL
);
ALTER SEQUENCE privilege_id_seq INCREMENT BY 50;

CREATE TABLE public.role
(
    id        BIGSERIAL PRIMARY KEY,
    name VARCHAR UNIQUE NOT NULL
);
ALTER SEQUENCE role_id_seq INCREMENT BY 50;

CREATE TABLE public.users_roles
(
    id        BIGSERIAL PRIMARY KEY,
    user_id BIGSERIAL,
    role_id BIGSERIAL,
    FOREIGN KEY(role_id) REFERENCES public.role(id),
    FOREIGN KEY(user_id) REFERENCES public.users(id)
);
ALTER SEQUENCE users_roles_id_seq INCREMENT BY 50;

CREATE TABLE public.roles_privileges
(
    id        BIGSERIAL PRIMARY KEY,
    privilege_id BIGSERIAL,
    role_id BIGSERIAL,
    FOREIGN KEY(role_id) REFERENCES public.role(id),
    FOREIGN KEY(privilege_id) REFERENCES public.privilege(id)
);
ALTER SEQUENCE roles_privileges_id_seq INCREMENT BY 50;

CREATE TABLE public.password_reset_token
(
    id        BIGSERIAL PRIMARY KEY,
    token VARCHAR NOT NULL,
    expiry_date  TIMESTAMP NOT NULL,
    user_id BIGSERIAL,
    FOREIGN KEY(user_id) REFERENCES public.users(id)
);
ALTER SEQUENCE password_reset_token_id_seq INCREMENT BY 50;

CREATE TABLE public.verification_token
(
    id        BIGSERIAL PRIMARY KEY,
    token VARCHAR NOT NULL,
    user_id BIGSERIAL,
    expiry_date TIMESTAMP,
    FOREIGN KEY(user_id) REFERENCES public.users(id)
);
ALTER SEQUENCE verification_token_id_seq INCREMENT BY 50;

CREATE TABLE public.user_socials_mapping
(
    id        BIGSERIAL PRIMARY KEY,
    socials_name VARCHAR,
    link VARCHAR,
    user_id BIGSERIAL,
    FOREIGN KEY(user_id) REFERENCES public.users(id)
);
ALTER SEQUENCE user_socials_mapping_id_seq INCREMENT BY 50;


