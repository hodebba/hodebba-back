-- INITIALIZE CELEBRTIES
ALTER SEQUENCE celebrity_id_seq RESTART WITH 200 INCREMENT BY 50;

INSERT INTO public.celebrity
VALUES (1, 'Jean-Luc Mélenchon', 'celebrities/njdrb4cbh8epscltofh3', 1, 1, 5);
INSERT INTO public.celebrity
VALUES (2, 'Marine Le Pen', 'celebrities/sp93aqimv5lul8psdbo3', 1, 7, 26);
INSERT INTO public.celebrity
VALUES (3, 'Emmanuel Macron', 'celebrities/ttpodep7tg7iwllm5cul', 1, 4, 15);
INSERT INTO public.celebrity
VALUES (4, 'François Ruffin', 'celebrities/o01inoyrkd5h3thwaeec', 1, 1, null);
INSERT INTO public.celebrity
VALUES (5, 'Marlène Schiappa', 'celebrities/hmp6i1p7r8tdqdkikqn9', 1, 4, 15);
INSERT INTO public.celebrity
VALUES (6, 'Rokhaya Diallo', 'celebrities/wmb4xp3hnldzvvxfxxjr', 2, null, null);
INSERT INTO public.celebrity
VALUES (7, 'Éric Zemmour', 'celebrities/eqntxchaudqoyzf1he2z', 2, 7, null);
INSERT INTO public.celebrity
VALUES (8, 'Pascal Praud', 'celebrities/w79rleybcfsboavlgr6h', 2, null, null);
INSERT INTO public.celebrity
VALUES (9, 'Natacha Polony', 'celebrities/b2hnv2byd9at0gvgpvov', 2, null, null);
INSERT INTO public.celebrity
VALUES (10, 'Kémi Séba', 'celebrities/ywvnsgeprsbbqqwwrloe', 3, null, null);
INSERT INTO public.celebrity
VALUES (11, 'Maxime Nicolle', 'celebrities/tt1atbfwecyegnzwwnfq', 3, null, null);
INSERT INTO public.celebrity
VALUES (12, 'Assa Traoré', 'celebrities/pyjdcxdlulrdebc2s2eq', 3, null, null);
INSERT INTO public.celebrity
VALUES (13, 'Greta Thunberg', 'celebrities/mlwiy3vlnqtzltbbo8ts', 3, null, null);
INSERT INTO public.celebrity
VALUES (14, 'François Fillon', 'celebrities/xum0unx4ssj9s8gqeqgj1s', 1, 6, null);
INSERT INTO public.celebrity
VALUES (15, 'Olivier Besancenot', 'celebrities/rmzxds9dailvabhok5gt7k', 1, 1, 3);
INSERT INTO public.celebrity
VALUES (16, 'Christiane Taubira', 'celebrities/g9v1104totenny02xeoj8w', 1, 3, 11);
INSERT INTO public.celebrity
VALUES (17, 'Amandine Gay', 'celebrities/xcdhxu43nlidblnimraj', 2, null, null);
INSERT INTO public.celebrity
VALUES (18, 'Audrey Pulvar', 'celebrities/xe1pf1wbgdar66nxu9ve', 2, null, null);
INSERT INTO public.celebrity
VALUES (19, 'Benoît Hamon', 'celebrities/o3u7owsluh43pf3qhydg', 1, 2, 9);
INSERT INTO public.celebrity
VALUES (20, 'Nicolas Sarkozy', 'celebrities/rp7vcccp8kgqjrnx7qd3', 1, 6, 21);
INSERT INTO public.celebrity
VALUES (21, 'Christophe Barbier', 'celebrities/x7c1fnntuq4o8r3p7kmr', 2, null, null);

