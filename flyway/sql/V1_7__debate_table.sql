CREATE TABLE public.theme(
    id BIGSERIAL PRIMARY KEY,
    label VARCHAR NOT NULL UNIQUE
);
ALTER SEQUENCE theme_id_seq INCREMENT BY 50;

CREATE TABLE public.tag(
    id BIGSERIAL PRIMARY KEY,
    label VARCHAR NOT NULL UNIQUE
);
ALTER SEQUENCE tag_id_seq INCREMENT BY 50;

CREATE TABLE public.tweet(
   id BIGSERIAL PRIMARY KEY,
   link VARCHAR NOT NULL UNIQUE
);
ALTER SEQUENCE tweet_id_seq INCREMENT BY 50;

CREATE TABLE public.debate(
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR,
    image_link VARCHAR,
    created_date TIMESTAMP NOT NULL,
    view_number BIGSERIAL NOT NULL,
    presentation TEXT,
    debate_state VARCHAR NOT NULL,
    user_id BIGSERIAL NOT NULL,
    theme_id BIGSERIAL,
    FOREIGN KEY(user_id) REFERENCES public.users(id),
    FOREIGN KEY(theme_id) REFERENCES public.theme(id)
);
ALTER SEQUENCE debate_id_seq INCREMENT BY 50;

CREATE TABLE public.debate_tweet(
    id BIGSERIAL PRIMARY KEY,
    debate_id BIGSERIAL,
    tweet_id BIGSERIAL,
    FOREIGN KEY(debate_id) REFERENCES public.debate(id),
    FOREIGN KEY(tweet_id) REFERENCES public.tweet(id)
);

CREATE TABLE public.debate_tag(
    id BIGSERIAL PRIMARY KEY,
    debate_id BIGSERIAL,
    tag_id BIGSERIAL,
    FOREIGN KEY(debate_id) REFERENCES public.debate(id),
    FOREIGN KEY(tag_id) REFERENCES public.tag(id)
);
ALTER SEQUENCE debate_tag_id_seq INCREMENT BY 50;

CREATE TABLE public.poll(
    id BIGSERIAL PRIMARY KEY,
    question VARCHAR,
    debate_id BIGSERIAL NOT NULL UNIQUE,
    FOREIGN KEY(debate_id) REFERENCES public.debate(id)
);
ALTER SEQUENCE poll_id_seq INCREMENT BY 50;

CREATE TABLE public.poll_vote_type(
    id BIGSERIAL PRIMARY KEY,
    label VARCHAR NOT NULL,
    poll_id BIGSERIAL NOT NULL,
    count BIGSERIAL NOT NULL,
    FOREIGN KEY(poll_id) REFERENCES public.poll(id)
);
ALTER SEQUENCE poll_vote_type_id_seq INCREMENT BY 50;

CREATE TABLE public.poll_vote(
    id BIGSERIAL PRIMARY KEY,
    poll_id BIGSERIAL NOT NULL,
    poll_vote_type_id BIGSERIAL NOT NULL,
    user_id BIGSERIAL NOT NULL,
    FOREIGN KEY(poll_id) REFERENCES public.poll(id),
    FOREIGN KEY(poll_vote_type_id) REFERENCES public.poll_vote_type(id),
    FOREIGN KEY(user_id) REFERENCES public.users(id)
);
ALTER SEQUENCE poll_vote_id_seq INCREMENT BY 50;