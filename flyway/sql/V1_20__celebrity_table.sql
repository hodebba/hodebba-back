CREATE TABLE public.topic
(
    id    BIGSERIAL PRIMARY KEY,
    label VARCHAR NOT NULL UNIQUE
);
ALTER SEQUENCE topic_id_seq INCREMENT BY 50;

CREATE TABLE public.profession
(
    id    BIGSERIAL PRIMARY KEY,
    label VARCHAR NOT NULL UNIQUE
);
ALTER SEQUENCE profession_id_seq INCREMENT BY 50;

CREATE TABLE public.political_party
(
    id    BIGSERIAL PRIMARY KEY,
    label VARCHAR NOT NULL UNIQUE
);
ALTER SEQUENCE political_party_id_seq INCREMENT BY 50;

CREATE TABLE public.political_position
(
    id    BIGSERIAL PRIMARY KEY,
    label VARCHAR NOT NULL UNIQUE
);
ALTER SEQUENCE political_position_id_seq INCREMENT BY 50;

CREATE TABLE public.celebrity
(
    id                    BIGSERIAL PRIMARY KEY,
    full_name             VARCHAR   NOT NULL,
    image_link            VARCHAR   NOT NULL,
    profession_id         BIGSERIAL NOT NULL,
    political_position_id BIGSERIAL,
    political_party_id    BIGSERIAL,
    FOREIGN KEY (profession_id) REFERENCES public.profession (id),
    FOREIGN KEY (political_position_id) REFERENCES public.political_position (id),
    FOREIGN KEY (political_party_id) REFERENCES public.political_party (id)
);
ALTER SEQUENCE celebrity_id_seq INCREMENT BY 50;

CREATE TABLE public.statement
(
    id             BIGSERIAL PRIMARY KEY,
    created_date   TIMESTAMP NOT NULL,
    is_validated   BOOLEAN   NOT NULL,
    statement_type VARCHAR,
    content        TEXT      NOT NULL,
    likes          INTEGER   NOT NULL,
    dislikes       INTEGER   NOT NULL,
    topic_id       BIGSERIAL NOT NULL,
    user_id        BIGSERIAL NOT NULL,
    celebrity_id   BIGSERIAL NOT NULL,
    FOREIGN KEY (topic_id) REFERENCES public.topic (id),
    FOREIGN KEY (user_id) REFERENCES public.users (id),
    FOREIGN KEY (celebrity_id) REFERENCES public.celebrity (id)
);
ALTER SEQUENCE statement_id_seq INCREMENT BY 50;

CREATE TABLE public.debate_statement
(
    id           BIGSERIAL PRIMARY KEY,
    debate_id    BIGSERIAL,
    statement_id BIGSERIAL,
    FOREIGN KEY (debate_id) REFERENCES public.debate (id),
    FOREIGN KEY (statement_id) REFERENCES public.statement (id)
);


CREATE TABLE public.position
(
    id              BIGSERIAL PRIMARY KEY,
    created_date    TIMESTAMP NOT NULL,
    question        VARCHAR   NOT NULL,
    explanation     TEXT,
    summary_type    VARCHAR   NOT NULL,
    summary_content VARCHAR   NOT NULL,
    topic_id        BIGSERIAL NOT NULL,
    debate_id       BIGSERIAL,
    celebrity_id    BIGSERIAL NOT NULL,
    FOREIGN KEY (topic_id) REFERENCES public.topic (id),
    FOREIGN KEY (debate_id) REFERENCES public.debate (id),
    FOREIGN KEY (celebrity_id) REFERENCES public.celebrity (id)
);
ALTER SEQUENCE position_id_seq INCREMENT BY 50;

CREATE TABLE public.like_dislike_statement
(
    id           BIGINT PRIMARY KEY REFERENCES public.like_dislike,
    statement_id BIGSERIAL NOT NULL,
    FOREIGN KEY (statement_id) REFERENCES public.statement (id)
);

CREATE TABLE public.source_statement
(
    id           BIGINT PRIMARY KEY REFERENCES public.source,
    statement_id BIGSERIAL NOT NULL,
    FOREIGN KEY (statement_id) REFERENCES public.statement (id)
);
